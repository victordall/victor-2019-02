let pokeApi = new PokeApi();
let pokemonEspecifico = pokeApi.buscarEspecifico(6);
pokemonEspecifico.then(pokemon => {
    let poke = new Pokemon(pokemon);
    renderizacaoPokemon(poke);
    
})

function renderizacaoPokemon(pokemon) {
    let dadosPokemon = document.getElementById('dadosPokemon');
    let img = document.getElementById('img');
    let nome = dadosPokemon.querySelector('.nome');
    //nome.innerHTML = pokemon.name;
    nome.innerHTML = pokemon.nome;   
    img.src = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/6.png";
}