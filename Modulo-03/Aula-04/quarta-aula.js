let pokemon = fetch(`https://pokeapi.co/api/v2/pokemon/`);

pokemon
    .then(data => data.json())
    .then(data => {
        console.log(data.results[5])
    });


const valor1 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve({ valorAtual: '1' })
    }, 4000)
})

const valor2 = new Promise((resolve, reject) => {
    resolve({ valorAtual: '2' })
})

Promise.all([valor1, valor2])
   // .race([valor1. valor2])
    .then(resposta => {
        console.log(resposta);
    })

let charizard = fetch(`https://pokeapi.co/api/v2/pokemon/6`);



// class Turma {
//     constructor(ano) {
//         this.ano = ano;
//     }

//     apresentarAno() {
//         console.log(`Essa turma é do ano de ${this.ano}`);
//     }

//     static info() {
//         console.log(`Testando informações`)
//     }

//     get anoTurma() {
//         console.log(this.ano);
//     }

//     set local(localizacao) {
//         this.localTurma = localizacao;
//     }
// }

// //const vemser = new Turma("2019/02");
// //vemser.apresentarAno();
// //vemser.local = "DBC";
// //console.log(vemser.localTurma);

// class Vemser extends Turma {
//     constructor(ano, local, qtdAlunos) {
//         super(ano);
//         this.local = local;
//         this.qtdAlunos = qtdAlunos;
//     }

//     descricao() {
//         console.log(`Turma do Vem Ser, ano ${this.ano}, realizado na ${this.local} quantidade de alunos: ${this.qtdAlunos}`)
//     }
// }

// const vemser = new Vemser("2019/02", "DBC", 17);
// vemser.descricao();

// let defer = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         if (true) {
//             resolve("Foi resolvido");
//         } else {
//             reject("Nem eras");
//         }
//     }, 3000);
// });

// defer
//     .then((data) => {
//         console.log(data);
//         return "Novo resultado";
//     })
//     .then((data) => {
//         console.log(data)
//     })
//     .catch((erro) => console.log(`Nice kati: ${erro}`))

