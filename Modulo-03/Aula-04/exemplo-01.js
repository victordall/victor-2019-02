class Turma {
    constructor(ano) {
        this.ano = ano;
    }

    apresentarAno() {
        console.log(`Essa turma é do ano de ${this.ano}`);
    }

    static info() {
        console.log(`Testando informações`)
    }

    get anoTurma() {
        console.log(this.ano);
    }

    set local(localizacao) {
        this.localTurma = localizacao;
    }
}

//const vemser = new Turma("2019/02");
//vemser.apresentarAno();
//vemser.local = "DBC";
//console.log(vemser.localTurma);

class Vemser extends Turma {
    constructor(ano, local, qtdAlunos) {
        super(ano);
        this.local = local;
        this.qtdAlunos = qtdAlunos;
    }

    descricao(){
        console.log(`Turma do Vem Ser, ano ${this.ano}, realizado na ${this.local} quantidade de alunos: ${this.qtdAlunos}`)
    }
}

const vemser = new Vemser("2019/02", "DBC", 17);
vemser.descricao();

let defer = new Promise((resolve, reject) => {
    if(true){
        resolve("Foi resolvido"); 
    }else{
        reject("Nem eras");
    }
});

defer
    .then((data) => {
        console.log(data);
        return "Novo resultado";
    })
    .then((data) => {
        console.log(data)
    })
    .catch((erro) => console.log(`Nice kati: ${erro}`))