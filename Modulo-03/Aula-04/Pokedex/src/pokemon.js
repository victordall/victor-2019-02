class Status {
  constructor( status ) {
    this.nome = status.stat.name;
    this.baseStat = status.base_stat;
  }
}

class Pokemon { // eslint-disable-line no-unused-vars
  constructor( obj ) {
    this.nome = obj.name;
    this.id = obj.id;
    this.peso = obj.weight;
    this.altura = obj.height;
    this.stats = obj.stats;
    this.tipo = obj.types;
    this.sprite = obj.sprites.front_default;
  }

  get estatisticas() {
    const listStatusF = this.stats.map( status => new Status( status ) );
    return listStatusF;
  }
}
