const pokeApi = new PokeApi();

let pokemonNaTela = -1;
let numSorteioAntigo = 0;
const numPoke = document.getElementById( 'numPoke' );
const spanErroIdInvalido = document.getElementById( 'erroId' );
const spanErroPkmNaTela = document.getElementById( 'erroPkmNaTela' );
const nomePkm = document.getElementById( 'nomePokemon' );
const spritePkm = document.getElementById( 'spritePkm' );
const alturaPkm = document.getElementById( 'altura' );
const pesoPkm = document.getElementById( 'peso' );
const tipoP = document.getElementById( 'tipoP' );
const tipoS = document.getElementById( 'tipoS' );
const ulStats = document.getElementById( 'stats' );
const h3S = document.getElementById( 'h3S' );

function formatarAltura( altura ) {
  const alturaF = altura * 10;
  if ( alturaF > 99 ) return `${ alturaF / 100 } m`;
  return `${ alturaF } cm`;
}

function darTipoAoBtn( btn, tipo ) {
  switch ( tipo ) {
    case 'normal':
      btn.classList.add( 'button-normal' );
      break;
    case 'fighting':
      btn.classList.add( 'button-fight' );
      break;
    case 'flying':
      btn.classList.add( 'button-flying' );
      break;
    case 'poison':
      btn.classList.add( 'button-poison' );
      break;
    case 'ground':
      btn.classList.add( 'button-ground' );
      break;
    case 'rock':
      btn.classList.add( 'button-rock' );
      break;
    case 'bug':
      btn.classList.add( 'button-bug' );
      break;
    case 'ghost':
      btn.classList.add( 'button-ghost' );
      break;
    case 'steel':
      btn.classList.add( 'button-steel' );
      break;
    case 'fire':
      btn.classList.add( 'button-fire' );
      break;
    case 'water':
      btn.classList.add( 'button-water' );
      break;
    case 'grass':
      btn.classList.add( 'button-grass' );
      break;
    case 'eletric':
      btn.classList.add( 'button-eletric' );
      break;
    case 'psychic':
      btn.classList.add( 'button-psychic' );
      break;
    case 'ice':
      btn.classList.add( 'button-ice' );
      break;
    case 'dragon':
      btn.classList.add( 'button-dragon' );
      break;
    case 'dark':
      btn.classList.add( 'button-dark' );
      break;
    case 'fairy':
      btn.classList.add( 'button-fairy' );
      break;
    default:
      break;
  }
}

function darTipo( tipos ) {
  let primary = tipos[0].type.name;
  const classTipoP = tipoP.classList.item( 0 );
  tipoP.classList.remove( classTipoP );
  darTipoAoBtn( tipoP, primary );
  primary = primary.toUpperCase();
  tipoP.innerHTML = `${ primary }`;
  tipoP.classList.remove( 'hide' );
  try {
    tipoS.classList.remove( 'hide' );
    let secondary = tipos[1].type.name;
    const classTipo = tipoS.classList.item( 0 );
    tipoS.classList.remove( classTipo );
    darTipoAoBtn( tipoS, secondary );
    secondary = secondary.toUpperCase();
    tipoS.innerHTML = `${ secondary }`;
  } catch ( err ) {
    tipoS.classList.add( 'hide' );
  }
}

function renderizarStats( stats ) {
  h3S.classList.remove( 'hide' );
  ulStats.innerHTML = '';
  stats.forEach( s => {
    const stat = document.createElement( 'li' );
    stat.appendChild( document.createTextNode( `${ s.nome }: ${ s.baseStat }` ) );
    ulStats.appendChild( stat );
  } );
}

function renderizacaoPokemon( pokemon ) {
  const nomeFormatado = pokemon.nome.charAt( 0 ).toUpperCase() + pokemon.nome.slice( 1 );
  nomePkm.innerHTML = `${ nomeFormatado } #${ pokemon.id }`
  spritePkm.src = `${ pokemon.sprite }`
  alturaPkm.innerHTML = `Altura: ${ formatarAltura( pokemon.altura ) }`;
  pesoPkm.innerHTML = `Peso: ${ pokemon.peso / 10 } kg`;
  darTipo( pokemon.tipo );
  renderizarStats( pokemon.estatisticas );
}

function procurarPkm() {
  const numPkm = isNaN( numPoke.value ) ? '' : numPoke.value; // eslint-disable-line no-restricted-globals
  if ( numPkm === '' ) {
    spanErroIdInvalido.className = 'show';
    return;
  }
  if ( pokemonNaTela != numPkm ) { // eslint-disable-line eqeqeq
    const pokemonProcurado = pokeApi.buscarEspecifico( numPkm );
    pokemonProcurado
      .then( pokeRes => {
        const pkm = new Pokemon( pokeRes );
        spanErroIdInvalido.className = 'hide';
        spanErroPkmNaTela.className = 'hide';
        pokemonNaTela = numPkm;
        renderizacaoPokemon( pkm );
      } )
      .catch( erro => {
        spanErroIdInvalido.className = 'show';
        console.log( new ErrorEvent( erro ) );
      } )
  } else {
    spanErroPkmNaTela.className = 'show';
    spanErroIdInvalido.className = 'hide';
  }
}

numPoke.addEventListener( 'blur', () => procurarPkm() );

function geraRandom() {
  const min = 1;
  const max = 802;
  let random = Math.floor( Math.random() * ( +max - +min ) ) + +min;
  while ( numSorteioAntigo === random ) {
    random = Math.floor( Math.random() * ( +max - +min ) ) + +min;
  }
  numSorteioAntigo = random;
  pokemonNaTela = numSorteioAntigo;
  return random;
}

function estouComSorte() { // eslint-disable-line no-unused-vars
  const random = geraRandom();
  const pokemonProcurado = pokeApi.buscarEspecifico( random );
  pokemonProcurado
    .then( pokeRes => {
      const pkm = new Pokemon( pokeRes );
      spanErroIdInvalido.className = 'hide';
      spanErroPkmNaTela.className = 'hide';
      renderizacaoPokemon( pkm );
    } )
    .catch( erro => {
      spanErroIdInvalido.className = 'show';
      console.log( new ErrorEvent( erro ) );
    } )
}
