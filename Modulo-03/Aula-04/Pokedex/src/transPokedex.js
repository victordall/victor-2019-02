const pokedexFechada = document.getElementById( 'pokedexFechada' );
//  let pokedexTrans = document.getElementById('pokedexTrans');
const pokedexAberta = document.getElementById( 'pokedexAberta' );
const pkBolaTop = document.getElementById( 'pkBolaTop' );
const pkBolaBot = document.getElementById( 'pkBolaBot' );

function abrePokedex() { // eslint-disable-line no-unused-vars
  pkBolaTop.classList.add( 'rotateT' );
  pkBolaBot.classList.add( 'rotateB' );
  setTimeout( () => {
    pokedexFechada.classList.add( 'hide' );
    // pokedexTrans.classList.remove("hide");
    pokedexAberta.classList.remove( 'hide' );
  }, 100 );
}
