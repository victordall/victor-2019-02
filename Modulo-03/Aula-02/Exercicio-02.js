//Exercício 1
function arrendodar(numero, precisao = 2) {
    const fator = Math.pow(10, precisao);
    return Math.ceil(numero * fator) / fator;
}

let imprimirGBP = function (numero) {
    let qtdCasasMilhares = 3;
    let separadorMilhar = ",";
    let separadorDecimal = ".";

    let stringBuffer = [];
    let parteDecimal = arrendodar(Math.abs(numero) % 1);
    let parteInteira = Math.trunc(numero);
    let parteInteiraString = Math.abs(parteInteira).toString();
    let parteInteiraTamanho = parteInteiraString.length;
    let c = 1;
    while (parteInteiraString.length > 0) {
        if (c % qtdCasasMilhares == 0) {
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho - c)}`)
            parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c)
        } else if (parteInteiraString.length < qtdCasasMilhares) {
            stringBuffer.push(parteInteiraString);
            parteInteiraString = '';
        }
        c++
    }
    stringBuffer.push(parteInteiraString);

    let decimalString = parteDecimal.toString().replace('0.', '');
    return `${parteInteira >= 0 ? '£ ' : '-£ '}${stringBuffer.reverse().join('')}${separadorDecimal}${decimalString}`
}

console.log("EX. 1");
console.log(imprimirGBP(0));
console.log(imprimirGBP(3498.99));
console.log(imprimirGBP(-3498.99));
console.log(imprimirGBP(2313477.0135));

let imprimirFR  = function (numero) {
    let qtdCasasMilhares = 3;
    let separadorMilhar = ".";
    let separadorDecimal = ",";

    let stringBuffer = [];
    let parteDecimal = arrendodar(Math.abs(numero) % 1);
    let parteInteira = Math.trunc(numero);
    let parteInteiraString = Math.abs(parteInteira).toString();
    let parteInteiraTamanho = parteInteiraString.length;
    let c = 1;
    while (parteInteiraString.length > 0) {
        if (c % qtdCasasMilhares == 0) {
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho - c)}`)
            parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c)
        } else if (parteInteiraString.length < qtdCasasMilhares) {
            stringBuffer.push(parteInteiraString);
            parteInteiraString = '';
        }
        c++
    }
    stringBuffer.push(parteInteiraString);

    let decimalString = parteDecimal.toString().replace('0.', '');
    return `${stringBuffer.reverse().join('')}${separadorDecimal}${decimalString}${' €'}`
}

console.log("EX. 2");
console.log(imprimirFR(0));
console.log(imprimirFR(3498.99));
console.log(imprimirFR(-3498.99));
console.log(imprimirFR(2313477.0135));