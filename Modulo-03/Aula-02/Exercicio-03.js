function cardapioIFood(veggie = true, comLactose = false) {
  let cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ]

  if (comLactose) {
    cardapio.push('pastel de queijo')
  }

  cardapio = [...cardapio, 'pastel de carne', 'empada de legumes marabijosa']
  
  if (veggie) {
    cardapio.splice(cardapio.indexOf('enroladinho de salsicha'), 1);
    cardapio.splice(cardapio.indexOf('pastel de carne'), 1);
  }

  let resultado = cardapio.map(alimento => alimento.toUpperCase());
  return resultado;
}

console.log(cardapioIFood(true, true)) // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]




//concat sad version
// cardapio = cardapio.concat([
  //   'pastel de carne',
  //   'empada de legumes marabijosa'
  // ])

  //caso queira algum item especifico
  // let resultado = cardapio.filter(alimento => alimento === 'Cuca de uva')
  //     .map(alimento => alimento.toUpperCase());