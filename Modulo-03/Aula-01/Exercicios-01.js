//Exercício 1
let calcularCirculo = function (raio, tipo) {
    const pi = 3.14;
    if (tipo == "A") {
        return (raio * raio) * pi;
    } else if (tipo == "C") {
        return ((2 * pi) * raio);
    } else {
        return 0;
    }
}

console.log("EX. 1");
console.log(calcularCirculo(3, "A"));
console.log(calcularCirculo(3, "C"));
console.log(calcularCirculo(2, "Z"));

// let circulo = {
//     raio: 3,
//     tipoCalculo: "A"
// }

// let calcularCirculo = function ({ raio, tipoCalculo: tipo }) {
//     return Math.ceil(tipo == "A" ? Math.PI * Math.pow(raio, 2) : 2 * Math.PI * raio);
// }

//Exercício 2
let naoBissexto = function (ano) {
    if (ano % 400 == 0) {
        return false;
    } else if (ano % 4 == 0 && ano % 100 != 0) {
        return false;
    }
    return true;
}

console.log("EX. 2");
console.log(naoBissexto(2016))
console.log(naoBissexto(2000))
console.log(naoBissexto(2001))

//Exercício 3
let somarPares = function (array) {
    let total = 0;
    for (i = 0; i < array.length; i++) {
        if (i % 2 == 0) {
            total = total + array[i];
        }
    }

    return total;
}

console.log("EX. 3");
console.log(somarPares([1, 56, 4.34, 6, -2]));

//Exercício 4

let adicionar = op1 => op2 => op1 + op2;
console.log("EX. 4");
console.log(adicionar(3)(4));
console.log(adicionar(5643)(8749));

//Exercício 5
function arrendodar(numero, precisao = 2) {
    const fator = Math.pow(10, precisao);
    return Math.ceil(numero * fator) / fator;
}

let imprimirBRL = function (numero) {
    let qtdCasasMilhares = 3;
    let separadorMilhar = ".";
    let separadorDecimal = ",";

    let stringBuffer = [];
    let parteDecimal = arrendodar(Math.abs(numero) % 1);
    let parteInteira = Math.trunc(numero);
    let parteInteiraString = Math.abs(parteInteira).toString();
    let parteInteiraTamanho = parteInteiraString.length;
    let c = 1;
    while (parteInteiraString.length > 0) {
        if (c % qtdCasasMilhares == 0) {
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho - c)}`)
            parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c)
        } else if (parteInteiraString.length < qtdCasasMilhares) {
            stringBuffer.push(parteInteiraString);
            parteInteiraString = '';
        }
        c++
    }
    stringBuffer.push(parteInteiraString);

    let decimalString = parteDecimal.toString().replace('0.', '');
    return `${parteInteira >= 0 ? 'R$' : '-R$'}${stringBuffer.reverse.join('')}${separadorDecimal}${decimalString}`
}

console.log("EX. 5");
console.log(imprimirBRL(0));
console.log(imprimirBRL(3498.99));
console.log(imprimirBRL(-3498.99));
console.log(imprimirBRL(23974298374.3499));



// let imprimirBRL = function (valor) {
//     let numeroString = valor.toString();
//     numeroString = numeroString.replace(".", ",");

//     function adicionarPonto(numero) {
//         var args = [].slice.call(arguments, 1),
//             i = 0;

//         return str.replace(/%s/g, () => args[i++]);
//     }

//     return numeroString;
// }




