//console.log('alert("Cheguei") <-- kk');

var teste = 123;
let teste1 = 123;
const teste2 = 123;
var teste = "111";
//console.log(teste);
//console.log(teste1);
{
    let teste1 = "Aqui mudou";
    //console.log(teste1);
}
//console.log(teste1);

const pessoa = {
    nome: "Marcos",
    idade: 29,
    endereco: {
        logradouro: "rua da esquerda",
        numero: 123
    }
};

//Object.freeze(pessoa);
//pessoa.nome = "Marcos H"; //msm const

//console.log(pessoa.nome);


function somar(valor1, valor2) {
    console.log(valor1 + valor2);
}

// somar(1, 3);
// somar("Aqui", 3);

function ondeMoro(cidade) {
    console.log("Eu moro em " + cidade + "E eu sou muito feliz");
    console.log(`Eu moro em ${cidade} E eu sou muito feliz`);
    //`` + valor2
}

//ondeMoro("Gravataí");

function fruteira() {
    let texto =
        `Banana
Ameixa
Laranja
Marimba`;

    console.log(texto);
}

//fruteira();

function quemSou(pessoa) {
    console.log(`Meu nome é ${pessoa.nome} e tenho ${pessoa.idade} anos`);
}

//quemSou(pessoa);

let funcaoTeste = function () {
    return "Teste";
}

let funcaoSomarValor = function (a, b, c = 0) {
    return a + b + c;
}

let add = funcaoSomarValor;
let res = add(3, 2);
let res2 = add(3, 2, 3);

//console.log(funcaoTeste());
// console.log("Com 'c': " + res2);
// console.log("Sem 'c': " + res);

const { nome: n, idade } = pessoa;
const { endereco: { logradouro, numero } } = pessoa;
// console.log(n, idade);
// console.log(logradouro, numero);


const array = [1, 3, 4, 8];
const [n1, , n2, , n3 = 9] = array;
//console.log(n1, n2,n3);

function testarPessoa({ nome, idade }) {
    console.log(nome, idade);
}

// testarPessoa(pessoa);

function testarPessoaArray({ nome, idade }, [n1,, n2]) {
    console.log(nome, idade, n1,n2);
}

//testarPessoaArray(pessoa, array);

let a1 = 42;
let b1 = 15;
console.log(a1, b1);

[a1, b1] = [b1, a1];