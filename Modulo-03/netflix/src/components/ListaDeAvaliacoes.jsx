import Avaliacao from '../models/Avaliacao'


export default class ListaDeAvaliacoes {
    constructor() {
        this.avaliacoes = [].map(a => new Avaliacao(a.nomeEpisodio, a.nota, a.temporada, a.ep))
    }

    avaliar(episodio) {
        const { nome, nota, temporada, ordemEpisodio } = episodio;
        const Avaliacao = {
            nomeEpisodio: nome,
            nota: parseInt(nota),
            temporada: temporada,
            ep: ordemEpisodio
        }
        this.avaliacoes.push(Avaliacao);
    }
}