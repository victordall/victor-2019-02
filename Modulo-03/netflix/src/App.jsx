import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import './App.css';

import InfoJsFlix from './pages/InfoJsFlix';
import ReactMirror from './pages/React';
import Login from './pages/Login';

import { PrivateRoute } from './components/PrivateRoute';

export default class App extends Component {
  render() {
    return (
      <Router>
        <React.Fragment>
          <PrivateRoute path="/" exact component={PaginaInicial} />
          <Route path="/login" component={Login} />
          <Route path="/mirror" exact component={ReactMirror} />
          <Route path="/infoFlix" component={InfoJsFlix} />
        </React.Fragment>
      </Router>
    );
  }
}


const PaginaInicial = () =>
  <section className="principal">
    <Link to="/mirror" >Mirror</Link>
    <Link to="/infoFlix">Info JsFlix</Link>
  </section>
