import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as axios from 'axios';
import './mirror.css';
import ListaEpisodios from '../models/ListaEpisodios';
import EpisodioPadrao from '../components/EpisodioPadrao';
import MensagemFlash from '../components/MensagemFlash';
import MensagemFlashConst from '../constants/MensagemFlashConst';
import MeuInputNumero from '../components/MeuInputNumero';
import ListaDeAvaliacoes from '../components/ListaDeAvaliacoes'

class ReactMirror extends Component {
  constructor(props) {
    super(props)
    this.listaEpisodios = new ListaEpisodios();
    this.listAvaliacoes = new ListaDeAvaliacoes();

    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      exibirMensagem: false,
      mensagem: '',
      deveExibirErro: false
    }
  }

  componentDidMount() {
    axios.get('https://pokeapi.co/api/v2/pokemon/').then(response => console.log(response))
  }

  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
      episodio
    })
  }

  marcarComoAssistido() {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido(episodio)
    this.setState({
      episodio
    })
  }

  registrarNota({ valor, erro }) {
    this.setState({
      deveExibirErro: erro
    })
    if (erro) {
      return;
    }
    const { episodio } = this.state
    let cor, mensagem
    if (episodio.validarNota(valor)) {
      episodio.avaliar(valor)
      this.listAvaliacoes.avaliar(episodio);
      alert(this.listAvaliacoes.avaliacoes.length)
      cor = 'verde'
      mensagem = MensagemFlashConst.SUCESSO.REGISTRAR_NOTA
    } else {
      cor = 'vermelho'
      mensagem = MensagemFlashConst.ERRO.NOTA_INVALIDA
    }
    this.exibirMensagem({ cor, mensagem })
  }

  exibirMensagem = ({ cor, mensagem }) => {
    this.setState({
      cor,
      mensagem,
      exibirMensagem: true
    })
  }

  atualizarMensagem = devoExibir => {
    this.setState({
      exibirMensagem: devoExibir
    })
  }

  logout() {
    localStorage.removeItem('Authorization');
  }

  render() {
    const { episodio, exibirMensagem, cor, mensagem, deveExibirErro } = this.state
    return (
      <div className="App">
        <MensagemFlash atualizarMensagem={this.atualizarMensagem}
          cor={cor}
          deveExibirMensagem={exibirMensagem}
          mensagem={mensagem} segundos={5} />
        <div className="App-Header">
          <Menu />
          <button type="button" onClick={this.logout.bind(this)}>Deslogar</button>
          <EpisodioPadrao episodio={episodio} sortearNoComp={this.sortear.bind(this)} MarcarNoComp={this.marcarComoAssistido.bind(this)} />
          <MeuInputNumero placeholder="1 a 5"
            mensagemCampo="Qual sua nota para esse episódio?"
            atualizarValor={this.registrarNota.bind(this)}
            obrigatorio={true}
            deveExibirErro={deveExibirErro}
            visivel={episodio.assistido || false} />
        </div>
      </div>
    );
  }
}

const Menu = () =>
  <section className="principal">
    <Link to="/" >Pagina Inicial</Link>
    <Link to="/infoFlix">Info JsFlix</Link>
  </section>

export default ReactMirror;
