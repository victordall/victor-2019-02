import Episodio from '../models/Episodios'

export default class Avaliacao {
    constructor(nomeEpisodio, nota, temporada, ep) {
        this.nomeEpisodio = nomeEpisodio
        this.nota = nota
        this.temporada = temporada
        this.ep = ep
    }

    avaliar(episodio) {
        const { nome, nota, temporada, ordemEpisodio} = episodio;
        this.nomeEpisodio = nome;
        this.nota = parseInt(nota)
        this.temporada = temporada;
        this.ep = ordemEpisodio
    }
}