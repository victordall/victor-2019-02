import React, { Component } from 'react';
import './App.css';
import './css/section.css'
import ListaEpisodios from './models/ListaEpisodios';
import EpisodioPadrao from './components/EpisodioPadrao'
import TesteRenderizacao from './components/TestRenderizacao';

class App extends Component {
  constructor(props) {
    super(props)
    this.ListaEpisodios = new ListaEpisodios();
    // this.sortear = this.sortear.bind( this )
    this.state = {
      episodio: this.ListaEpisodios.aleatorios
    }
  }

  sortear = () => {
    const episodio = this.ListaEpisodios.aleatorios
    this.setState({
      episodio
    })
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.ListaEpisodios.marcarComoAssistido(episodio)
    this.setState({
      episodio
    })
  }

  darNota(evt) {
    const { episodio } = this.state
    const registrado = document.getElementById('registrado');
    const erro = document.getElementById('erro');
    const res = this.ListaEpisodios.darNota(episodio, evt.target.value);
    if (res) {
      registrado.classList.remove('hide');
      erro.classList.add('hide');
      this.setState({
        episodio
      })
      setTimeout(() => { registrado.classList.add('hide'); }, 5000);
    } else {
      erro.classList.remove('hide');
    }

  }

  gerarInput() {
    return (
      this.state.episodio.assistido && (
      <div className="row">
        <div className="col col-100">
          <span id="registrado" className="hide"><b>Nota registrada com sucesso!</b></span>
          <span id="erro" className="hide"><b>Nota deve ser entre 1 e 5!</b></span>
          <form>
            <label for="avaliar">Avalie o episódio</label>
            <input type="number" id="avaliar" className="margin-top" onBlur={this.darNota.bind(this)} placeholder="1 a 5"></input>
          </form>
        </div>
      </div>
      )
    )
  }



  render() {
    const { episodio } = this.state
    return (
      <div className="App">
        <div className="App-header">
          <EpisodioPadrao episodio={episodio} sortearnoComp={this.sortear.bind(this)} marcarAssististidoComp={this.marcarComoAssistido.bind(this)} gerarInputComp={this.gerarInput.bind(this)} />      
         {/* <TesteRenderizacao >  nome={'Marcos'}
            Aqui novamente
          </TesteRenderizacao>  */}
          </div>
      </div>
    );

  }
}

export default App;
