import React from 'react'

const EpisodioPadrao = props => {
    const { episodio } = props;
    return (
        <React.Fragment>
            <div className="Container">
                <div className="row">
                    <div className="col col-100">
                        <h2>{episodio.nome}</h2>
                        <img src={episodio.thumbUrl} alt={episodio.nome}></img>
                    </div>
                </div>
                <div className="row">
                    <div className="col col-100">
                        <span>Duração: {episodio.duracao} minutos</span>
                    </div>
                </div>
                <div className="row">
                    <div className="col col-50">
                        <span>Temporada: {episodio.temporada}</span>
                    </div>
                    <div className="col col-50">
                        <span>Episódio: {episodio.ordemEpisodio}</span>
                    </div>
                </div>
                <div className="row">
                    <div className="col col-50">
                        <span>Nota:</span>
                    </div>
                    <div className="col col-50">
                        {episodio.nota || "Sem nota"}
                    </div>
                </div>
                <div className="row">
                    <div className="col col-100">
                        <button className="button-prox" onClick={() => props.sortearnoComp()}>Próximo</button>
                    </div>
                </div>
                <div className="row">
                    <div className="col col-74">
                        <h4>Já assisti? {episodio.assistido ? 'Sim' : 'Não'}, {episodio.qtdVezesAssistido} vez(es)</h4>
                    </div>
                    <div className="col col-25 margin-top">
                        <button className="button-jaAssisti" onClick={() => props.marcarAssististidoComp()}>Já vi</button>
                    </div>
                </div>
                {props.gerarInputComp()}
            </div>
        </React.Fragment>
    )
}

export default EpisodioPadrao