//Exercicio 1
let multiplicar = function (...valores) {
    if (valores.length > 0) {
        const mult = valores[0];
        valores.splice(0, 1);
        let resultado = valores.map(val => mult * val);
        return resultado;
    }
}

console.log(multiplicar(3, 4, 5, 6, 7, 8));
console.log(multiplicar(5, 3, 4)) // [15, 20]	
console.log(multiplicar(5, 3, 4, 5)) // [15, 20, 25]


//EXERCICIO 2 ESTÁ NOO MODULO 2 - EXTRA 1 -> js chamado validar.js



//aula normal
String.prototype.correr = function(upper = false){
    let texto = `${this} estou orrendo`;
    return upper ? texto.toUpperCase() : texto;
}

console.log("eu".correr(true));
