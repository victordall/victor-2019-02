import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import '../css/container.css'
import '../css/login.css'
import '../css/general.css'
import '../css/button.css'
import ComponentHeader from '../components/header/ComponentHeader'
import ComponentFooter from '../components/footer/ComponentFooter'


export default class Home extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
        // this.trocaValoresState = this.trocaValoresState.bind(this)
    }

    render() {

        return (
            <React.Fragment>
                <ComponentHeader></ComponentHeader>
                <React.Fragment>
                    <div className="container">
                        <div className="row row-100 padding-top-login padding-bot-login">
                            <div className="col col-100 center-horizontal">
                                <div className="row">
                                    <div className="col col-100 col-center">
                                        <h1>Bem vindo!</h1>
                                    </div>
                                </div>
                                <div className="row margin-top-50">
                                    <div className="col col-25">
                                        <Link className="btn-home" to="/Agencias">Agências</Link>
                                    </div>
                                    <div className="col col-25">
                                        <Link className="btn-home" to="/Clientes">Clientes</Link>
                                    </div>
                                    <div className="col col-25">
                                        <Link className="btn-home" to="/TiposDeContas">Tipo de contas</Link>
                                    </div>
                                    <div className="col col-25">
                                        <Link className="btn-home" to="/ContaClientes">Conta de clientes</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </React.Fragment>
                <ComponentFooter></ComponentFooter>
            </React.Fragment >
        )
    }
}