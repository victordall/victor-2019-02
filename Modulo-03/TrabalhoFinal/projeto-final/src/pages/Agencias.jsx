import React, { Component } from 'react';
import * as axios from 'axios';
import './css/ListDetalhes.css'
import '../css/container.css'
import '../css/general.css'
import '../css/button.css'
import Agencia from '../models/Agencia'
import Endereco from '../models/Endereco'
import ComponentHeader from '../components/header/ComponentHeader'
import ComponentFooter from '../components/footer/ComponentFooter'
import ComponentTodasAgencias from '../components/agencia/ComponentTodasAgencias'
import DetalhesAgencia from '../components/agencia/DetalhesAgencia'
import BotaoVoltar from '../components/BotaoVoltar'



export default class Agencias extends Component {
    constructor(props) {
        super(props)
        this.state = {
            agencias: [],
            mostrarAll: true,
            mostrarEspe: false,
            agencia: {},
            filtrou: false
        }
        this.tornarDigital = this.tornarDigital.bind(this);
        this.getDetails = this.getDetails.bind(this);
        this.voltar = this.voltar.bind(this);
        this.filtrar = this.filtrar.bind(this);
        this.getAllAgencias();
    }


    getAgenciasFromStorage() {
        this.setState({
            agencias: JSON.parse(localStorage.getItem("listaAgencia")),
            mostrarAll: true,
            mostrarEspe: false,
            filtrou: false
        });
    }

    getAllAgencias() {
        axios.get('http://localhost:1337/agencias', {
            headers: { Authorization: 'banco-vemser-api-fake' }
        }).then(resp => {
            const agenciasList = resp.data.agencias
                .map(a => new Agencia(a.id, a.codigo, a.nome,
                    new Endereco(a.endereco.logradouro, a.endereco.numero, a.endereco.bairro, a.endereco.cidade, a.endereco.uf), false));
            this.setState({
                agencias: agenciasList,
                mostrarAll: true,
                mostrarEspe: false,
            })
            localStorage.setItem('listaAgencia', JSON.stringify(agenciasList));
        }).catch(x => {
            console.log(x)
        })
    }

    filtrar(tipo) {
        console.log(tipo)
        if (tipo) {
            const { agencias } = this.state
            const digitais = agencias.filter(a => a.is_digital == true);
            this.setState({
                agencias: digitais,
                mostrarAll: true,
                mostrarEspe: false,
                filtrou: true
            })
        }else{
            this.getAgenciasFromStorage();
        }

    }

    tornarDigital(id) {
        const { agencias } = this.state;
        const index = agencias.findIndex((a => a.id == id));
        if (index != null) {
            agencias[index].is_digital = !agencias[index].is_digital;
            localStorage.setItem('listaAgencia', JSON.stringify(agencias));
            this.setState({
                agencias: agencias
            })
        }
    }

    getDetails(id) {
        const { agencias } = this.state;
        const index = agencias.findIndex((a => a.id == id));
        this.setState({
            mostrarAll: false,
            mostrarEspe: true,
            agencia: agencias[index]
        })
    }

    voltar() {
        this.getAgenciasFromStorage();
    }

    render() {
        const { agencias, mostrarAll, mostrarEspe, agencia, filtrou } = this.state
        return (
            <React.Fragment>
                <ComponentHeader></ComponentHeader>
                {
                    mostrarAll ? <ComponentTodasAgencias agencias={agencias} filtrou={filtrou}
                        getDetails={this.getDetails}
                        tornarDigital={this.tornarDigital} filtrar={this.filtrar} /> : null
                }
                {
                    mostrarEspe ? <React.Fragment><DetalhesAgencia agencia={agencia} />
                        <BotaoVoltar voltar={this.voltar} /></React.Fragment> : null
                }
                <ComponentFooter></ComponentFooter>
            </React.Fragment>
        )
    }
}




//lixo?
    // atualizarState() {
    //     const list = localStorage.getItem('listaAgencia');
    //     const agencia = JSON.parse(list).map(a => new Agencia(a.id, a.codigo, a.nome,
    //         new Endereco(a.endereco.logradouro, a.endereco.numero, a.endereco.bairro, a.endereco.cidade, a.endereco.uf), a.is_digital));
    //     this.setState({
    //         agencias: agencia
    //     })
    // }