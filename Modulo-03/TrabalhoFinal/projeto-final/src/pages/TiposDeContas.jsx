import React, { Component } from 'react';
import * as axios from 'axios';
import './css/ListDetalhes.css'
import '../css/container.css'
import '../css/general.css'
import '../css/button.css'
import TipoConta from '../models/TipoConta'
import ComponentHeader from '../components/header/ComponentHeader'
import ComponentFooter from '../components/footer/ComponentFooter'
import ComponentTodasTipoContas from '../components/tipoContas/ComponentTodasTipoContas'
import DetalhesTipoConta from '../components/tipoContas/DetalhesTipoConta'
import BotaoVoltar from '../components/BotaoVoltar'

export default class TiposDeContas extends Component {
    constructor(props) {
        super(props)
        this.state = {
            contas: [],
            mostrarAll: true,
            mostrarEspe: false,
            conta: {},
        }
        this.getDetails = this.getDetails.bind(this);
        this.voltar = this.voltar.bind(this);
        this.getAllContas();
    }



    getAllContas() {
        axios.get('http://localhost:1337/tipoContas', {
            headers: { Authorization: 'banco-vemser-api-fake' }
        }).then(resp => {
            const cList = resp.data.tipos
                .map(c => new TipoConta(c.id, c.nome));
            this.setState({
                contas: cList,
                mostrarAll: true,
                mostrarEspe: false,
            })
        }).catch(x => {
            console.log(x)
        })
    }

    getDetails(id) {
        const { contas } = this.state;
        const index = contas.findIndex((a => a.id == id));
        this.setState({
            mostrarAll: false,
            mostrarEspe: true,
            tipoConta: contas[index]
        })
    }

    voltar() {
        this.getAllContas();
    }

    render() {
        const { contas, mostrarAll, mostrarEspe, tipoConta } = this.state
        return (
            <React.Fragment>
                <ComponentHeader></ComponentHeader>
                {
                    contas.length > 0 && mostrarAll ? <ComponentTodasTipoContas contas={contas}
                        getDetails={this.getDetails} /> : null
                }
                {
                    mostrarEspe ? <React.Fragment><DetalhesTipoConta tipoConta={tipoConta} />
                        <BotaoVoltar voltar={this.voltar} /></React.Fragment> : null
                }
                <ComponentFooter></ComponentFooter>
            </React.Fragment>
        )
    }
}
