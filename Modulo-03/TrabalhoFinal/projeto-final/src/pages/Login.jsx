import React, { Component } from 'react';
import * as axios from 'axios';
import '../css/container.css'
import '../css/login.css'
import '../css/general.css'
import '../css/button.css'
import ComponentHeader from '../components/header/ComponentHeader'
import ComponentFooter from '../components/footer/ComponentFooter'
import MensagemFlashConst from '../constants/MensagemFlashConst';
import MensagemFlash from '../components/mensagemFlash/MensagemFlash'



export default class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            password: '',
            exibirMensagem: false,
            mensagem: '',
            deveExibirErro: false
        }
        this.trocaValoresState = this.trocaValoresState.bind(this)
    }

    exibirMensagem = ({ cor, mensagem }) => {
        this.setState({
            cor,
            mensagem,
            exibirMensagem: true
        })
    }

    atualizarMensagem = devoExibir => {
        this.setState({
            exibirMensagem: devoExibir
        })
    }


    trocaValoresState(e) {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        })
    }

    logar(e) {
        e.preventDefault();
        const { email, senha } = this.state
        if (email && senha) {
            axios.post('http://localhost:1337/login', {
                email: this.state.email,
                senha: this.state.senha
            }).then(resp => {
                localStorage.setItem('Authorization', resp.data.token);
                this.props.history.push("/")
            }
            ).catch(x => {
                let cor = 'vermelho'
                let mensagem = MensagemFlashConst.ERRO.LOGIN_ERRO
                this.exibirMensagem({ cor, mensagem })
            })
        } else {
            let cor = 'vermelho'
            let mensagem = MensagemFlashConst.ERRO.CAMPO_VAZIO
            this.exibirMensagem({ cor, mensagem })
        }
    }

    render() {
        const { exibirMensagem, cor, mensagem } = this.state
        return (

            <React.Fragment>
                <ComponentHeader></ComponentHeader>
                <MensagemFlash atualizarMensagem={this.atualizarMensagem}
                    cor={cor}
                    deveExibirMensagem={exibirMensagem}
                    mensagem={mensagem} segundos={5} />
                <React.Fragment>
                    <div className="container">
                        <div className="row row-100 padding-top-login padding-bot-login">
                            <div className="col-100 col-center center-horizontal">
                                <h1>Login</h1>
                                <input type="text" name="email" id="email" placeholder="Digite o email" onChange={this.trocaValoresState} />
                                <input type="password" name="senha" id="senha" placeholder="Digite a senha" onChange={this.trocaValoresState} />
                                <button className="btn-login" type="button" onClick={this.logar.bind(this)}>Logar</button>
                            </div>
                        </div>
                    </div>
                </React.Fragment>
                <ComponentFooter></ComponentFooter>
            </React.Fragment>
        )
    }
}