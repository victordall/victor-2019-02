import React, { Component } from 'react';
import * as axios from 'axios';
import './css/ListDetalhes.css'
import '../css/container.css'
import '../css/general.css'
import '../css/button.css'
import Cliente from '../models/Cliente'
import ContaCliente from '../models/ContaCliente'
import TipoConta from '../models/TipoConta'
import ComponentHeader from '../components/header/ComponentHeader'
import ComponentFooter from '../components/footer/ComponentFooter'
import ComponentTodasContasClientes from '../components/contaclientes/ComponentTodasContasClientes'
import DetalhesContaC from '../components/contaclientes/DetalhesContaC'
import DetalhesTipoConta from '../components/tipoContas/DetalhesTipoConta'
import DetalhesCliente from '../components/clientes/DetalhesCliente'
import BotaoVoltar from '../components/BotaoVoltar'

export default class ContaClientes extends Component {
    constructor(props) {
        super(props)
        this.state = {
            contasC: [],
            mostrarAll: true,
            mostrarEspe: false,
            contaC: {},
            tipoConta: {},
            cliente: {},
            nomeCliente: ''
        }
        this.getDetails = this.getDetails.bind(this);
        this.voltar = this.voltar.bind(this);
        this.getAllContaC();
    }



    getAllContaC() {
        axios.get('http://localhost:1337/conta/clientes', {
            headers: { Authorization: 'banco-vemser-api-fake' }
        }).then(resp => {
            const ccList = resp.data.cliente_x_conta
                .map(c => new ContaCliente(c.id, c.codigo,
                    new TipoConta(c.tipo.id, c.tipo.nome),
                    new Cliente(c.cliente.id, c.cliente.nome, c.cliente.cpf)));
            this.setState({
                contasC: ccList,
                mostrarAll: true,
                mostrarEspe: false,
            })
        }).catch(x => {
            console.log(x)
        })
    }

    getDetails(id) {
        const { contasC } = this.state;
        const index = contasC.findIndex((a => a.id == id));
        this.setState({
            mostrarAll: false,
            mostrarEspe: true,
            numAgencia: contasC[index].codigo,
            tipoConta: contasC[index].tipoConta,
            cliente: contasC[index].Cliente,
            nomeCliente: contasC[index].Cliente.nome
        })
    }

    voltar() {
        this.getAllContaC();
    }

    render() {
        const { contasC, mostrarAll, mostrarEspe, numAgencia, tipoConta, cliente, nomeCliente } = this.state
        return (
            <React.Fragment>
                <ComponentHeader></ComponentHeader>
                {
                    contasC.length > 0 && mostrarAll ? <ComponentTodasContasClientes contasC={contasC}
                        getDetails={this.getDetails} /> : null
                }
                {
                    mostrarEspe ? <React.Fragment>
                        <DetalhesContaC numAgencia={numAgencia} nomeCliente={nomeCliente} />
                        <hr className="margin-top"></hr>
                        <DetalhesTipoConta tipoConta={tipoConta} />
                        <hr className="margin-top"></hr>
                        <DetalhesCliente cliente={cliente} />
                        <BotaoVoltar voltar={this.voltar} /></React.Fragment> : null
                }
                <ComponentFooter></ComponentFooter>
            </React.Fragment>
        )
    }
}
