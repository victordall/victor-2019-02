import React, { Component } from 'react';
import * as axios from 'axios';
import './css/ListDetalhes.css'
import '../css/container.css'
import '../css/general.css'
import '../css/button.css'
import Cliente from '../models/Cliente'
import Agencia from '../models/Agencia'
import Endereco from '../models/Endereco'
import ComponentHeader from '../components/header/ComponentHeader'
import ComponentFooter from '../components/footer/ComponentFooter'
import ComponentTodosClientes from '../components/clientes/ComponentTodosClientes'
import DetalhesCliente from '../components/clientes/DetalhesCliente'
import DetalhesAgencia from '../components/agencia/DetalhesAgencia'
import BotaoVoltar from '../components/BotaoVoltar'

export default class Clientes extends Component {
    constructor(props) {
        super(props)
        this.state = {
            clientes: [],
            mostrarAll: true,
            mostrarEspe: false,
            cliente: {},
            agencia: {}
        }
        this.getDetails = this.getDetails.bind(this);
        this.voltar = this.voltar.bind(this);
        this.getAllClientes();
    }



    getAllClientes() {
        axios.get('http://localhost:1337/clientes', {
            headers: { Authorization: 'banco-vemser-api-fake' }
        }).then(resp => {
            const clienteList = resp.data.clientes
                .map(c => new Cliente(c.id, c.nome, c.cpf,
                    new Agencia(c.agencia.id, c.agencia.codigo, c.agencia.nome,
                        new Endereco(c.agencia.endereco.logradouro, c.agencia.endereco.numero, c.agencia.endereco.bairro,
                            c.agencia.endereco.cidade, c.agencia.endereco.uf), false)));
            this.setState({
                clientes: clienteList,
                mostrarAll: true,
                mostrarEspe: false,
            })
        }).catch(x => {
            console.log(x)
        })
    }

    getDetails(id) {
        const { clientes } = this.state;
        const index = clientes.findIndex((a => a.id == id));
        const indexAgencia = JSON.parse(localStorage.getItem("listaAgencia")).findIndex(a => a.id == clientes[index].agencia.id); 
        this.setState({
            mostrarAll: false,
            mostrarEspe: true,
            cliente: clientes[index],
            agencia: JSON.parse(localStorage.getItem("listaAgencia"))[indexAgencia]
        })
    }

    voltar() {
        this.getAllClientes();
    }

    render() {
        const { clientes, mostrarAll, mostrarEspe, cliente, agencia } = this.state
        return (
            <React.Fragment>
                <ComponentHeader></ComponentHeader>
                {
                    clientes.length > 0 && mostrarAll ? <ComponentTodosClientes clientes={clientes}
                        getDetails={this.getDetails} /> : null
                }
                {
                    mostrarEspe ? <React.Fragment><DetalhesCliente cliente={cliente} />
                    <hr className="margin-top"></hr>
                    <DetalhesAgencia agencia={agencia} />
                    <BotaoVoltar voltar={this.voltar}/></React.Fragment> : null
                }
                <ComponentFooter></ComponentFooter>
            </React.Fragment>
        )
    }
}
