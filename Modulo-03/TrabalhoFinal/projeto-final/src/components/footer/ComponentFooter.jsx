import React from 'react';
import { Link } from 'react-router-dom'
import '../footer/footer.css'

const ComponentFooter = props => {
    return (
        <React.Fragment>
            <footer class="footer">
                <div class="container">
                    <div class="table">
                        <div class="table-cell text-center">
                            <nav>
                                <ul>
                                    <li>
                                        <Link to="/Agencias">Agências</Link>
                                    </li>
                                    <li>
                                        <Link to="/Clientes">Clientes</Link>
                                    </li>
                                    <li>
                                        <Link to="/TiposDeContas">Tipo de contas</Link>
                                    </li>
                                    <li>
                                        <Link to="/ContaClientes">Conta de clientes</Link>
                                    </li>
                                </ul>
                            </nav>
                            <p className="logo"><b>&copy;BANCO VEMSER</b></p>
                        </div>
                    </div>
                </div>
            </footer>
        </React.Fragment>
    )
}

export default ComponentFooter

