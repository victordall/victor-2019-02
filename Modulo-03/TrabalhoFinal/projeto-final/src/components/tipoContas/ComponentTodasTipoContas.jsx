import React, { Component } from 'react';
import TipoConta from '../../models/TipoConta'

export default class ComponentTodasTipoContas extends Component {
    render() {
        const { contas } = this.props
        return (
            <React.Fragment>
                <div className="container margin-top-50 padding-agencias">
                    <div className="row">
                        <div className="col col-100 col-center">
                            <div className="row">
                                <div className="col col-100">
                                    <h1>Tipos de Contas</h1>
                                </div>
                            </div>
                            <table className="margin-top">
                                <tr>
                                    <th>
                                        AÇÕES
                                   </th>
                                    <th>
                                        NOME
                                   </th>
                                </tr>
                                {contas.map(c => (
                                    <tr>
                                        <td>
                                            <i className="fa fa-info-circle tooltip " onClick={() => this.props.getDetails(c.id)} >
                                                <div class="tooltiptext">Ver detalhes</div>
                                            </i>
                                        </td>
                                        <td>
                                            {c.nome}
                                        </td>
                                    </tr>
                                ))}
                            </table>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }

}

