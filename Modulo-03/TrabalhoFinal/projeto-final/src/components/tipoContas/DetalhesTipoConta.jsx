import React, { Component } from 'react';
import TipoConta from '../../models/TipoConta'

export default class DetalhesTipoConta extends Component {
    render() {
        const { tipoConta } = this.props
        return (
            <React.Fragment>
                <div className="container margin-top-50">
                    <div className="row">
                        <div className="col col-100 col-center">
                            <div className="row">
                                <div className="col col-100">
                                    <h1>Conta {tipoConta.nome}</h1>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col col-50">
                                    <h3 className="dados">ID</h3>
                                </div>
                                <div className="col col-50">
                                    <h3 className="dados">{tipoConta.id}</h3>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col col-50">
                                    <h3 className="dados">Tipo</h3>
                                </div>
                                <div className="col col-50">
                                    <h3 className="dados">{tipoConta.nome}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

