import React, { Component } from 'react';
import Agencia from '../../models/Agencia'
import Endereco from '../../models/Endereco'

export default class DetalhesAgencia extends Component {
    render() {
        const { agencia } = this.props
        return (
            <React.Fragment>
                <div className="container margin-top-50">
                    <div className="row">
                        <div className="col col-100 col-center">
                            <div className="row">
                                <div className="col col-100">
                                    <h1>Agência {agencia.nome}</h1>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col col-50">
                                    <h3 className="dados">Código</h3>
                                </div>
                                <div className="col col-50">
                                    <h3 className="dados">{agencia.codigo}</h3>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col col-50">
                                    <h3 className="dados">Digital</h3>
                                </div>
                                <div className="col col-50">
                                    <h3 className="dados">{agencia.is_digital ? 'SIM' : 'NÃO'}</h3>
                                </div>
                            </div>
                            <hr className="margin-top"></hr>
                            <div className="row">
                                <div className="col col-100 col-center">
                                    <h1>Endereço da agência {agencia.nome}</h1>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col col-50">
                                    <h3 className="dados">Logradouro</h3>
                                </div>
                                <div className="col col-50">
                                    <h3 className="dados">{agencia.endereco.logradouro}</h3>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col col-50">
                                    <h3 className="dados">Número</h3>
                                </div>
                                <div className="col col-50">
                                    <h3 className="dados">{agencia.endereco.numero}</h3>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col col-50">
                                    <h3 className="dados">Bairro</h3>
                                </div>
                                <div className="col col-50">
                                    <h3 className="dados">{agencia.endereco.bairro}</h3>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col col-50">
                                    <h3 className="dados">Cidade</h3>
                                </div>
                                <div className="col col-50">
                                    <h3 className="dados">{agencia.endereco.cidade}</h3>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col col-50">
                                    <h3 className="dados">UF</h3>
                                </div>
                                <div className="col col-50">
                                    <h3 className="dados">{agencia.endereco.uf}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

