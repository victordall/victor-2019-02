import React, { Component } from 'react';
import Agencia from '../../models/Agencia'
import Endereco from '../../models/Endereco'

export default class ComponentTodasAgencias extends Component {
    render() {
        const { agencias, filtrou } = this.props
        return (
            <React.Fragment>
                <div className="container margin-top-50 padding-agencias">
                    <div className="row">
                        <div className="col col-100 col-center">
                            <div className="row">
                                <div className="col col-100">
                                    <h1>Agências</h1>
                                </div>
                            </div>
                            <table className="margin-top">
                                <tr>
                                    <th>
                                        AÇÕES
                                   </th>
                                    <th>
                                        CÓDIGO
                                   </th>
                                    <th>
                                        NOME
                                   </th>
                                    <th>
                                        DIGITAL
                                        {
                                            filtrou ?
                                                <React.Fragment><button className="btn-digital"
                                                    onClick={() => this.props.filtrar(false)}
                                                    type="button">Mostrar Todas</button>
                                                </React.Fragment>
                                                : <React.Fragment><button className="btn-digital"
                                                    onClick={() => this.props.filtrar(true)}
                                                    type="button">Filtrar Digitais</button>
                                                </React.Fragment>
                                        }

                                    </th>
                                </tr>
                                {agencias.map(a => (
                                    <tr>
                                        <td>
                                            <i className="fa fa-info-circle tooltip " onClick={() => this.props.getDetails(a.id)} >
                                                <div class="tooltiptext">Ver detalhes</div>
                                            </i>
                                            <button className="btn-digital"
                                                onClick={() => this.props.tornarDigital(a.id)}
                                                type="button">Alterna virtualidade</button>
                                        </td>
                                        <td>
                                            {a.codigo}
                                        </td>
                                        <td>
                                            {a.nome}
                                        </td>
                                        <td>
                                            {a.is_digital ? 'SIM' : 'NÃO'}
                                        </td>
                                    </tr>
                                ))}
                            </table>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }

}

