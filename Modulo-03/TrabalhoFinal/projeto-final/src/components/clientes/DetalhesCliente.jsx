import React, { Component } from 'react';
import Agencia from '../../models/Agencia'
import Cliente from '../../models/Cliente'
import Endereco from '../../models/Endereco'

export default class DetalhesCliente extends Component {
    render() {
        const { cliente } = this.props
        return (
            <React.Fragment>
                <div className="container margin-top-50">
                    <div className="row">
                        <div className="col col-100 col-center">
                            <div className="row">
                                <div className="col col-100">
                                    <h1>Cliente {cliente.nome}</h1>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col col-50">
                                    <h3 className="dados">CPF</h3>
                                </div>
                                <div className="col col-50">
                                    <h3 className="dados">{cliente.cpf}</h3>
                                </div>
                            </div>
                        </div>                       
                    </div>
                </div>
            </React.Fragment>
        )
    }

}

