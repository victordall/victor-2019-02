import React, { Component } from 'react';
import Agencia from '../../models/Agencia'
import Endereco from '../../models/Endereco'

export default class ComponentTodosClientes extends Component {
    render() {
        const { clientes } = this.props
        return (
            <React.Fragment>
                <div className="container margin-top-50 padding-agencias">
                    <div className="row">
                        <div className="col col-100 col-center">
                            <div className="row">
                                <div className="col col-100">
                                    <h1>Clientes</h1>
                                </div>
                            </div>
                            <table className="margin-top">
                                <tr>
                                    <th>
                                        AÇÕES
                                   </th>
                                    <th>
                                        NOME
                                   </th>
                                    <th>
                                        CPF
                                   </th>
                                </tr>
                                {clientes.map(c => (
                                    <tr>
                                        <td>
                                            <i className="fa fa-info-circle tooltip " onClick={() => this.props.getDetails(c.id)} >
                                                <div class="tooltiptext">Ver detalhes</div>
                                            </i>
                                        </td>
                                        <td>
                                            {c.nome}
                                        </td>
                                        <td>
                                            {c.cpf}
                                        </td>
                                    </tr>
                                ))}
                            </table>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }

}

