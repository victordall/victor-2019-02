import React, { Component } from 'react';
import Agencia from '../../models/Agencia'
import Cliente from '../../models/Cliente'
import Endereco from '../../models/Endereco'

export default class DetalhesContaC extends Component {
    render() {
        const { numAgencia, nomeCliente } = this.props
        return (
            <React.Fragment>
                <div className="container margin-top-50">
                    <div className="row">
                        <div className="col col-100 col-center">
                            <div className="row">
                                <div className="col col-100">
                                    <h1>Conta do Cliente {nomeCliente}</h1>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col col-50">
                                    <h3 className="dados">Nº Agência</h3>
                                </div>
                                <div className="col col-50">
                                    <h3 className="dados">{numAgencia}</h3>
                                </div>
                            </div>
                        </div>                       
                    </div>
                </div>
            </React.Fragment>
        )
    }

}

