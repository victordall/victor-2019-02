import React, { Component } from 'react';
import Agencia from '../../models/Agencia'
import Endereco from '../../models/Endereco'

export default class ComponentTodasContasClientes extends Component {
    render() {
        const { contasC } = this.props
        return (
            <React.Fragment>
                <div className="container margin-top-50 padding-agencias">
                    <div className="row">
                        <div className="col col-100 col-center">
                            <div className="row">
                                <div className="col col-100">
                                    <h1>Conta</h1>
                                </div>
                            </div>
                            <table className="margin-top">
                                <tr>
                                    <th>
                                        AÇÕES
                                   </th>
                                    <th>
                                        Código Agência
                                   </th>
                                    <th>
                                        Tipo da Conta
                                   </th>
                                   <th>
                                       Nome Cliente
                                   </th>
                                </tr>
                                {contasC.map(c => (
                                    <tr>
                                        <td>
                                            <i className="fa fa-info-circle tooltip " onClick={() => this.props.getDetails(c.id)} >
                                                <div class="tooltiptext">Ver detalhes</div>
                                            </i>
                                        </td>
                                        <td>
                                            {c.codigo}
                                        </td>
                                        <td>
                                            {c.tipoConta.nome}
                                        </td>
                                        <td>
                                            {c.Cliente.nome}
                                        </td>
                                    </tr>
                                ))}
                            </table>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }

}

