import React from 'react';
import { Link } from 'react-router-dom'
import '../header/header.css'

const ComponentHeader = props => {
    return (
        <React.Fragment>
            <header className="header">
                <nav className="container clearfix">
                    <b> <a className="logo pull-left" href="Home.html"><b>BANCO VEMSER</b></a>
                        <ul className="pull-right">
                            <li>
                                <Link to="/Agencias">Agências</Link>
                            </li>
                            <li>
                                <Link to="/Clientes">Clientes</Link>
                            </li>
                            <li>
                                <Link to="/TiposDeContas">Tipo de contas</Link>
                            </li>
                            <li>
                                <Link to="/ContaClientes">Conta de clientes</Link>
                            </li>
                        </ul>
                    </b>
                </nav>
            </header>
        </React.Fragment>
    )
}

export default ComponentHeader

