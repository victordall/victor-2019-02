import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Login from './pages/Login';
import Agencias from './pages/Agencias'
import Clientes from './pages/Clientes'
import TiposDeContas from './pages/TiposDeContas'
import ContaClientes from './pages/ContaClientes'
import Home from './pages/Home'
import { PrivateRoute } from './components/PrivateRoute';

export default class App extends Component {
  render() {
    return (
      <Router>
        <React.Fragment>
          <PrivateRoute path="/" exact component={Home} />
          <PrivateRoute path='/Agencias' exact component={Agencias} />
          <PrivateRoute path='/Clientes' exact component={Clientes} />
          <PrivateRoute path='/TiposDeContas' exact component={TiposDeContas} />
          <PrivateRoute path='/ContaClientes' exact component={ContaClientes} />
          <PrivateRoute path='/Home' exact component={Home} />
          <Route path="/login" component={Login} />
        </React.Fragment>
      </Router>
    );
  }
}
