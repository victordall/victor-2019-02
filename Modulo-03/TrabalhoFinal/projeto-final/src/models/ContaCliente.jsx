import TipoConta from './TipoConta'
import Cliente from './Cliente'

export default class ContaCliente {
    constructor(id, codigo, TipoConta, Cliente) {
        this.id = id
        this.codigo = codigo
        this.tipoConta = TipoConta
        this.Cliente = Cliente
    }
}