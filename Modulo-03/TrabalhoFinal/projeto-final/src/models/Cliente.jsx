import Agencia from './Agencia'

export default class Cliente {
    constructor(id, nome, cpf, Agencia) {
        this.id = id
        this.nome = nome
        this.cpf = cpf
        this.agencia = Agencia
    }
}