export default class Endereco {
    constructor(logradouro, numero, bairro, cidade, uf) {
        this.logradouro = logradouro
        this.numero = numero
        this.bairro = bairro
        this.cidade = cidade
        this.uf = uf
    }
}