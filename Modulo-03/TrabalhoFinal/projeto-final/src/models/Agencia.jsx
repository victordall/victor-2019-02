import Endereco from './Endereco'

export default class Agencia {
    constructor( id, codigo, nome, Endereco, is_digital ) {
        this.id = id
        this.codigo = codigo
        this.nome = nome        
        this.endereco = Endereco
        this.is_digital = is_digital
    }
}