-- Bancos por País
SELECT BANCOS.NOME, PAISES.NOME FROM BANCOS 
JOIN AGENCIAS ON ( BANCOS.ID = AGENCIAS.FK_ID_BANCOS)
JOIN ENDERECOS ON (AGENCIAS.FK_ID_ENDERECOS = ENDERECOS.ID)
JOIN BAIRROS ON (ENDERECOS.FK_ID_BAIRRO = BAIRROS.ID)
JOIN CIDADES ON (BAIRROS.FK_ID_CIDADE = CIDADES.ID)
JOIN ESTADOS ON (CIDADES.FK_ID_ESTADO = ESTADOS.ID)
JOIN PAISES ON (ESTADOS.FK_ID_PAIS = PAISES.ID);

-- Completo de agência (Dados de agência, correntistas, clientes e dados dos clientes)

SELECT * FROM AGENCIAS RIGHT JOIN ENDERECOS 
ON (AGENCIAS.FK_ID_ENDERECOS = ENDERECOS.ID)
RIGHT JOIN BAIRROS ON (ENDERECOS.FK_ID_BAIRRO = BAIRROS.ID)
RIGHT JOIN CIDADES ON (BAIRROS.FK_ID_CIDADE = CIDADES.ID)
RIGHT JOIN ESTADOS ON (CIDADES.FK_ID_ESTADO = ESTADOS.ID)
RIGHT JOIN PAISES ON (ESTADOS.FK_ID_PAIS = PAISES.ID)
RIGHT JOIN AGENCIAS_X_CORRENTISTAS AG_CO ON (AGENCIAS.ID = AG_CO.FK_ID_AGENCIA)
RIGHT JOIN CORRENTISTAS ON (AG_CO.FK_ID_CORRENTISTA = CORRENTISTAS.ID)
RIGHT JOIN CLIENTES_X_CORRENTISTAS CXC ON (CORRENTISTAS.ID = CXC.FK_ID_CORRENTISTA)
RIGHT JOIN CLIENTES ON (CXC.FK_ID_CLIENTE = CLIENTES.ID)
RIGHT JOIN ENDERECOS ENDC ON (CLIENTES.FK_ID_ENDERECO = ENDC.ID) 
RIGHT JOIN BAIRROS BC ON(ENDC.FK_ID_BAIRRO = BC.ID)
RIGHT JOIN CIDADES CB ON (BC.FK_ID_CIDADE = CB.ID)
RIGHT JOIN ESTADOS EC ON (CB.FK_ID_ESTADO = EC.ID)
RIGHT JOIN PAISES PC ON (EC.FK_ID_PAIS = PC.ID);

-- Geral de clientes cadastrados no nosso sistema (lista de todos clientes com seus dados)

SELECT * FROM CLIENTES JOIN ENDERECOS 
ON (CLIENTES.FK_ID_ENDERECO = ENDERECOS.ID)
INNER JOIN CLIENTES_X_CORRENTISTAS ON (CLIENTES.ID = CLIENTES_X_CORRENTISTAS.FK_ID_CLIENTE)
JOIN CORRENTISTAS ON (CLIENTES_X_CORRENTISTAS.FK_ID_CORRENTISTA = CORRENTISTAS.ID)
INNER JOIN AGENCIAS_X_CORRENTISTAS ON (CORRENTISTAS.ID = AGENCIAS_X_CORRENTISTAS.FK_ID_CORRENTISTA)
JOIN AGENCIAS ON (AGENCIAS_X_CORRENTISTAS.FK_ID_AGENCIA = AGENCIAS.ID)
JOIN ENDERECOS ON (AGENCIAS.FK_ID_ENDERECOS = ENDERECOS.ID)
JOIN BAIRROS ON (ENDERECOS.FK_ID_BAIRRO = BAIRROS.ID)
JOIN CIDADES ON (BAIRROS.FK_ID_CIDADE = CIDADES.ID)
JOIN ESTADOS ON (CIDADES.FK_ID_ESTADO = ESTADOS.ID)
JOIN PAISES ON (ESTADOS.FK_ID_PAIS = PAISES.ID);