CREATE TABLE PAISES(
    ID INTEGER NOT NULL PRIMARY KEY,
    NOME VARCHAR(100) NOT NULL
); 

CREATE SEQUENCE PAISES_SEQ START WITH 1 INCREMENT BY 1;

    
CREATE TABLE ESTADOS (
    ID INTEGER NOT NULL PRIMARY KEY,
    NOME VARCHAR(100) NOT NULL,
    UF CHAR(2) NOT NULL,
    FK_ID_PAIS INTEGER NOT NULL,
    FOREIGN KEY (FK_ID_PAIS) REFERENCES PAISES(ID)
    );
    
CREATE SEQUENCE ESTADOS_SEQ START WITH 1 INCREMENT BY 1;

CREATE TABLE CIDADES (
    ID INTEGER NOT NULL PRIMARY KEY,
    NOME VARCHAR(100) NOT NULL,
    FK_ID_ESTADO INTEGER NOT NULL,
    FOREIGN KEY (FK_ID_ESTADO) REFERENCES ESTADOS(ID)
    );
    
CREATE SEQUENCE CIDADES_SEQ START WITH 1 INCREMENT BY 1;
    
CREATE TABLE BAIRROS (
        ID INTEGER NOT NULL PRIMARY KEY,
        NOME VARCHAR(100) NOT NULL,
        FK_ID_CIDADE INTEGER NOT NULL,
        FOREIGN KEY (FK_ID_CIDADE) REFERENCES CIDADES (ID)
);

CREATE SEQUENCE BAIRROS_SEQ START WITH 1 INCREMENT BY 1;

CREATE TABLE ENDERECOS (
    ID INTEGER NOT NULL PRIMARY KEY,
    LOGRADOURO VARCHAR(255) NOT NULL,
    NUMERO INTEGER NOT NULL,
    COMPLEMENTO VARCHAR(100),
    FK_ID_BAIRRO INTEGER NOT NULL,
    FOREIGN KEY (FK_ID_BAIRRO) REFERENCES BAIRROS (ID)
    );
    
CREATE SEQUENCE ENDERECOS_SEQ START WITH 1 INCREMENT BY 1;

CREATE TABLE BANCOS (
    ID INTEGER NOT NULL PRIMARY KEY,
    CODIGO INTEGER NOT NULL,
    NOME VARCHAR(100) NOT NULL
);

CREATE SEQUENCE BANCOS_SEQ START WITH 1 INCREMENT BY 1;
  
CREATE TABLE AGENCIAS (
    ID INTEGER NOT NULL PRIMARY KEY,
    CODIGO INTEGER NOT NULL,
    NOME VARCHAR(100) NOT NULL,
    FK_ID_BANCOS INTEGER NOT NULL,
    FK_ID_ENDERECOS INTEGER NOT NULL,
    FOREIGN KEY (FK_ID_BANCOS) REFERENCES BANCOS(ID),
    FOREIGN KEY (FK_ID_ENDERECOS) REFERENCES ENDERECOS(ID)
    );

CREATE SEQUENCE AGENCIAS_SEQ START WITH 1 INCREMENT BY 1;

CREATE TABLE CORRENTISTAS (
    ID INTEGER NOT NULL PRIMARY KEY,
    RAZAO_SOCIAL VARCHAR(100),
    CNPJ VARCHAR(14) NOT NULL,
    TIPO VARCHAR(15) CHECK(TIPO IN ('Conjunta', 'PJ', 'PF', 'Investimento'))
    );

CREATE SEQUENCE CORRENTISTAS_SEQ START WITH 1 INCREMENT BY 1;

CREATE TABLE AGENCIAS_X_CORRENTISTAS (
    ID INTEGER NOT NULL PRIMARY KEY,
    FK_ID_AGENCIA INTEGER NOT NULL,
    FK_ID_CORRENTISTA INTEGER NOT NULL,
    FOREIGN KEY (FK_ID_AGENCIA) REFERENCES AGENCIAS(ID),
    FOREIGN KEY (FK_ID_CORRENTISTA) REFERENCES CORRENTISTAS(ID)
);

CREATE SEQUENCE AGENCIAS_X_CORRENTISTAS_SEQ START WITH 1 INCREMENT BY 1;

CREATE TABLE TELEFONES (
    ID INTEGER NOT NULL PRIMARY KEY,
    NUMERO VARCHAR(20),
    TIPO VARCHAR(15) CHECK(TIPO IN ('FIXO', 'CELULAR'))
    );
	
CREATE SEQUENCE TELEFONES_SEQ START WITH 1 INCREMENT BY 1;

CREATE TABLE CLIENTES 
(
    ID INTEGER NOT NULL PRIMARY KEY,
    NOME VARCHAR(100) NOT NULL,
    CPF CHAR(11) UNIQUE NOT NULL,
    FK_ID_ENDERECO INTEGER NOT NULL,
    FOREIGN KEY (FK_ID_ENDERECO) REFERENCES ENDERECOS(ID),
    RG VARCHAR(15) NOT NULL,
    DATA_NASCIMENTO CHAR(10) NOT NULL,
    ESTADO_CIVIL VARCHAR(15) CHECK(ESTADO_CIVIL IN ('SOLTEIRO', 'CASADO', 'DIVORCIADO', 'VIUVO')),
    CONJUGUE VARCHAR(100)
    );

CREATE SEQUENCE CLIENTES_SEQ START WITH 1 INCREMENT BY 1;

CREATE TABLE EMAILS 
(
    ID INTEGER NOT NULL PRIMARY KEY,
    EMAIL VARCHAR(100) NOT NULL,
    FK_ID_CLIENTE INTEGER NOT NULL,
    FOREIGN KEY (FK_ID_CLIENTE) REFERENCES CLIENTES(ID)
    );

CREATE SEQUENCE EMAILS_SEQ START WITH 1 INCREMENT BY 1;

CREATE TABLE CLIENTES_X_TELEFONES 
(
    ID INTEGER NOT NULL PRIMARY KEY,
    FK_ID_CLIENTE INTEGER NOT NULL,
    FOREIGN KEY (FK_ID_CLIENTE) REFERENCES CLIENTES(ID),
    FK_ID_TELEFONE INTEGER NOT NULL,
    FOREIGN KEY (FK_ID_TELEFONE) REFERENCES TELEFONES(ID)
    );

CREATE SEQUENCE CLIENTES_X_TELEFONES_SEQ START WITH 1 INCREMENT BY 1;

CREATE TABLE CLIENTES_X_CORRENTISTAS 
(
    ID INTEGER NOT NULL PRIMARY KEY,
    FK_ID_CLIENTE INTEGER NOT NULL,
    FOREIGN KEY (FK_ID_CLIENTE) REFERENCES CLIENTES(ID),
    FK_ID_CORRENTISTA INTEGER NOT NULL,
    FOREIGN KEY (FK_ID_CORRENTISTA) REFERENCES CORRENTISTAS(ID)
    );

CREATE SEQUENCE CLIENTES_X_CORRENTISTAS_SEQ START WITH 1 INCREMENT BY 1;
 
--CORREÇÕES
ALTER TABLE CORRENTISTAS DROP COLUMN TIPO;
ALTER TABLE CORRENTISTAS ADD TIPO VARCHAR(15) CHECK(TIPO IN ('CONJUNTA', 'PJ', 'PF', 'INVESTIMENTO')) NOT NULL;
ALTER TABLE CORRENTISTAS MODIFY CNPJ NULL;
ALTER TABLE ESTADOS DROP COLUMN UF;


    
