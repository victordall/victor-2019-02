import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args){
        Connection conn = Connector.connect();
        try{
            //PAISES
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'PAISES'")
                    .executeQuery();
            if(!rs.next()){
                //CRIAR TABLE SE N TEM
                conn.prepareStatement("CREATE TABLE PAISES(\n"
                    + "ID INTEGER NOT NULL PRIMARY KEY, \n"
                        + "NOME VARCHAR(100) NOT NULL \n"
                        + ")\n").execute();
                //INSERT INCIAL
                PreparedStatement pst = conn.prepareStatement("insert into paises(id, nome) " +
                        "values (PAISES_SEQ.nextval, ?)");
                pst.setString(1, "Brasil");
                pst.executeUpdate();
            }

            rs = conn.prepareStatement("select * from paises").executeQuery();
            while (rs.next()){
                System.out.println(String.format("Nome do pais: %s", rs.getString("nome")));
            }

            //ESTADOS
            rs = conn.prepareStatement("select tname from tab where tname = 'ESTADOS'")
                    .executeQuery();
            if(!rs.next()){
                conn.prepareStatement("CREATE TABLE ESTADOS (\n"
                        + "ID INTEGER NOT NULL PRIMARY KEY, \n"
                        + "NOME VARCHAR(100) NOT NULL \n"
                        + ")\n").execute();
                PreparedStatement pst = conn.prepareStatement("insert into ESTADOS(id, nome) " +
                        "values (ESTADOS_SEQ.nextval, ?)");
                pst.setString(1, "Pará");
                pst.executeUpdate();
            }

            rs = conn.prepareStatement("select * from estados").executeQuery();
            while (rs.next()){
                System.out.println(String.format("Nome do estado: %s", rs.getString("nome")));
            }

            //CIDADES
            rs = conn.prepareStatement("select tname from tab where tname = 'CIDADES'")
                    .executeQuery();
            if(!rs.next()){
                conn.prepareStatement("CREATE TABLE CIDADES (\n"
                        + "ID INTEGER NOT NULL PRIMARY KEY, \n"
                        + "NOME VARCHAR(100) NOT NULL \n"
                        + ")\n").execute();
                PreparedStatement  pst = conn.prepareStatement("insert into CIDADES(id, nome) " +
                        "values (CIDADES_SEQ.nextval, ?)");
                pst.setString(1, "Cidade do pará");
                pst.executeUpdate();

            }

            rs = conn.prepareStatement("select * from cidades").executeQuery();
            while (rs.next()){
                System.out.println(String.format("Nome da cidade: %s", rs.getString("nome")));
            }

            //BAIRROS
            rs = conn.prepareStatement("select tname from tab where tname = 'BAIRROS'")
                    .executeQuery();
            if(!rs.next()){
                conn.prepareStatement("CREATE TABLE BAIRROS (\n"
                        + "ID INTEGER NOT NULL PRIMARY KEY, \n"
                        + "NOME VARCHAR(100) NOT NULL \n"
                        + ")\n").execute();
                PreparedStatement pst = conn.prepareStatement("insert into bairros(id, nome) " +
                        "values (BAIRROS_SEQ.nextval, ?)");
                pst.setString(1, "Bairro da cidade do pará");
                pst.executeUpdate();
            }

            rs = conn.prepareStatement("select * from bairros").executeQuery();
            while (rs.next()){
                System.out.println(String.format("Nome do bairro: %s", rs.getString("nome")));
            }
        }catch (SQLException ex){
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "ERRO: ", ex);
        }
    }
}
