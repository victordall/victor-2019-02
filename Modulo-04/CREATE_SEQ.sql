CREATE SEQUENCE PAISES_SEQ START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE ESTADOS_SEQ START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE CIDADES_SEQ START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE BAIRROS_SEQ START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE ENDERECOS_SEQ START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE BANCOS_SEQ START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE AGENCIAS_SEQ START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE CORRENTISTAS_SEQ START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE AGENCIAS_X_CORRENTISTAS_SEQ START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE TELEFONES_SEQ START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE CLIENTES_SEQ START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE EMAILS_SEQ START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE CLIENTES_X_TELEFONES_SEQ START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE CLIENTES_X_CORRENTISTAS_SEQ START WITH 1 INCREMENT BY 1;

