package br.com.dbccompany.bancodigital.Services;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.*;

import br.com.dbccompany.bancodigital.Dao.TelefonesDAO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Telefones;

public class TelefonesService {

	private static final TelefonesDAO DAO = new TelefonesDAO();
	private static final Logger LOG = Logger.getLogger(PaisesServices.class.getName());
	
	public void salvarPaises(Telefones t) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction trans = HibernateUtil.getSession().getTransaction();
		
		try {
			DAO.criar(t);
			if(started) {
				trans.commit();
			}
		}
		catch (Exception e) {
			trans.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
}
