package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;


@Entity
@SequenceGenerator (allocationSize = 1, name = "CORRENTISTAS_SEQ", sequenceName = "CORRENTISTAS_SEQ")
public class Correntistas extends AbstractEntity{
	
	@Id
	@GeneratedValue ( generator = "CORRENTISTAS_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer idCorrentista;
	private String razaoSocial;
	private String cnpj;
	private double saldo;
	private enum tipo { CORRENTISTA, PJ, PF, INVESTIMENTO }
	
	@ManyToMany (cascade = CascadeType.ALL)
	@JoinTable( name = "correntistas_x_clientes",
			joinColumns = { @JoinColumn( name = "id_correntista")},
			inverseJoinColumns = {@JoinColumn(name = "id_cliente")})
	private List<Clientes> clientes = new ArrayList<>();
	
	@ManyToMany (mappedBy = "correntistas")
	private List<Agencias> agencias = new ArrayList<>();
	
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	@Override
	public Integer getId() {
		return idCorrentista;
	}
	public Integer getIdCorrentista() {
		return idCorrentista;
	}
	public void setIdCorrentista(Integer idCorrentista) {
		this.idCorrentista = idCorrentista;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public List<Clientes> getClientes() {
		return clientes;
	}
	public void setClientes(List<Clientes> clientes) {
		this.clientes = clientes;
	}
	public List<Agencias> getAgencias() {
		return agencias;
	}
	public void setAgencias(List<Agencias> agencias) {
		this.agencias = agencias;
	};
}
