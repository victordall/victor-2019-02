package br.com.dbccompany.bancodigital.Services;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.*;

import br.com.dbccompany.bancodigital.Dao.ClientesDAO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Entity.Enderecos;
import br.com.dbccompany.bancodigital.Entity.EstadoCivil;

public class ClientesServices {

	private static final ClientesDAO DAO = new ClientesDAO();
	private static final Logger LOG = Logger.getLogger(PaisesServices.class.getName());
	
	public void salvarClientes(ClientesDTO dto) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction trans = HibernateUtil.getSession().getTransaction();
		Clientes c = DAO.parseFrom(dto);
		try {
			Clientes res = null;
			if(dto.getId() != null) {
				res =  DAO.buscar(dto.getId());
			}			
			if(res == null) {
				DAO.criar(c);
			}
			else {
				c.setIdCliente(res.getId());
				DAO.atualizar(c);
			}
			if(started) {
				trans.commit();
			}
		}
		catch (Exception e) {
			trans.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}

	public ClientesDTO criarCliente(String nome, String rg, String dataNascimento, EstadoCivil estadoCivil, String cpf, String conjugue) {
		ClientesDTO dto = new ClientesDTO();
		dto.setConjugue(conjugue);
		dto.setCpf(cpf);
		dto.setRg(rg);
		dto.setDataNascimento(dataNascimento);
		dto.setNome(nome);
		dto.setEstadoCivil(estadoCivil);
		return dto;
	}
}
