package br.com.dbccompany.bancodigital.Entity;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.*;

@SuppressWarnings("deprecation")
public class HibernateUtil {

	private static final SessionFactory sessionFactory;
	private static final Session session;
		
	static {
		try {
			sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
			session = sessionFactory.openSession();
		}catch (Throwable t) {
			System.err.println("FAIL: " + t);
			throw new ExceptionInInitializerError(t);
		}
		
	}
	
	public static Session getSession() {
		return session;
	}
	
	public static boolean beginTransaction() {
		Transaction transacion = session.getTransaction();
		if(transacion == null || !transacion.isActive()) {
			transacion = session.beginTransaction();
			return true;
		}
		return false;
	}
	
	public static SessionFactory getSessionfactory() {
		return sessionFactory;
	}
	
}
