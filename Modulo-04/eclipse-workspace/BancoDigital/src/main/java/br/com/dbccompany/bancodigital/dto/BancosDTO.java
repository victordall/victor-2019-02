package br.com.dbccompany.bancodigital.dto;

import java.util.ArrayList;
import java.util.List;

import br.com.dbccompany.bancodigital.Entity.Agencias;

public class BancosDTO {

	private Integer idBanco;
	private Integer codigo;
	private String nome;
	private List<Agencias> agencias = new ArrayList<>();
	public Integer getIdBanco() {
		return idBanco;
	}
	public void setIdBanco(Integer idBanco) {
		this.idBanco = idBanco;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public List<Agencias> getAgencias() {
		return agencias;
	}
	public void setAgencias(List<Agencias> agencias) {
		this.agencias = agencias;
	}
}
