package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.persistence.SequenceGenerator;


@Entity
@SequenceGenerator (allocationSize = 1, name = "TELEFONES_SEQ", sequenceName = "TELEFONES_SEQ")
public class Telefones extends AbstractEntity{

	@Id
	@GeneratedValue ( generator = "TELEFONES_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer idTelefone;
	private String numero;
	@Enumerated(EnumType.STRING)
	private TelefonesType tipo;
	
	@ManyToMany (cascade = CascadeType.ALL)
	@JoinTable( name = "telefones_x_clientes",
			joinColumns = { @JoinColumn( name = "id_telefones")},
			inverseJoinColumns = {@JoinColumn(name = "id_clientes")})
	private List<Clientes> clientes = new ArrayList<>();
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public TelefonesType getTipo() {
		return tipo;
	}
	public void setTipo(TelefonesType tipo) {
		this.tipo = tipo;
	}
	@Override
	public Integer getId() {
		return idTelefone;
	}
}
