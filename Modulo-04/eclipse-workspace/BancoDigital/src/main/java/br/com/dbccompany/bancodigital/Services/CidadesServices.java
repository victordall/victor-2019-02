package br.com.dbccompany.bancodigital.Services;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.*;

import br.com.dbccompany.bancodigital.Dao.CidadesDAO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.dto.CidadesDTO;
import br.com.dbccompany.bancodigital.dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Entity.Estados;

public class CidadesServices {

	private static final CidadesDAO DAO = new CidadesDAO();
	private static final Logger LOG = Logger.getLogger(PaisesServices.class.getName());
	
	public void salvarCidade(CidadesDTO cdto) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction trans = HibernateUtil.getSession().getTransaction();
		Cidades c = DAO.parseFrom(cdto);
		try {
			Cidades cidRes = null;
			if(cdto.getIdCidade() != null) {
				cidRes =  DAO.buscar(cdto.getIdCidade());
			}			
			if(cidRes == null) {
				DAO.criar(c);
			}
			else {
				c.setIdCidade(cidRes.getId());
				DAO.atualizar(c);
			}
			if(started) {
				trans.commit();
			}
		}
		catch (Exception e) {
			trans.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
	public Cidades buscarPorNome(CidadesDTO dto) {
		String nome = dto.getNome();
		Cidades c = null;
		if(nome != null && !nome.equals("")) {
			c = DAO.buscarPorNome(nome);
		}
		return c;
	}
}
