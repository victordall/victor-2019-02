package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;


@Entity
@SequenceGenerator (allocationSize = 1, name = "AGENCIAS_SEQ", sequenceName = "AGENCIAS_SEQ")
public class Agencias extends AbstractEntity {

	@Id
	@GeneratedValue ( generator = "AGENCIAS_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer idAgencia;
	private Integer codigo;
	private String nome;
	
	@ManyToOne
	@JoinColumn(name = "fk_id_banco")
	private Bancos banco;
	
	@OneToOne
	@JoinColumn(name = "fk_id_endereco")
	private Enderecos endereco;
	
	@ManyToMany (cascade = CascadeType.ALL)
	@JoinTable( name = "agencias_x_correntistas",
			joinColumns = { @JoinColumn( name = "id_agencia")},
			inverseJoinColumns = {@JoinColumn(name = "id_correntista")})
	private List<Correntistas> correntistas = new ArrayList<>();
	
	

	public List<Correntistas> getCorrentistas() {
		return correntistas;
	}
	public void setCorrentistas(List<Correntistas> correntistas) {
		this.correntistas = correntistas;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Bancos getBanco() {
		return banco;
	}
	public void setBanco(Bancos banco) {
		this.banco = banco;
	}
	public Enderecos getEndereco() {
		return endereco;
	}
	public void setEndereco(Enderecos endereco) {
		this.endereco = endereco;
	}
	@Override
	public Integer getId() {
		return idAgencia;
	}
	public void setIdAgencia(Integer idAgencia) {
		this.idAgencia = idAgencia;
	}
	
	
}
