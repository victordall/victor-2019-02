package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Entity.Estados;
import br.com.dbccompany.bancodigital.Entity.Paises;
import br.com.dbccompany.bancodigital.dto.EstadosDTO;
import br.com.dbccompany.bancodigital.dto.PaisesDTO;;

public class EstadosDAO extends AbstractDAO<Estados> {

	public Estados parseFrom(EstadosDTO dto) {
		Estados est = null;
		if(dto.getIdEstado() != null) {
			est = buscar(dto.getIdEstado());
		}else {
			est = new Estados();			
		}	
		PaisesDAO pDAO = new PaisesDAO();;
		Paises p = pDAO.parseFrom(dto.getPais());

		if(p != null) {
			est.setPais(p);
		}
		est.setNome(dto.getNome());
		return est;
	}
	
	@Override
	protected Class<Estados> getEntityClass() {
		return Estados.class;
	}
}
