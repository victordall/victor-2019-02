package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Entity.Bairros;
import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.dto.BairrosDTO;
import br.com.dbccompany.bancodigital.dto.CidadesDTO;

public class BairrosDAO extends AbstractDAO<Bairros> {

	public Bairros parseFrom(BairrosDTO dto) {
		Bairros bairro = null;
		if(dto.getIdBairro() != null) {
			bairro = buscar(dto.getIdBairro());
		}
		else {
			bairro = new Bairros();			
		}		
		CidadesDAO cDAo = new CidadesDAO();
		Cidades c = cDAo.parseFrom(dto.getCidade());
		if(c != null) {
			bairro.setCidade(c);
		}
		bairro.setNome(dto.getNome());
		return bairro;
	}
	
	@Override
	protected Class<Bairros> getEntityClass() {
		return Bairros.class;
	}

}
