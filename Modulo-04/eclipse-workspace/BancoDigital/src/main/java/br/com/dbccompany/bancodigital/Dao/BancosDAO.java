package br.com.dbccompany.bancodigital.Dao;

import org.hibernate.Session;

import br.com.dbccompany.bancodigital.Entity.Bancos;
import br.com.dbccompany.bancodigital.Entity.Estados;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.dto.BancosDTO;

public class BancosDAO extends AbstractDAO<Bancos>{

	public Bancos parseFrom(BancosDTO dto) {
		Bancos b = null;
		b = dto.getIdBanco() != null ? buscar(dto.getIdBanco()) : new Bancos();
		return b;
	}
	
	public Bancos buscarPorCodigo(Integer codigo) {
		Session session = HibernateUtil.getSession();
		return (Bancos) session.createQuery("select e from " + getEntityClass().getSimpleName()
				+ " e where codigo = " + codigo).uniqueResult();
	} 
	
	@Override
	protected Class<Bancos> getEntityClass() {
		return Bancos.class;
	}

}
