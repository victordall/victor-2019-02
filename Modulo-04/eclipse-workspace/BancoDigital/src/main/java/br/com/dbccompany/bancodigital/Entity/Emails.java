package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;


@Entity
@SequenceGenerator (allocationSize = 1, name = "EMAILS_SEQ", sequenceName = "EMAILS_SEQ")
public class Emails extends AbstractEntity{

	@Id
	@GeneratedValue ( generator = "EMAILS_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer idEmails;
	private String email;
	private enum tipo { FIXO, CELULAR }	
	@ManyToOne
	@JoinColumn(name = "fk_id_cliente")
	private Clientes cliente;
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Clientes getCliente() {
		return cliente;
	}
	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}
	@Override
	public Integer getId() {
		return idEmails;
	}
}
