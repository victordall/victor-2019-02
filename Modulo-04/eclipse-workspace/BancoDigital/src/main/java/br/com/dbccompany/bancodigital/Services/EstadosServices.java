
package br.com.dbccompany.bancodigital.Services;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.EstadosDAO;
import br.com.dbccompany.bancodigital.Entity.Estados;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Paises;
import br.com.dbccompany.bancodigital.dto.EstadosDTO;
import br.com.dbccompany.bancodigital.dto.PaisesDTO;

public class EstadosServices {

	private static final EstadosDAO DAO = new EstadosDAO();
	private static final Logger LOG = Logger.getLogger(PaisesServices.class.getName());
	
	public void salvarEstados(EstadosDTO dto) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction trans = HibernateUtil.getSession().getTransaction();
		Estados est = DAO.parseFrom(dto);
		try {
			Estados estadoRes = null;
			if(dto.getIdEstado() != null) {
				estadoRes =  DAO.buscar(dto.getIdEstado());
			}			
			if(estadoRes == null) {
				DAO.criar(est);
			}
			else {
				est.setIdEstado(estadoRes.getId());
				DAO.atualizar(est);
			}
			if(started) {
				trans.commit();
			}
		}
		catch (Exception e) {
			trans.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}

	public Estados buscarPorNome(EstadosDTO dto) {
		String nome = dto.getNome();
		Estados e = null;
		if(nome != null && !nome.equals("")) {
			e = DAO.buscarPorNome(nome);
		}
		return e;
	}
}

