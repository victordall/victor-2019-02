package br.com.dbccompany.bancodigital.dto;

public class CidadesDTO {

	private Integer idCidade;
	private String nome;
	
	private EstadosDTO Estado;

	public Integer getIdCidade() {
		return idCidade;
	}

	public void setIdCidade(Integer idCidade) {
		this.idCidade = idCidade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public EstadosDTO getEstado() {
		return Estado;
	}

	public void setEstado(EstadosDTO estado) {
		Estado = estado;
	}
}
