package br.com.dbccompany.bancodigital.Dao;

import org.hibernate.Session;

import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Paises;
import br.com.dbccompany.bancodigital.dto.PaisesDTO;

public class PaisesDAO extends AbstractDAO<Paises> {

	public Paises parseFrom(PaisesDTO dto) {
		Paises p = null;
		if(dto.getIdPaises() != null) {
			p = buscar(dto.getIdPaises());
		}else {
			p = new Paises();
		}
		p.setNome(dto.getNome());
		return p;
	}
	
	@Override
	protected Class<Paises> getEntityClass() {
		return Paises.class;
	}
	
}
