package br.com.dbccompany.bancodigital.dto;

public class BairrosDTO {
	private Integer idBairro;
	private String nome;
	
	private CidadesDTO cidade;

	public Integer getIdBairro() {
		return idBairro;
	}

	public void setIdBairro(Integer idBairro) {
		this.idBairro = idBairro;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public CidadesDTO getCidade() {
		return cidade;
	}

	public void setCidade(CidadesDTO cidade) {
		this.cidade = cidade;
	}
}
