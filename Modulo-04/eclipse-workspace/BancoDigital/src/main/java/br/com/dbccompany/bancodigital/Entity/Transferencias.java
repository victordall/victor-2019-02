package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;


@Entity
@SequenceGenerator (allocationSize = 1, name = "TRANSFERENCIAS_SEQ", sequenceName = "TRANSFERENCIAS_SEQ")
public class Transferencias extends AbstractEntity{
	
	@Id
	@GeneratedValue ( generator = "TRANSFERENCIAS_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer idTransferencia;
	private double valor;
	private Correntistas beneficiadoId;
	private Correntistas pagador;
	
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public Correntistas getBeneficiadoId() {
		return beneficiadoId;
	}
	public void setBeneficiadoId(Correntistas beneficiadoId) {
		this.beneficiadoId = beneficiadoId;
	}
	public Correntistas getPagador() {
		return pagador;
	}
	public void setPagador(Correntistas pagador) {
		this.pagador = pagador;
	}
	
	@Override
	public Integer getId() {
		return idTransferencia;
	}	
}
