package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Entity.Estados;
import br.com.dbccompany.bancodigital.dto.ClientesDTO;

public class ClientesDAO extends AbstractDAO<Clientes>{

	public Clientes parseFrom(ClientesDTO dto) {
		Clientes c = null;
		
		c = dto.getId() != null ? buscar(dto.getId()) : new Clientes();
		
		c.setConjugue(dto.getConjugue());
		c.setCpf(dto.getCpf());
		c.setRg(dto.getRg());
		c.setDataNascimento(dto.getDataNascimento());
		c.setNome(dto.getNome());
		c.setEstadoCivil(dto.getEstadoCivil());
		return c;
	}
	
	
	@Override
	protected Class<Clientes> getEntityClass() {
		return Clientes.class;
	}

}
