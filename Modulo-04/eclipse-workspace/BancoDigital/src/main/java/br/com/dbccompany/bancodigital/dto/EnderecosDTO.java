package br.com.dbccompany.bancodigital.dto;

public class EnderecosDTO {

	private Integer idEndereco;
	private String logradouro;
	private String complemento;
	private Integer numero;
	
	private BairrosDTO bairro;

	
	public Integer getIdEndereco() {
		return idEndereco;
	}

	public void setIdEndereco(Integer idEndereco) {
		this.idEndereco = idEndereco;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public BairrosDTO getBairro() {
		return bairro;
	}

	public void setBairro(BairrosDTO bairro) {
		this.bairro = bairro;
	}
}
