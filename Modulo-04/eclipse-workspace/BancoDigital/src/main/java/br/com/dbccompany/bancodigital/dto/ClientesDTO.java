package br.com.dbccompany.bancodigital.dto;

import br.com.dbccompany.bancodigital.Entity.Enderecos;
import br.com.dbccompany.bancodigital.Entity.EstadoCivil;

public class ClientesDTO {

	private Integer id;
	private String nome;
	private String cpf;
	private String rg;
	private String dataNascimento;
	private EstadoCivil estadoCivil;
	private String conjugue;
	
	private EnderecosDTO endereco;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getConjugue() {
		return conjugue;
	}

	public void setConjugue(String conjugue) {
		this.conjugue = conjugue;
	}

	public EnderecosDTO getEndereco() {
		return endereco;
	}

	public void setEndereco(EnderecosDTO endereco) {
		this.endereco = endereco;
	}
}
