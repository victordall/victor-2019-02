package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Entity.Correntistas;
import br.com.dbccompany.bancodigital.Entity.Estados;
import br.com.dbccompany.bancodigital.dto.CorrentistasDTO;


public class CorrentistasDAO extends AbstractDAO<Correntistas>{

	@Override
	protected Class<Correntistas> getEntityClass() {
		return Correntistas.class;
	}

	public Correntistas parseFrom(CorrentistasDTO dto) {
		Correntistas c = null;
		c = dto.getId() != null ? buscar(dto.getId()) :  new Correntistas();
		c.setSaldo(dto.getSaldo());
		return c;
	}

}
