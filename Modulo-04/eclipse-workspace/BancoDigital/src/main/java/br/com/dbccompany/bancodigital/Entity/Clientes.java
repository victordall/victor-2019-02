package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;


@Entity
@SequenceGenerator (allocationSize = 1, name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ")
public class Clientes extends AbstractEntity{

	@Id
	@GeneratedValue ( generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer idCliente;
	private String nome;
	private String cpf;
	private String rg;
	private String dataNascimento;
	private EstadoCivil estadoCivil;
	private String conjugue;
	
	@ManyToOne
	@JoinColumn(name = "fk_id_endereco")
	private Enderecos endereco;
	
	@ManyToMany ( mappedBy = "clientes")
	private List<Telefones> telefone = new ArrayList<>();
	
	@ManyToMany (mappedBy = "clientes")
	private List<Correntistas> correntistas = new ArrayList<>();	
	

	public List<Correntistas> getCorrentistas() {
		return correntistas;
	}

	public void setCorrentistas(List<Correntistas> correntistas) {
		this.correntistas = correntistas;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getConjugue() {
		return conjugue;
	}

	public void setConjugue(String conjugue) {
		this.conjugue = conjugue;
	}

	public Enderecos getEndereco() {
		return endereco;
	}

	public void setEndereco(Enderecos endereco) {
		this.endereco = endereco;
	}

	public List<Telefones> getTelefone() {
		return telefone;
	}

	public void setTelefone(List<Telefones> telefone) {
		this.telefone = telefone;
	}

	@Override
	public Integer getId() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}	
	
	
}
