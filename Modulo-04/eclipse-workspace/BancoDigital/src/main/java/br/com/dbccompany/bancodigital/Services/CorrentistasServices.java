package br.com.dbccompany.bancodigital.Services;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.*;

import br.com.dbccompany.bancodigital.Dao.CorrentistasDAO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Entity.Correntistas;

public class CorrentistasServices {

	private static final CorrentistasDAO DAO = new CorrentistasDAO();
	private static final Logger LOG = Logger.getLogger(PaisesServices.class.getName());
	
	public void salvarCorrentistas(CorrentistasDTO dto) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction trans = HibernateUtil.getSession().getTransaction();
		Correntistas c = DAO.parseFrom(dto);
		try {
			Correntistas res = null;
			if(dto.getId() != null) {
				res =  DAO.buscar(dto.getId());
			}		
			c.setSaldo(dto.getSaldo());
			if(res == null) {
				DAO.criar(c);
			}
			else {
				c.setIdCorrentista(res.getId());
				DAO.atualizar(c);
			}
			if(started) {
				trans.commit();
			}
		}
		catch (Exception e) {
			trans.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
	public void efetuarTransferencia(CorrentistasDTO remetente, CorrentistasDTO destinatario, double valor) {
		if(remetente.getSaldo() > valor) {
			destinatario.setSaldo(remetente.getSaldo() + valor);
			remetente.setSaldo(remetente.getSaldo() - valor);
		}
		salvarCorrentistas(remetente);
		salvarCorrentistas(destinatario);
	}
}
