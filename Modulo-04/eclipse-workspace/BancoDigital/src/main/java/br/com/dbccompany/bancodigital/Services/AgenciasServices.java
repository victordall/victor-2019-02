package br.com.dbccompany.bancodigital.Services;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.*;

import br.com.dbccompany.bancodigital.Dao.AgenciasDAO;
import br.com.dbccompany.bancodigital.Dao.BancosDAO;
import br.com.dbccompany.bancodigital.Dao.EnderecoDAO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.dto.BancosDTO;
import br.com.dbccompany.bancodigital.dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.Bancos;

public class AgenciasServices {

	private static final AgenciasDAO DAO = new AgenciasDAO();
	private static final BancosDAO BANCO_DAO = new BancosDAO();
	private static final EnderecoDAO END_DAO = new EnderecoDAO();
	private static final Logger LOG = Logger.getLogger(PaisesServices.class.getName());
	
	public void salvarAgencia(AgenciasDTO dto) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction trans = HibernateUtil.getSession().getTransaction();
		Agencias a = DAO.parseFrom(dto);
		try {
			Agencias res = null;
			if(dto.getIdAgencia() != null) {
				res =  DAO.buscar(dto.getIdAgencia());
			}			
			a.setCodigo(dto.getCodigo());
			a.setNome(dto.getNome());
			if(res == null) {	
				a.setEndereco(END_DAO.buscar(dto.getEndereco().getIdEndereco()));
				if(dto.getBanco() != null) {
					a.setBanco(BANCO_DAO.buscar(dto.getBanco().getIdBanco()));
					DAO.criar(a);
				}
				
			}
			else {
				//FAZER ATUALIZAR CHECAR SE EXIETE BANCO ETC
				a.setIdAgencia(res.getId());
				DAO.atualizar(a);
			}
			if(started) {
				trans.commit();
			}
		}
		catch (Exception e) {
			trans.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
	public AgenciasDTO criarAgencia(String nome, Integer codigo, BancosDTO banco, EnderecosDTO endereco) {
		AgenciasDTO obj = new AgenciasDTO();
		obj.setCodigo(codigo);
		obj.setNome(nome);
		obj.setBanco(banco);
		obj.setEndereco(endereco);
		return obj;
	}
}
