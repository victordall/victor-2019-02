package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.Bancos;
import br.com.dbccompany.bancodigital.dto.AgenciasDTO;

public class AgenciasDAO extends AbstractDAO<Agencias> {

	public Agencias parseFrom(AgenciasDTO dto) {
		Agencias ag = null;
		ag = dto.getIdAgencia() != null ? buscar(dto.getIdAgencia()) : new Agencias();
		return ag;
	}
	
	@Override
	protected Class<Agencias> getEntityClass() {
		return Agencias.class;
	}

}
