package br.com.dbccompany.bancodigital.Services;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.*;


import br.com.dbccompany.bancodigital.Dao.PaisesDAO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Paises;
import br.com.dbccompany.bancodigital.dto.PaisesDTO;

public class PaisesServices {

	private static final PaisesDAO PAISES_DAO = new PaisesDAO();
	private static final Logger LOG = Logger.getLogger(PaisesServices.class.getName());
	
	public void salvarPaises(PaisesDTO pDto) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction trans = HibernateUtil.getSession().getTransaction();
		Paises p = PAISES_DAO.parseFrom(pDto);
		
		try {
			Paises paisesRes = null;
			if(pDto.getIdPaises() != null) {
				paisesRes =  PAISES_DAO.buscar(pDto.getIdPaises());
			}			
			if(paisesRes == null) {
				PAISES_DAO.criar(p);
			}
			else {
				p.setIdPais(paisesRes.getId());
				PAISES_DAO.atualizar(p);
			}
			if(started) {
				trans.commit();
			}
			pDto.setIdPaises(p.getId());
		}
		catch (Exception e) {
			trans.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}

	public Paises buscarPorNome(PaisesDTO dto) {
		String nome = dto.getNome();
		Paises p = null;
		if(nome != null && !nome.equals("")) {
			p = PAISES_DAO.buscarPorNome(nome);
		}
		return p;
	}
	
	public Paises buscarPorNome(String nome) {
		Paises p = null;
		if(nome != null && !nome.equals("")) {
			p = PAISES_DAO.buscarPorNome(nome);
		}
		return p;
	}
	
//	public void salvarPaises(Paises p) {
//		boolean started = HibernateUtil.beginTransaction();
//		Transaction trans = HibernateUtil.getSession().getTransaction();
//		
//		try {
//			PAISES_DAO.criar(p);
//			if(started) {
//				trans.commit();
//			}
//		}
//		catch (Exception e) {
//			trans.rollback();
//			LOG.log(Level.SEVERE, e.getMessage(), e);
//		}
//	}
}
