package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Entity.Estados;
import br.com.dbccompany.bancodigital.Entity.Paises;
import br.com.dbccompany.bancodigital.dto.CidadesDTO;
import br.com.dbccompany.bancodigital.dto.EstadosDTO;

public class CidadesDAO extends AbstractDAO<Cidades> {

	public Cidades parseFrom(CidadesDTO dto) {
		Cidades c = null;
		if(dto.getIdCidade() != null) {
			c = buscar(dto.getIdCidade());
		}else {
			c = new Cidades();			
		}	
		EstadosDAO eDAO = new EstadosDAO();
		Estados est = eDAO.parseFrom(dto.getEstado());

		if(est != null) {
			c.setEstado(est);;
		}
		c.setNome(dto.getNome());
		return c;
	}
	
	@Override
	protected Class<Cidades> getEntityClass() {
		return Cidades.class;
	}

}
