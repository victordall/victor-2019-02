package br.com.dbccompany.bancodigital.Services;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.*;

import br.com.dbccompany.bancodigital.Dao.EnderecoDAO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Paises;
import br.com.dbccompany.bancodigital.dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.dto.PaisesDTO;
import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class EnderecosServices {

	private static final EnderecoDAO DAO = new EnderecoDAO();
	private static final Logger LOG = Logger.getLogger(PaisesServices.class.getName());
	
	public void salvarEnderecos(EnderecosDTO dto) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction trans = HibernateUtil.getSession().getTransaction();
		Enderecos end = DAO.parseFrom(dto);
		try {
			Enderecos endRes = null;
			if(dto.getIdEndereco() != null) {
				endRes =  DAO.buscar(dto.getIdEndereco());
			}			
			if(endRes == null) {
				DAO.criar(end);
			}
			else {
				end.setIdEndereco(endRes.getId());
				DAO.atualizar(end);
			}
			if(started) {
				trans.commit();
			}
		}
		catch (Exception e) {
			trans.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}

	public EnderecosDTO buscar(Integer id) {
		EnderecosDTO dto = new EnderecosDTO();
		Enderecos end = DAO.buscar(id);
		dto.setComplemento(end.getComplemento());
		dto.setIdEndereco(end.getId());
		dto.setLogradouro(end.getLogradouro());
		dto.setNumero(end.getNumero());
		//dto.setBairro(end.getBairro());
		return dto;
		
	}
}
