package br.com.dbccompany.bancodigital.dto;

public class EstadosDTO {

	private Integer idEstado;
	private String nome;
	
	private PaisesDTO pais;

	public Integer getIdEstado() {
		return idEstado;
	}

	public void setIdEstados(Integer idEstados) {
		this.idEstado = idEstados;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public PaisesDTO getPais() {
		return pais;
	}

	public void setPaises(PaisesDTO pais) {
		this.pais = pais;
	}
}
