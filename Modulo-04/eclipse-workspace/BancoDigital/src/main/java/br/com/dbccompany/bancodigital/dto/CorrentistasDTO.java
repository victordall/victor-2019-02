package br.com.dbccompany.bancodigital.dto;

public class CorrentistasDTO {

	private Integer id;
	private double saldo;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	
}
