package br.com.dbccompany.trabalhofinal.Entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@SequenceGenerator(allocationSize = 1, name = "ESPACOS_SEQ", sequenceName = "ESPACOS_SEQ")
public class Espacos extends AbstractEntity {

    @Id
    @GeneratedValue(generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idEspacos;

    @NotNull
    @Column(unique=true)
    private String nome;

    @NotNull
    private Integer qtdPessoas;
    @NotNull
    private Double valor;

    @OneToMany(mappedBy = "espacosCliente", cascade = CascadeType.ALL)
    private List<SaldoCliente> saldoClientes;

    @OneToMany(mappedBy = "espacos", cascade = CascadeType.ALL)
    private List<EspacosPacotes> espacosPacotes;

    @OneToOne(mappedBy = "espacos")
    private Contratacao contratacao;

    public void setIdEspacos(Integer idEspacos) {
        this.idEspacos = idEspacos;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<SaldoCliente> getSaldoClientes() {
        return saldoClientes;
    }

    public void setSaldoClientes(List<SaldoCliente> saldoClientes) {
        this.saldoClientes = saldoClientes;
    }

    @Override
    public Integer getId() {
        return idEspacos;
    }
}