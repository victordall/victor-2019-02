package br.com.dbccompany.trabalhofinal.Repository;

import br.com.dbccompany.trabalhofinal.Entity.Clientes;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ClienteRepository extends CrudRepository<Clientes, Integer> {

    List<Clientes> findAll();
    Clientes findByCpf(String cpf);
}
