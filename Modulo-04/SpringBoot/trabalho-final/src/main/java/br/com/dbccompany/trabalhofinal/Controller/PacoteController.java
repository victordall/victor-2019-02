package br.com.dbccompany.trabalhofinal.Controller;

import br.com.dbccompany.trabalhofinal.DTO.PacoteDTO;
import br.com.dbccompany.trabalhofinal.Entity.Pacotes;
import br.com.dbccompany.trabalhofinal.Services.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pacote")
public class PacoteController {

    @Autowired
    private PacoteService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Pacotes> todosPacotes(){
        return service.todoPacotes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Pacotes criarPacoteComEspacos(@RequestBody PacoteDTO dto)  {
        return service.createPacoteDeEspacos(dto);
    }
}