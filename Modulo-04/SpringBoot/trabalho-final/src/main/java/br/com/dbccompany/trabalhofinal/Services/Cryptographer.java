package br.com.dbccompany.trabalhofinal.Services;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.*;

public class Cryptographer {


    public static String Encrypt(String senha) throws NoSuchAlgorithmException {
        MessageDigest msgD = MessageDigest.getInstance("MD5");
        msgD.reset();
        msgD.update(senha.getBytes());
        byte[] digest = msgD.digest();
        BigInteger bigInt = new BigInteger(1,digest);
        String hash = bigInt.toString(16);
        while(hash.length() < 32 ){
            hash = "0"+hash;
        }
        return hash;
    }
}
