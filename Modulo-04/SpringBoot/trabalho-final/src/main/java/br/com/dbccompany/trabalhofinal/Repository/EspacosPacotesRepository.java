package br.com.dbccompany.trabalhofinal.Repository;

import br.com.dbccompany.trabalhofinal.Entity.EspacosPacotes;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EspacosPacotesRepository extends CrudRepository<EspacosPacotes, Integer> {
    List<EspacosPacotes> findAll();
}