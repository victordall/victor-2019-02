package br.com.dbccompany.trabalhofinal.Entity;

import javax.persistence.*;

@Entity
@SequenceGenerator(allocationSize = 1, name = "ESPACOS_X_PACOTES_SEQ", sequenceName = "ESPACOS_X_PACOTES_SEQ")
public class EspacosPacotes extends AbstractEntity {

    @Id
    @GeneratedValue(generator = "ESPACOS_X_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idEspacosPacotes;

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;
    private Integer quantidade;
    private Integer prazo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idEspacos", referencedColumnName = "idEspacos")
    private Espacos espacos;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idPacote", referencedColumnName = "idPacote")
    private Pacotes pacotes;

    @Override
    public Integer getId() {
        return idEspacosPacotes;
    }

    public Integer getIdEspacosPacotes() {
        return idEspacosPacotes;
    }

    public void setIdEspacosPacotes(Integer idEspacosPacotes) {
        this.idEspacosPacotes = idEspacosPacotes;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }

    public Pacotes getPacotes() {
        return pacotes;
    }

    public void setPacotes(Pacotes pacotes) {
        this.pacotes = pacotes;
    }
}