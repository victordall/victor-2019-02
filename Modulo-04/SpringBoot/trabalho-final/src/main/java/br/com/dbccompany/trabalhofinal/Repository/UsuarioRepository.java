package br.com.dbccompany.trabalhofinal.Repository;

import br.com.dbccompany.trabalhofinal.Entity.Usuarios;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UsuarioRepository extends CrudRepository<Usuarios, Integer> {

    List<Usuarios> findAll();
    Usuarios findByEmail(String email);
    Usuarios findByLogin(String login);
}
