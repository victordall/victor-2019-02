package br.com.dbccompany.trabalhofinal.Repository;

import br.com.dbccompany.trabalhofinal.Entity.Pacotes;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PacotesRepository extends CrudRepository<Pacotes, Integer> {
    List<Pacotes> findAll();
}