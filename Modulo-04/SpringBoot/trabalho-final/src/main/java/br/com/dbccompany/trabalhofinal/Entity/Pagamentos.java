package br.com.dbccompany.trabalhofinal.Entity;

import javax.persistence.*;

@Entity
@SequenceGenerator(allocationSize = 1, name = "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ")
public class Pagamentos extends AbstractEntity {

    @Id
    @GeneratedValue(generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idEspacosPacotes;

    @Enumerated(EnumType.STRING)
    private TipoPagamento tipoPagamento;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idClientesPacotes", referencedColumnName = "idClientesPacotes")
    private ClientesPacotes clientesPacotes;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idContratacao", referencedColumnName = "idContratacao")
    private Contratacao contratacao;

    @Override
    public Integer getId() {
        return idEspacosPacotes;
    }
}