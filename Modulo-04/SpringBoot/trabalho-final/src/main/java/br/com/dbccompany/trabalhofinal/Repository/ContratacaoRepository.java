package br.com.dbccompany.trabalhofinal.Repository;

import br.com.dbccompany.trabalhofinal.Entity.Contratacao;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContratacaoRepository extends CrudRepository<Contratacao, Integer> {
    List<Contratacao> findAll();
}
