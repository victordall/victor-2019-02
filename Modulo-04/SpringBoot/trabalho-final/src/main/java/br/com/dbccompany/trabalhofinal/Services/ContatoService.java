package br.com.dbccompany.trabalhofinal.Services;

import br.com.dbccompany.trabalhofinal.Entity.Contato;
import br.com.dbccompany.trabalhofinal.Entity.TipoContato;
import br.com.dbccompany.trabalhofinal.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Contato salvar(Contato e ){
        return repository.save(e);
    }

    @Transactional( rollbackFor = Exception.class)
    public Contato editar(Integer id, Contato e ){
        e.setIdContato(id);
        return repository.save(e);
    }

    @Transactional( rollbackFor = Exception.class)
    public Contato criarContato(String valor, TipoContato tc){
        Contato contato = new Contato();
        contato.setTipoContato(tc);
        contato.setValor(valor);
        return salvar(contato);
    }

    public List<Contato> todosContatos(){
        return (List<Contato>) repository.findAll();
    }
}
