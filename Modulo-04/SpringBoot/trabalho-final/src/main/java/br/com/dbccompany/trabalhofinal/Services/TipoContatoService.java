package br.com.dbccompany.trabalhofinal.Services;
import br.com.dbccompany.trabalhofinal.DTO.TipoContatoDTO;
import br.com.dbccompany.trabalhofinal.Entity.TipoContato;
import br.com.dbccompany.trabalhofinal.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TipoContatoService {

    @Autowired
    private TipoContatoRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public TipoContato salvar(TipoContato t ){
        return repository.save(t);
    }

    @Transactional( rollbackFor = Exception.class)
    public TipoContato editar(Integer id, TipoContato e ){
        e.setIdTipoConta(id);
        return repository.save(e);
    }

    public List<TipoContato> todosTipoContato(){
        return (List<TipoContato>) repository.findAll();
    }

    public TipoContatoDTO createTipoContatoIfDoesntExist(String valor){
        TipoContato tipoContato = repository.findByNome(valor);
        TipoContatoDTO dto = new TipoContatoDTO();
        dto.setMensagem("Tipo já existe!");
        if(tipoContato == null){
            tipoContato = new TipoContato();
            tipoContato.setNome(valor);
            tipoContato = repository.save(tipoContato);
            dto.setMensagem("Cadastrado com sucesso!");
        }
        dto.setObject(tipoContato);
        return dto;
    }
}
