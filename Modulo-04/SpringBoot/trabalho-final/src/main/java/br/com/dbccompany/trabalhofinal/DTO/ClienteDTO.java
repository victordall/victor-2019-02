package br.com.dbccompany.trabalhofinal.DTO;

import br.com.dbccompany.trabalhofinal.Entity.Clientes;
import br.com.dbccompany.trabalhofinal.Entity.Pacotes;

import java.util.List;

public class ClienteDTO {
    private Clientes cliente;
    private String email;
    private String telefone;
    private String mensagem;
    private PacoteDTO pacoteDTO;

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public PacoteDTO getPacoteDTO() {
        return pacoteDTO;
    }

    public void setPacoteDTO(PacoteDTO pacoteDTO) {
        this.pacoteDTO = pacoteDTO;
    }
}
