package br.com.dbccompany.trabalhofinal.Controller;

import br.com.dbccompany.trabalhofinal.Entity.Usuarios;
import br.com.dbccompany.trabalhofinal.Services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;


@Controller
@RequestMapping("/api/user")
public class UsuarioController {

    @Autowired
    private UsuarioService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Usuarios> todosUsuarios(){
        return service.todosUsuarios();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public String novoUsuario(@RequestBody Usuarios usuario) throws NoSuchAlgorithmException {
        Usuarios user = service.createUser(usuario.getNome(), usuario.getSenha(), usuario.getEmail(), usuario.getLogin());
        String res = "";
        res = user != null ? "Usuário " + user.getNome() + " cadastrado com sucesso!" : "Erro! Login ou Email indisponíveis.";
        return res;
    }
}
