package br.com.dbccompany.trabalhofinal.Repository;

import br.com.dbccompany.trabalhofinal.Entity.TipoContato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TipoContatoRepository  extends CrudRepository<TipoContato, Integer> {

    List<TipoContato> findAll();

    TipoContato findByNome(String nome);
}
