package br.com.dbccompany.trabalhofinal.Repository;

import br.com.dbccompany.trabalhofinal.Entity.Espacos;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EspacosRepository extends CrudRepository<Espacos, Integer> {
    List<Espacos> findAll();
}
