package br.com.dbccompany.trabalhofinal.Controller;

import br.com.dbccompany.trabalhofinal.Entity.Contato;
import br.com.dbccompany.trabalhofinal.Services.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contato")
public class ContatoController {

    @Autowired
    private ContatoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Contato> todosContatos(){
        return service.todosContatos();
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Contato editarContato(@PathVariable Integer id, @RequestBody Contato c){
        return service.editar(id, c);
    }

}