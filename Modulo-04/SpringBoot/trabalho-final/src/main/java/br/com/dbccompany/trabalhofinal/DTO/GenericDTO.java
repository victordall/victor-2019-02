package br.com.dbccompany.trabalhofinal.DTO;

public class GenericDTO {
    private String valor;

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
