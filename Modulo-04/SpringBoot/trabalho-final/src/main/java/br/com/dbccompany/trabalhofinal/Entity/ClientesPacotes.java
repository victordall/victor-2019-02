package br.com.dbccompany.trabalhofinal.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@SequenceGenerator(allocationSize = 1, name = "CLIENTES_X_PACOTES_SEQ", sequenceName = "CLIENTES_X_PACOTES_SEQ")
public class ClientesPacotes extends AbstractEntity {

    @Id
    @GeneratedValue(generator = "CLIENTES_X_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idClientesPacotes;
    private Integer quantidade;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idPacote", referencedColumnName = "idPacote")
    private Pacotes pacotes;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idCliente", referencedColumnName = "idCliente")
    private Clientes clientes;

    @OneToMany(mappedBy = "clientesPacotes", cascade = CascadeType.ALL)
    private List<Pagamentos> pagamentos;

    @Override
    public Integer getId() {
        return idClientesPacotes;
    }

    public void setIdClientesPacotes(Integer idClientesPacotes) {
        this.idClientesPacotes = idClientesPacotes;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public List<Pagamentos> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamentos> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public Pacotes getPacotes() {
        return pacotes;
    }

    public void setPacotes(Pacotes pacotes) {
        this.pacotes = pacotes;
    }
}