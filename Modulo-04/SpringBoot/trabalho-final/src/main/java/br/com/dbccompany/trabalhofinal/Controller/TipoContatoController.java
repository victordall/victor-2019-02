package br.com.dbccompany.trabalhofinal.Controller;

import br.com.dbccompany.trabalhofinal.DTO.GenericDTO;
import br.com.dbccompany.trabalhofinal.DTO.TipoContatoDTO;
import br.com.dbccompany.trabalhofinal.Entity.TipoContato;
import br.com.dbccompany.trabalhofinal.Services.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/tipoContato")
public class TipoContatoController {

    @Autowired
    private TipoContatoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<TipoContato> todosTipoContatos(){
        return service.todosTipoContato();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public TipoContatoDTO novoTipoContato(@RequestBody GenericDTO dto){
        return service.createTipoContatoIfDoesntExist(dto.getValor());
    }
}
