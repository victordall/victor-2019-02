package br.com.dbccompany.trabalhofinal.Controller;

import br.com.dbccompany.trabalhofinal.DTO.ClienteDTO;
import br.com.dbccompany.trabalhofinal.Entity.Clientes;
import br.com.dbccompany.trabalhofinal.Entity.ClientesPacotes;
import br.com.dbccompany.trabalhofinal.Services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/cliente")
public class ClienteController {

    @Autowired
    private ClienteService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Clientes> todosUsuarios(){
        return service.todoClientes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Clientes novoClientes(@RequestBody ClienteDTO dto)  {
        return service.createCliente(dto);
    }

    @PostMapping(value = "/contratarPacote")
    @ResponseBody
    public ClientesPacotes contratarPacote(@RequestBody ClienteDTO dto)  {
        return service.contratarPacote(dto);
    }
}