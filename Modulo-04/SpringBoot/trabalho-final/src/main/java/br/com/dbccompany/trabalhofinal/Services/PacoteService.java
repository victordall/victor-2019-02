package br.com.dbccompany.trabalhofinal.Services;

import br.com.dbccompany.trabalhofinal.DTO.PacoteDTO;
import br.com.dbccompany.trabalhofinal.Entity.Espacos;
import br.com.dbccompany.trabalhofinal.Entity.EspacosPacotes;
import br.com.dbccompany.trabalhofinal.Entity.Pacotes;
import br.com.dbccompany.trabalhofinal.Repository.EspacosPacotesRepository;
import br.com.dbccompany.trabalhofinal.Repository.EspacosRepository;
import br.com.dbccompany.trabalhofinal.Repository.PacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
public class PacoteService {

    @Autowired
    private PacotesRepository repository;

    @Autowired
    private EspacosRepository espacosRepository;

    @Autowired
    private EspacosPacotesRepository espacosPacotesRepository;

    @Transactional(rollbackFor = Exception.class)
    private Pacotes salvar(Pacotes p) {
        return repository.save(p);
    }

    @Transactional(rollbackFor = Exception.class)
    public Pacotes editar(Integer id, Pacotes p) {
        p.setIdPacote(id);
        return repository.save(p);
    }

    public List<Pacotes> todoPacotes() {
        return (List<Pacotes>) repository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Pacotes createPacoteDeEspacos(PacoteDTO dto)  {
        Pacotes p = new Pacotes();
        p.setValor(dto.getPacote().getValor());
        p = salvar(p);
        for(Integer id : dto.getEspacos()){
            EspacosPacotes eP = new EspacosPacotes();
            eP.setPacotes(p);
            eP.setPrazo(dto.getPrazo());
            eP.setQuantidade(dto.getQuantidade());
            eP.setTipoContratacao(dto.getTipoContratacao());
            Optional<Espacos> esp = espacosRepository.findById(id);
            if(!esp.isPresent()){
                break;
            }
            eP.setEspacos(esp.get());
            espacosPacotesRepository.save(eP);
        }
        return p;
    }
}