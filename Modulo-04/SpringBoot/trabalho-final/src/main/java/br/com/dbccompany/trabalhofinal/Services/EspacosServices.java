package br.com.dbccompany.trabalhofinal.Services;

import br.com.dbccompany.trabalhofinal.Entity.Espacos;
import br.com.dbccompany.trabalhofinal.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EspacosServices {

        @Autowired
        private EspacosRepository repository;

        @Transactional( rollbackFor = Exception.class)
        public Espacos salvar(Espacos e ){
            return repository.save(e);
        }

        @Transactional( rollbackFor = Exception.class)
        public Espacos editar(Integer id, Espacos e ){
            e.setIdEspacos(id);
            return repository.save(e);
        }

        public List<Espacos> todosEspacos(){
            return (List<Espacos>) repository.findAll();
        }
}
