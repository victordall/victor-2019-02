package br.com.dbccompany.trabalhofinal.DTO;

import br.com.dbccompany.trabalhofinal.Entity.Clientes;
import br.com.dbccompany.trabalhofinal.Entity.Contratacao;
import br.com.dbccompany.trabalhofinal.Entity.Espacos;

public class ContratacaoDTO {

    private Contratacao contratacao;
    private Clientes cliente;
    private Espacos espaco;
    private double valorPagar;
    private Integer idCliente;
    private Integer idEspaco;

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public Espacos getEspaco() {
        return espaco;
    }

    public void setEspaco(Espacos espaco) {
        this.espaco = espaco;
    }

    public double getValorPagar() {
        return valorPagar;
    }

    public void setValorPagar(double valorPagar) {
        this.valorPagar = valorPagar;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdEspaco() {
        return idEspaco;
    }

    public void setIdEspaco(Integer idEspaco) {
        this.idEspaco = idEspaco;
    }
}
