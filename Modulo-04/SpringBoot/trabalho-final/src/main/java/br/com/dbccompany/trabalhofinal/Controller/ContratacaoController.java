package br.com.dbccompany.trabalhofinal.Controller;

import br.com.dbccompany.trabalhofinal.DTO.ContratacaoDTO;
import br.com.dbccompany.trabalhofinal.Entity.Contratacao;
import br.com.dbccompany.trabalhofinal.Services.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {

    @Autowired
    private ContratacaoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Contratacao> todosEspacos(){
        return service.todasContratacoes();
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Contratacao editarContratacao(@PathVariable Integer id, @RequestBody Contratacao c){
        return service.editar(id, c);
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ContratacaoDTO novaContratacao(@RequestBody ContratacaoDTO dto){
        return service.criarContratacao(dto);
    }

}