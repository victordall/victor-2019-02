package br.com.dbccompany.trabalhofinal.Entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.List;

@Entity
@SequenceGenerator(allocationSize = 1, name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ")
public class Clientes extends AbstractEntity{

    @Id
    @GeneratedValue( generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idCliente;
    @NotNull
    private String nome;
    @NotNull
    @Column(unique=true)
    private String cpf;
    @NotNull
    private Date dataNascimento;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idContato", referencedColumnName = "idContato")
    private Contato contato;

    @OneToOne(mappedBy = "clientes")
    private Contratacao contratacao;

    @OneToMany(mappedBy = "clientes", cascade = CascadeType.ALL)
    private List<SaldoCliente> saldoClientes;

    @OneToMany(mappedBy = "clientes", cascade = CascadeType.ALL)
    private List<ClientesPacotes> clientesPacotes;

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date data_nascimento) {
        this.dataNascimento = data_nascimento;
    }

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }

    @Override
    public Integer getId() {
        return idCliente;
    }
}
