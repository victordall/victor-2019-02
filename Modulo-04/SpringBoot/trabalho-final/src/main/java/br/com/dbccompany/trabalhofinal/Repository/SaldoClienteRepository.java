package br.com.dbccompany.trabalhofinal.Repository;

import br.com.dbccompany.trabalhofinal.Entity.SaldoCliente;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, Integer> {
    List<SaldoCliente> findAll();
}