package br.com.dbccompany.trabalhofinal.Entity;

import javax.persistence.*;
import java.sql.Date;

@Entity
@SequenceGenerator(allocationSize = 1, name = "ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ")
public class Acessos extends AbstractEntity {

    @Id
    @GeneratedValue(generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idAcessos;
    private boolean is_Entrada;
    private Date data;
    private boolean is_excecao;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumns({
            @JoinColumn(name = "idCliente", referencedColumnName = "idCliente"),
            @JoinColumn(name = "idEspacos", referencedColumnName = "idEspacos")
    })
    private SaldoCliente saldoCliente;


    @Override
    public Integer getId() {
        return idAcessos;
    }
}