package br.com.dbccompany.trabalhofinal.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@SequenceGenerator(allocationSize = 1, name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
public class Contato extends AbstractEntity{

    @Id
    @GeneratedValue( generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idContato;
    private String valor;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idTipoContato", referencedColumnName = "idTipoContato")
    private TipoContato tipoContato;

    @OneToMany(mappedBy = "contato", cascade = CascadeType.ALL)
    private List<Clientes> clientes;

    public void setIdContato(Integer idContato) {
        this.idContato = idContato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }

    @Override
    public Integer getId() {
        return idContato;
    }
}
