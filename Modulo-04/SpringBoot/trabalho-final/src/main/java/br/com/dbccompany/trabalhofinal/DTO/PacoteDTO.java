package br.com.dbccompany.trabalhofinal.DTO;

import br.com.dbccompany.trabalhofinal.Entity.Pacotes;
import br.com.dbccompany.trabalhofinal.Entity.TipoContratacao;

import java.util.List;

public class PacoteDTO {
    private Pacotes pacote;
    private List<Integer> espacos;
    private List<Integer> clientes;
    private TipoContratacao tipoContratacao;
    private Integer quantidade;
    private Integer prazo;

    public Pacotes getPacote() {
        return pacote;
    }

    public void setPacote(Pacotes pacote) {
        this.pacote = pacote;
    }

    public List<Integer> getEspacos() {
        return espacos;
    }

    public void setEspacos(List<Integer> espacos) {
        this.espacos = espacos;
    }

    public List<Integer> getClientes() {
        return clientes;
    }

    public void setClientes(List<Integer> clientes) {
        this.clientes = clientes;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}