package br.com.dbccompany.trabalhofinal.Services;

import br.com.dbccompany.trabalhofinal.Entity.SaldoCliente;
import br.com.dbccompany.trabalhofinal.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SaldoClienteService {

    @Autowired
    private SaldoClienteRepository repository;


    @Transactional(rollbackFor = Exception.class)
    private SaldoCliente salvar(SaldoCliente sc) {
        return repository.save(sc);
    }

//    private SaldoCliente getVencimento(SaldoCliente sc) {
//        return repository.save(sc);
//    }
}
