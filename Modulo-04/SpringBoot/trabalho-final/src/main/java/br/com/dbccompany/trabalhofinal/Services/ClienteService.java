package br.com.dbccompany.trabalhofinal.Services;

import br.com.dbccompany.trabalhofinal.DTO.ClienteDTO;
import br.com.dbccompany.trabalhofinal.DTO.TipoContatoDTO;
import br.com.dbccompany.trabalhofinal.Entity.*;
import br.com.dbccompany.trabalhofinal.Repository.ClienteRepository;
import br.com.dbccompany.trabalhofinal.Repository.ContatoRepository;
import br.com.dbccompany.trabalhofinal.Repository.PacotesRepository;
import br.com.dbccompany.trabalhofinal.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository repository;

    @Autowired
    private TipoContatoService tcService;

    @Autowired
    private ContatoService contatoService;

    @Autowired
    private ClientesPacotesService clientesPacotesService;

    @Autowired
    private PacotesRepository pacotesRepository;

    @Transactional( rollbackFor = Exception.class)
    private Clientes salvar(Clientes e ){
        return repository.save(e);
    }

    @Transactional( rollbackFor = Exception.class)
    public Clientes editar(Integer id, Clientes e ){
        e.setIdCliente(id);
        return repository.save(e);
    }

    public List<Clientes> todoClientes(){
        return (List<Clientes>) repository.findAll();
    }

    //Na criação deve por regra ser obrigatório ser criado um contato do tipo: Email !--> [e um do tipo: telefone]
    @Transactional( rollbackFor = Exception.class)
    public Clientes createCliente(ClienteDTO dto){
        //CRIAR EMAIL
        TipoContatoDTO email = tcService.createTipoContatoIfDoesntExist("EMAIL");
        Contato contato = contatoService.criarContato(dto.getEmail(), email.getTipoContato());

        //CRIAR TELEFONE
        //TipoContato telefone = tcService.createTipoContatoIfDoesntExist("TELEFONE");
        //Contato contatoTelefone = contatoService.criarContato(dto.getTelefone(), telefone);

        //Criar cliente
        Clientes cliente = new Clientes();
        cliente.setContato(contato);
        cliente.setCpf(dto.getCliente().getCpf());
        cliente.setNome(dto.getCliente().getNome());
        cliente.setDataNascimento(dto.getCliente().getDataNascimento());
        return salvar(cliente);
    }

    @Transactional( rollbackFor = Exception.class)
    public ClientesPacotes contratarPacote(ClienteDTO dto){

        Optional<Clientes> c = repository.findById(dto.getCliente().getId());
        Clientes clientes = c.get();

        ClientesPacotes cp = new ClientesPacotes();
        cp.setClientes(clientes);
        Optional<Pacotes> p = pacotesRepository.findById(dto.getPacoteDTO().getPacote().getId());
        Pacotes pacotes = p.get();
        cp.setPacotes(pacotes);
        cp.setQuantidade(dto.getPacoteDTO().getQuantidade());
        return clientesPacotesService.salvar(cp);
    }
}
