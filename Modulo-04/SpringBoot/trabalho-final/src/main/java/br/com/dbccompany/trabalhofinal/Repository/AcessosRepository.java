package br.com.dbccompany.trabalhofinal.Repository;

import br.com.dbccompany.trabalhofinal.Entity.Acessos;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface AcessosRepository extends CrudRepository<Acessos, Integer> {
    List<Acessos> findAll();
}
