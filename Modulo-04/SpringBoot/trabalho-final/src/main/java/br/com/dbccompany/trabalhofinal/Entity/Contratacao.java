package br.com.dbccompany.trabalhofinal.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@SequenceGenerator(allocationSize = 1, name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
public class Contratacao extends AbstractEntity {

    @Id
    @GeneratedValue(generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idContratacao;
    private Integer quantidade;
    private double desconto;
    private Integer prazo;

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idCliente", referencedColumnName = "idCliente")
    private Clientes clientes;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idEspacos", referencedColumnName = "idEspacos")
    private Espacos espacos;

    @OneToMany(mappedBy = "contratacao", cascade = CascadeType.ALL)
    private List<Pagamentos> pagamentos;

    public void setIdContratacao(Integer idContratacao) {
        this.idContratacao = idContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }

    public List<Pagamentos> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamentos> pagamentos) {
        this.pagamentos = pagamentos;
    }

    @Override
    public Integer getId() {
        return idContratacao;
    }
}