package br.com.dbccompany.trabalhofinal.Entity;

public enum TipoPagamento {
    DEBITO, CREDITO, DINHEIRO, TRANSFERENCIA
}
