package br.com.dbccompany.trabalhofinal.Services;

import br.com.dbccompany.trabalhofinal.Entity.Usuarios;
import br.com.dbccompany.trabalhofinal.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Usuarios salvar(Usuarios u) {
        return repository.save(u);
    }

    @Transactional(rollbackFor = Exception.class)
    public Usuarios editar(Integer id, Usuarios u) {
        u.setIdUsuario(id);
        return repository.save(u);
    }

    public List<Usuarios> todosUsuarios() {
        return (List<Usuarios>) repository.findAll();
    }

    //Campo email deve ser único, assim como login.
    public boolean isEmailUnique(String email) {
        Usuarios u = repository.findByEmail(email);
        if(u != null){
            return false;
        }
        return true;
    }

    //Campo email deve ser único, assim como login.
    public boolean isLoginUnique(String login){
        Usuarios u = repository.findByLogin(login);
        if(u != null){
            return false;
        }
        return true;
    }

    //Terá campo senha criptografado (MD5), com tamanho mínimo de 6 digitos alfanumérico.
    //Campo email deve ser único, assim como login.
    @Transactional(rollbackFor = Exception.class)
    public Usuarios createUser(String nome, String senha, String email, String login) throws  NoSuchAlgorithmException {
        Usuarios user = null;
        if(isEmailUnique(email) || isLoginUnique(login)){
            user = new Usuarios();
            user.setNome(nome);
            user.setEmail(email);
            user.setLogin(login);
            user.setSenha(Cryptographer.Encrypt(senha));
            user = salvar(user);
        }

        return user;
    }
}