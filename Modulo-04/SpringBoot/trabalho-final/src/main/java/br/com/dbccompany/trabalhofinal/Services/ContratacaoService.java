package br.com.dbccompany.trabalhofinal.Services;

import br.com.dbccompany.trabalhofinal.DTO.ContratacaoDTO;
import br.com.dbccompany.trabalhofinal.Entity.Clientes;
import br.com.dbccompany.trabalhofinal.Entity.Contratacao;
import br.com.dbccompany.trabalhofinal.Entity.Espacos;
import br.com.dbccompany.trabalhofinal.Repository.ClienteRepository;
import br.com.dbccompany.trabalhofinal.Repository.ContratacaoRepository;
import br.com.dbccompany.trabalhofinal.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContratacaoService {

    @Autowired
    private ContratacaoRepository repository;

    @Autowired
    private EspacosRepository espRepo;

    @Autowired
    private ClienteRepository cliRepo;

    @Transactional( rollbackFor = Exception.class)
    private Contratacao salvar(Contratacao e ){
        return repository.save(e);
    }

    @Transactional( rollbackFor = Exception.class)
    public Contratacao editar(Integer id, Contratacao e ){
        e.setIdContratacao(id);
        return repository.save(e);
    }

    @Transactional( rollbackFor = Exception.class)
    public ContratacaoDTO criarContratacao(ContratacaoDTO dto){
        Contratacao contratacao = new Contratacao();

        Optional<Clientes> cliente = cliRepo.findById(dto.getIdCliente());
        Optional<Espacos> espaco = espRepo.findById(dto.getIdEspaco());

        contratacao = dto.getContratacao();
        contratacao.setClientes(cliente.get());
        contratacao.setEspacos(espaco.get());

        contratacao = repository.save(contratacao);
        ContratacaoDTO dtoRes = new ContratacaoDTO();
        dtoRes.setContratacao(contratacao);
        dtoRes.setValorPagar(contratacao.getQuantidade() * contratacao.getEspacos().getValor());
        return dtoRes;
    }

    public double getValorPorIdContratacao(Integer id){
        Optional<Contratacao> contratacaoObj = repository.findById(id);
        Contratacao contratacao = null;
        double valorPagar = 0;
        if(contratacaoObj.isPresent()){
            contratacao = contratacaoObj.get();
            valorPagar = contratacao.getQuantidade() * contratacao.getEspacos().getValor();
        }
        return valorPagar;
    }

    public List<Contratacao> todasContratacoes(){
        return (List<Contratacao>) repository.findAll();
    }
}
