package br.com.dbccompany.trabalhofinal.Entity;

public enum TipoContratacao {
    MINUTO, HORA, TURNO, DIARIAS, SEMANA, MES
}
