package br.com.dbccompany.trabalhofinal.Repository;

import br.com.dbccompany.trabalhofinal.Entity.ClientesPacotes;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ClientesPacotesRepository extends CrudRepository<ClientesPacotes, Integer> {
        List<ClientesPacotes> findAll();
}
