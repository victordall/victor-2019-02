package br.com.dbccompany.trabalhofinal.Entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@SequenceGenerator(allocationSize = 1, name = "USUARIOS_SEQ", sequenceName = "USUARIOS_SEQ")
public class Usuarios extends AbstractEntity{

    @Id
    @GeneratedValue( generator = "USUARIOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idUsuario;

    @NotNull
    private String nome;

    @NotNull
    @Column(unique=true)
    private String email;

    @NotNull
    @Column(unique=true)
    private String login;

    @NotNull
    private String senha;

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Override
    public Integer getId() {
        return idUsuario;
    }
}
