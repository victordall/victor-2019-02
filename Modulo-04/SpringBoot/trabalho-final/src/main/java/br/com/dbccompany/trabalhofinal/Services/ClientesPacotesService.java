package br.com.dbccompany.trabalhofinal.Services;

import br.com.dbccompany.trabalhofinal.Entity.ClientesPacotes;
import br.com.dbccompany.trabalhofinal.Repository.ClientesPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClientesPacotesService {

    @Autowired
    private ClientesPacotesRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public ClientesPacotes salvar(ClientesPacotes e) {
        return repository.save(e);
    }


}