package br.com.dbccompany.trabalhofinal.Controller;

import br.com.dbccompany.trabalhofinal.Entity.Espacos;
import br.com.dbccompany.trabalhofinal.Services.EspacosServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espaco")
public class EspacosController {

    @Autowired
    private EspacosServices service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Espacos> todosEspacos(){
        return service.todosEspacos();
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Espacos editarEspaco(@PathVariable Integer id, @RequestBody Espacos c){
        return service.editar(id, c);
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Espacos novoTipoContato(@RequestBody Espacos espacos){
        return service.salvar(espacos);
    }

}