package br.com.dbccompany.trabalhofinal.Entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.List;

@Entity
@SequenceGenerator(allocationSize = 1, name = "SALDO_X_CLIENTE_SEQ", sequenceName = "SALDO_X_CLIENTE_SEQ")
public class SaldoCliente extends AbstractEntity {

    @Id
    @GeneratedValue(generator = "SALDO_X_CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idSaldoCliente;
    @NotNull
    private double quantidade;
    @NotNull
    private Date vencimento;

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idEspacos", referencedColumnName = "idEspacos")
    private Espacos espacosCliente;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idCliente", referencedColumnName = "idCliente")
    private Clientes clientes;

    @OneToMany(mappedBy = "saldoCliente", cascade = CascadeType.ALL)
    private List<Acessos> acessos;

    public void setIdSaldoCliente(Integer idSaldoCliente) {
        this.idSaldoCliente = idSaldoCliente;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Espacos getEspacos() {
        return espacosCliente;
    }

    public void setEspacos(Espacos espacos) {
        this.espacosCliente = espacos;
    }

    public Espacos getEspacosCliente() {
        return espacosCliente;
    }

    public void setEspacosCliente(Espacos espacosCliente) {
        this.espacosCliente = espacosCliente;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    @Override
    public Integer getId() {
        return idSaldoCliente;
    }
}