package br.com.dbccompany.trabalhofinal.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@SequenceGenerator(allocationSize = 1, name = "PACOTES_SEQ", sequenceName = "PACOTES_SEQ")
public class Pacotes extends AbstractEntity {

    @Id
    @GeneratedValue(generator = "PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idPacote;
    private Integer valor;

    @OneToMany(mappedBy = "pacotes", cascade = CascadeType.ALL)
    private List<EspacosPacotes> espacosPacotes;

    @OneToMany(mappedBy = "pacotes", cascade = CascadeType.ALL)
    private List<ClientesPacotes> clientesPacotes;

    public Integer getIdPacote() {
        return idPacote;
    }

    public void setIdPacote(Integer idPacote) {
        this.idPacote = idPacote;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    @Override
    public Integer getId() {
        return idPacote;
    }
}