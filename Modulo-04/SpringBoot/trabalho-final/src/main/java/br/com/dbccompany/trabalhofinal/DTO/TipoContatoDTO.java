package br.com.dbccompany.trabalhofinal.DTO;

import br.com.dbccompany.trabalhofinal.Entity.TipoContato;

public class TipoContatoDTO {
    private String mensagem;
    private TipoContato tipoContato;

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setObject(TipoContato object) {
        this.tipoContato = object;
    }
}
