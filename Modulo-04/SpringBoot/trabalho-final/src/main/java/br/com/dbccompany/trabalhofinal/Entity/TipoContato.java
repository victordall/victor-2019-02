package br.com.dbccompany.trabalhofinal.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@SequenceGenerator(allocationSize = 1, name = "TIPOCONTATO_SEQ", sequenceName = "TIPOCONTATO_SEQ")
public class TipoContato extends AbstractEntity{

    @Id
    @GeneratedValue( generator = "TIPOCONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idTipoContato;
    private String nome;

    @OneToMany(mappedBy = "tipoContato", cascade = CascadeType.ALL)
    private List<Contato> contatos;

    public void setIdTipoConta(Integer idTipoConta) {
        this.idTipoContato = idTipoConta;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public Integer getId() {
        return idTipoContato;
    }
}
