package br.com.dbccompany.trabalhofinal.Repository;

import br.com.dbccompany.trabalhofinal.Entity.Pagamentos;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentosRepository extends CrudRepository<Pagamentos, Integer> {
    List<Pagamentos> findAll();
}