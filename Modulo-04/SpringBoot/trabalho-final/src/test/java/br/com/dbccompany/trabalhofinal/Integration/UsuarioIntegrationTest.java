package br.com.dbccompany.trabalhofinal.Integration;

import br.com.dbccompany.trabalhofinal.Controller.UsuarioController;
import br.com.dbccompany.trabalhofinal.TrabalhoFinalApplicationTests;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class UsuarioIntegrationTest extends TrabalhoFinalApplicationTests {

    private MockMvc mock;

    @Autowired
    private UsuarioController controller;

    @Before
    public void setUp(){
        this.mock = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void testGetApiStatusOk() throws Exception {
        this.mock.perform(MockMvcRequestBuilders.get("/api/user/")).andExpect(MockMvcResultMatchers.status().isOk());
    }
}
