package br.com.dbccompany.trabalhofinal.Unit;

import br.com.dbccompany.trabalhofinal.Services.Cryptographer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CryptographerTest {

    @Test
    public void testCrypto() throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String senhaString = "senhaSimples";
        String senhaCriptografada = Cryptographer.Encrypt(senhaString);
        String hash = "88d1c8fd9c27ad30f3b5c8833d05f295";
        assertThat(hash).isEqualTo(senhaCriptografada);
    }
}
