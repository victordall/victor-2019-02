package br.com.dbccompany.trabalhofinal.Unit;

import static org.assertj.core.api.Assertions.*;

import br.com.dbccompany.trabalhofinal.Entity.Usuarios;
import br.com.dbccompany.trabalhofinal.Repository.UsuarioRepository;
import br.com.dbccompany.trabalhofinal.Services.UsuarioService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;
import java.security.NoSuchAlgorithmException;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UsuarioBusinessRulesTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UsuarioRepository repository;

//    @Autowired
//    private UsuarioService service;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void testAllFieldsMandatory(){
        Usuarios user = new Usuarios();
        user.setEmail("Email");
        user.setNome("Nome");

        try {
            entityManager.persist(user);
            entityManager.flush();
        } catch ( ConstraintViolationException e ) {
            //exception.expect(ConstraintViolationException.class);
            assertThat(e.getClass()).isEqualTo(ConstraintViolationException.class);
        }
    }

//    @Test
//    public void passwordIsMD5() throws NoSuchAlgorithmException {
//       Usuarios user = service.createUser("Nome", "senhaSimples", "Email", "Login");
//       String hash = "88d1c8fd9c27ad30f3b5c8833d05f295";
//       assertThat(hash).isEqualTo(user.getSenha());
//    }

    @Test
    public void emailAndCpfMustBeUnique(){
        Usuarios user = new Usuarios();
        user.setEmail("Email");
        user.setNome("Nome");
        user.setSenha("senha");
        user.setLogin("login");

        entityManager.persist(user);
        entityManager.flush();

        Usuarios user2 = new Usuarios();
        user.setEmail("Email");
        user.setNome("Nome");
        user.setSenha("senha");
        user.setLogin("login");

        try {
            entityManager.persist(user2);
            entityManager.flush();
        } catch ( ConstraintViolationException e ) {
            //exception.expect(ConstraintViolationException.class);
            assertThat(e.getClass()).isEqualTo(ConstraintViolationException.class);
        }
    }
}
