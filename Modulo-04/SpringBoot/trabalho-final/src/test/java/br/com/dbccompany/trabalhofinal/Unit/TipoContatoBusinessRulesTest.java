package br.com.dbccompany.trabalhofinal.Unit;

import br.com.dbccompany.trabalhofinal.Entity.TipoContato;
import br.com.dbccompany.trabalhofinal.Repository.TipoContatoRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TipoContatoBusinessRulesTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TipoContatoRepository repository;

    @Test
    public void findTipoContatoByTipo(){
        TipoContato tc = new TipoContato();
        tc.setNome("EMAIL");

        entityManager.persist(tc);
        entityManager.flush();

        TipoContato res = repository.findByNome(tc.getNome());
        assertThat(res.getNome()).isEqualTo(tc.getNome());
    }

    @Test
    public void tipoContatoMustBeUnique(){
        TipoContato tc = new TipoContato();
        tc.setNome("EMAIL");

        entityManager.persist(tc);
        entityManager.flush();

        TipoContato tc2  = new TipoContato();
        tc.setNome("EMAIL");

        try {
            entityManager.persist(tc2);
            entityManager.flush();
        } catch ( ConstraintViolationException e ) {
            //exception.expect(ConstraintViolationException.class);
            assertThat(e.getClass()).isEqualTo(ConstraintViolationException.class);
        }
    }
}
