package br.com.dbccompany.vemserSpring.Service;

import br.com.dbccompany.vemserSpring.Entity.Personagens;
import br.com.dbccompany.vemserSpring.Repository.PersonagemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
public class PersonagemService {

    @Autowired
    private PersonagemRepository repository;

    public List<Personagens> todosInventarios(){
        return (List<Personagens>) repository.findAll();
    }

    public Optional<Personagens> getById(Integer id){
        Optional<Personagens> p = null;
        p = repository.findById(id);
        return  p;
    }

    @Transactional( rollbackFor = Exception.class)
    public Personagens editar(Integer id, Personagens e ){
        e.setIdPersonagem(id);
        return repository.save(e);
    }

}
