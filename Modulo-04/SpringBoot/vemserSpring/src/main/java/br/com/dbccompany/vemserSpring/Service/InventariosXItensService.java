package br.com.dbccompany.vemserSpring.Service;

import br.com.dbccompany.vemserSpring.Entity.Inventarios;
import br.com.dbccompany.vemserSpring.Entity.InventariosXItens;
import br.com.dbccompany.vemserSpring.Entity.Itens;
import br.com.dbccompany.vemserSpring.Repository.InventariosXItensRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class InventariosXItensService {

    @Autowired
    private InventariosXItensRepository inventariosXItensRepository;

    @Transactional( rollbackFor = Exception.class)
    public InventariosXItens salvar(InventariosXItens e ){
        return inventariosXItensRepository.save(e);
    }

    @Transactional( rollbackFor = Exception.class)
    public InventariosXItens editar(Integer id, InventariosXItens e ){
        e.setIdX(id);
        return inventariosXItensRepository.save(e);
    }

    public List<InventariosXItens> todosInventariosXItens(){
        return (List<InventariosXItens>) inventariosXItensRepository.findAll();
    }

    @Transactional( rollbackFor = Exception.class)
    public boolean adquiriItem(Inventarios inventario, Itens item, int quantidade){
        InventariosXItens aux = new InventariosXItens();
        aux.setInventarioId(inventario);
        aux.setItemId(item);
        aux.setQuantidade(quantidade);
        InventariosXItens res = salvar(aux);
        if(res == null){
            return false;
        }
        return true;
    }
}
