package br.com.dbccompany.vemserSpring.Entity;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@SequenceGenerator(allocationSize = 1, name = "ITEMS_SEQ", sequenceName = "ITEMS_SEQ")
public class Itens extends AbstractEntity {

    @Id
    @GeneratedValue( generator = "ITEMS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idItem;
    private String descricao;

    @OneToMany ( mappedBy = "itemId", cascade = CascadeType.ALL)
	private List<InventariosXItens> inventarios_X_Itens = new ArrayList<>();

    public void setIdItem(Integer idItem) {
        this.idItem = idItem;
    }
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public Integer getId() {
        return idItem;
    }
}
