package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;

@Entity
@SequenceGenerator(allocationSize = 1, name = "INVENTARIOS_ITEM_SEQ", sequenceName = "INVENTARIOS_ITEM_SEQ")
public class InventariosXItens extends AbstractEntity {

    @Id
    @GeneratedValue( generator = "INVENTARIOS_ITEM_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private Integer quantidade;
    private boolean equipado;

    @ManyToOne
	@JoinColumn(name = "fk_id_item")
	private Itens itemId;

    @ManyToOne
	@JoinColumn(name = "fk_id_inventario")
	private Inventarios inventarioId;

    @Override
    public Integer getId() {
        return id;
    }

    public void setIdX(Integer id){
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public boolean isEquipado() {
        return equipado;
    }

    public void setEquipado(boolean equipado) {
        this.equipado = equipado;
    }

    public Itens getItemId() {
        return itemId;
    }

    public void setItemId(Itens itemId) {
        this.itemId = itemId;
    }

    public Inventarios getInventarioId() {
        return inventarioId;
    }

    public void setInventarioId(Inventarios inventarioId) {
        this.inventarioId = inventarioId;
    }
}
