package br.com.dbccompany.vemserSpring.Repository;

import br.com.dbccompany.vemserSpring.Entity.InventariosXItens;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventariosXItensRepository extends CrudRepository<InventariosXItens, Integer> {
    List<InventariosXItens> findAll();
}
