package br.com.dbccompany.vemserSpring.Controller;

import br.com.dbccompany.vemserSpring.DTO.AdquirirItemDTO;
import br.com.dbccompany.vemserSpring.Entity.Elfo;
import br.com.dbccompany.vemserSpring.Entity.Inventarios;
import br.com.dbccompany.vemserSpring.Entity.Itens;
import br.com.dbccompany.vemserSpring.Entity.Personagens;
import br.com.dbccompany.vemserSpring.Service.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/inventario")
public class InventarioController {

    @Autowired
    InventarioService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Inventarios> todosInventarios(){
        return service.todosInventarios();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Inventarios novoInventario(@RequestBody Inventarios inventario){
        return service.salvar(inventario);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Inventarios editarInventario(@PathVariable Integer id, @RequestBody Inventarios inventario){
        return service.editar(id, inventario);
    }

    @PostMapping(value = "/adquirirItem")
    @ResponseBody
    public boolean adquirirItem(@RequestBody AdquirirItemDTO dto){
        Personagens p = dto.getPersonagens();
        Itens item = dto.getItens();
        int quantidade = dto.getQuantidade();
        return service.adquiriItem(p, item, quantidade);
    }

//    @PostMapping(value = "/adquirirItem")
//    @ResponseBody
//    public boolean adquirirItem(@RequestBody Elfo elfo, @RequestBody Itens item, int quantidade){
//        return service.adquiriItem(elfo, item, quantidade);
//    }
}
