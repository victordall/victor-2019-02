package br.com.dbccompany.vemserSpring.Entity;



import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator(allocationSize = 1, name = "INVENTARIO_SEQ", sequenceName = "INVENTARIO_SEQ")
public class Inventarios {

    @Id
    @GeneratedValue( generator = "INVENTARIO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idInventario;

    @OneToOne(mappedBy = "inventario")
    private Personagens personagem;

    @OneToMany ( mappedBy = "inventarioId", cascade = CascadeType.ALL)
	private List<InventariosXItens> inventarios_X_Itens = new ArrayList<>();

    public Integer getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(Integer idInventario) {
        this.idInventario = idInventario;
    }
}
