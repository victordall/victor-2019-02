package br.com.dbccompany.vemserSpring.Service;

import br.com.dbccompany.vemserSpring.Entity.Itens;
import br.com.dbccompany.vemserSpring.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ItemService {

    @Autowired
    private ItemRepository itemRepository;

    @Transactional( rollbackFor = Exception.class)
    public Itens salvar(Itens i){
        return itemRepository.save(i);
    }

    @Transactional( rollbackFor = Exception.class)
    public Itens editar(Integer id, Itens i){
        i.setIdItem(id);
        return itemRepository.save(i);
    }

    public List<Itens> todosItens(){
        return (List<Itens>) itemRepository.findAll();
    }
}
