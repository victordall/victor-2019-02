package br.com.dbccompany.vemserSpring.DTO;

import br.com.dbccompany.vemserSpring.Entity.Itens;
import br.com.dbccompany.vemserSpring.Entity.Personagens;
import org.hibernate.cache.spi.support.AbstractReadWriteAccess;

public class AdquirirItemDTO {
    private Personagens personagens;
    private Itens itens;
    private int quantidade;

    public AdquirirItemDTO(Personagens personagem, Itens item, int quantidade){
        this.personagens = personagem;
        this.itens = item;
        this.quantidade = quantidade;
    }

    public Personagens getPersonagens() {
        return personagens;
    }

    public void setPersonagens(Personagens personagens) {
        this.personagens = personagens;
    }

    public Itens getItens() {
        return itens;
    }

    public void setItens(Itens itens) {
        this.itens = itens;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}
