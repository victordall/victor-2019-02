package br.com.dbccompany.vemserSpring.Service;

import br.com.dbccompany.vemserSpring.Entity.Inventarios;
import br.com.dbccompany.vemserSpring.Entity.InventariosXItens;
import br.com.dbccompany.vemserSpring.Entity.Itens;
import br.com.dbccompany.vemserSpring.Entity.Personagens;
import br.com.dbccompany.vemserSpring.Repository.InventarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class InventarioService {

    @Autowired
    private InventarioRepository inventarioRepository;

    @Autowired
    private  InventariosXItensService iXi;

    @Autowired
    private PersonagemService pService;

    @Transactional( rollbackFor = Exception.class)
    public Inventarios salvar( Inventarios e ){
        return inventarioRepository.save(e);
    }

    @Transactional( rollbackFor = Exception.class)
    public Inventarios editar(Integer id, Inventarios e){
        e.setIdInventario(id);
        return inventarioRepository.save(e);
    }

    public List<Inventarios> todosInventarios(){
        return (List<Inventarios>) inventarioRepository.findAll();
    }

    @Transactional( rollbackFor = Exception.class)
    public boolean adquiriItem(Personagens p, Itens item, int quantidade){
        Inventarios inventario = p.getInventario();
        if(inventario == null){
            inventario = salvar(inventario);
            p.setInventario(inventario);
            pService.editar(p.getId(), p);
        }
        boolean res =  iXi.adquiriItem(inventario, item, quantidade);
        return res;
    }
}
