package br.com.dbccompany.vemserSpring.Controller;

import br.com.dbccompany.vemserSpring.Entity.InventariosXItens;
import br.com.dbccompany.vemserSpring.Service.InventariosXItensService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/inventariosXItens")
public class InventariosXItensController {
    @Autowired
    InventariosXItensService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<InventariosXItens> todosInventariosXItens(){
        return service.todosInventariosXItens();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public InventariosXItens novoInventarioXItem(@RequestBody InventariosXItens inventarioXItem){
        return service.salvar(inventarioXItem);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public InventariosXItens editarInventariosXItens(@PathVariable Integer id, @RequestBody InventariosXItens inventarioXItem){
        return service.editar(id, inventarioXItem);
    }
}
