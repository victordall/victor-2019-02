package br.com.dbccompany.vemserSpring.Controller;

import br.com.dbccompany.vemserSpring.Entity.Itens;
import br.com.dbccompany.vemserSpring.Service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/item")
public class ItemController {

    @Autowired
    ItemService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Itens> todosItens(){
        return service.todosItens();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Itens novoItem(@RequestBody Itens item){
        return service.salvar(item);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Itens editarItem(@PathVariable Integer id, @RequestBody Itens item){
        return service.editar(id, item);
    }
}
