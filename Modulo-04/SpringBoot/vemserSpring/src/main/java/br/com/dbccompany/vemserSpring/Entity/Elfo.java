package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.Entity;

@Entity
public class Elfo extends Personagens {

    public Elfo(){
        super.setRaca(RaceType.ELFO);
        super.setStatus(Status.RECEM_CRIADO);
    }
}
