import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Login from './pages/Login';
import Home from './pages/Home'
import NovoElfo from './pages/NovoElfo'
import { PrivateRoute } from './components/PrivateRoute';

export default class App extends Component {
  render() {
    return (
      <Router>
        <React.Fragment>
          <PrivateRoute path="/" exact component={Home} />
          <PrivateRoute path='/Home' exact component={Home} />
          <PrivateRoute path='/NovoElfo' exact component={NovoElfo} />
          <Route path="/login" component={Login} />
        </React.Fragment>
      </Router>
    );
  }
}
