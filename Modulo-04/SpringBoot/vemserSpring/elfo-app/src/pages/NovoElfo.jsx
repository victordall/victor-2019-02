import React, { Component } from 'react';
import '../css/container.css'
import '../css/general.css'
import '../css/button.css'
import ComponentHeader from '../components/header/ComponentHeader'
import * as axios from 'axios';
import MensagemFlashConst from '../constants/MensagemFlashConst';
import MensagemFlash from '../components/mensagemFlash/MensagemFlash'




export default class NovoElfo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            nome: '',
            vida: '',
            exibirMensagem: false,
            mensagem: '',
            deveExibirErro: false
        }
        this.trocaValoresState = this.trocaValoresState.bind(this)
    }

    //FAZER ISSO SER REUTILIZAVEL E NAO REPETIR CODIGO - SOONtm
    exibirMensagem = ({ cor, mensagem }) => {
        this.setState({
            cor,
            mensagem,
            exibirMensagem: true
        })
    }

    atualizarMensagem = devoExibir => {
        this.setState({
            exibirMensagem: devoExibir
        })
    }

    trocaValoresState(e) {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        })
    }

    novoElfo(e) {
        e.preventDefault();
        const { nome, vida } = this.state
        if (nome && vida) {
            axios({
                method: 'post',
                url: 'http://localhost:8080/api/elfo/novo',
                headers: {
                    "Authorization": localStorage.getItem("Authorization")
                },  
                data: {
                    "nome": `${this.state.nome}`,
                    "vida": `${this.state.vida}` 
                }                
              }).then(resp => {
                let cor = 'verde'
                let mensagem = MensagemFlashConst.SUCESSO.ELFO_SALVO
                this.exibirMensagem({ cor, mensagem })
                this.setState({
                    nome: '',
                    vida: '',
                })
                document.getElementById('nome').value = ''
                document.getElementById('vida').value = ''
            }
            ).catch(x => {
                let cor = 'vermelho'
                let mensagem = MensagemFlashConst.ERRO.ERRO_POST_ELFO
                this.exibirMensagem({ cor, mensagem })
                console.log(x)
            })
        } else {
            let cor = 'vermelho'
            let mensagem = MensagemFlashConst.ERRO.CAMPO_VAZIO
            this.exibirMensagem({ cor, mensagem })
        }
    }

    render() {
        const { exibirMensagem, cor, mensagem } = this.state
        return (
            <React.Fragment>
                <ComponentHeader></ComponentHeader>
                <MensagemFlash atualizarMensagem={this.atualizarMensagem}
                    cor={cor}
                    deveExibirMensagem={exibirMensagem}
                    mensagem={mensagem} segundos={5} />
                <React.Fragment>
                    <div className="container">
                        <div className="row margin-top-50">
                            <div className="col col-100 col-center">
                                <h1>Criar um novo Elfo</h1>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col col-100">
                                <label>Nome</label>
                                <input type="text" name="nome" id="nome" onChange={this.trocaValoresState}></input>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col col-100">
                                <label>Vida</label>
                                <input type="number" name="vida" id="vida" onChange={this.trocaValoresState}></input>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col col-100 col-center">
                                <button className="btn-login" type="button" onClick={this.novoElfo.bind(this)}>Salvar!</button>
                            </div>
                        </div>
                    </div>
                </React.Fragment>
            </React.Fragment >
        )
    }
}