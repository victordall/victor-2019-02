import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import '../css/container.css'
import '../css/login.css'
import '../css/general.css'
import '../css/button.css'
import ComponentHeader from '../components/header/ComponentHeader'
import * as axios from 'axios';
import { throwStatement } from '@babel/types';



export default class Home extends Component {
    constructor(props){  
        super(props);  
        this.state = { 
            mostrarElfos: false,
            elfosList: [] 
                    };  
        }  
        

    todosElfos() {
        axios({
            method: 'get',
            url: 'http://localhost:8080/api/elfo/',
             headers: {
                 "Authorization": localStorage.getItem("Authorization")
            },                        
          }).then(resp => {            
            this.setState({
                elfosList: resp.data,
                mostrarElfos: true  
            })
            //console.log(resp.data)
            console.log(this.state.elfosList)
           
        }
        ).catch(x => {
            console.log(x)
        })
    }

    esconderElfos(){
        this.setState({
            mostrarElfos: false  
        })
    }

  
    render() {
        const { elfosList, mostrarElfos } = this.state
        return (            
            <React.Fragment>
                <ComponentHeader></ComponentHeader>
                <React.Fragment>
                    <div className="container">
                        <div className="row row-100 padding-top-login padding-bot-login">
                            <div className="col col-100 center-horizontal">
                                <div className="row">
                                    <div className="col col-100 col-center">
                                        <h1>Bem vindo!</h1>
                                    </div>
                                </div>
                                <div className="row">
                                    {mostrarElfos ?  
                                     <div className="col col-100 col-center">
                                     {
                                     elfosList.map( elf =>
                                         {
                                            return (
                                             <div className="row">
                                                 <div className="col col-100">
                                                        <h4>{elf.nome}</h4>
                                                     </div>
                                                 </div>
                                            )
                                         })
                                        //  <Link className="btn-home" onClick={this.esconderElfos.bind(this)}>Esconder Elfos</Link>
                                     }
                                  </div>
                                     : null
                                     }                                     
                                </div>
                                <div className="row margin-top-50">
                                   <div className="col col-100 col-center">
                                        <div className="col col-50">
                                            <Link className="btn-home" onClick={this.todosElfos.bind(this)}>Todos Elfos</Link>
                                        </div>
                                    <div className="col col-50">
                                        <Link className="btn-home" to="/NovoElfo">Novo Elfo</Link>
                                    </div>
                                   </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </React.Fragment>
            </React.Fragment >
        )
    }
}