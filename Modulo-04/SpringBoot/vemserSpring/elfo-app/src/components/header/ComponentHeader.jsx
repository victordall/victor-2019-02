import React from 'react';
import { Link } from 'react-router-dom'
import '../header/header.css'

const ComponentHeader = props => {
    return (
        <React.Fragment>
            <header className="header">
                <nav className="container clearfix">
                    <b> <a className="logo pull-left" href="Home.html"><b>APP</b></a>
                        <ul className="pull-right">
                            <li>
                                <Link to="/Home">Home</Link>
                            </li>
                            <li>
                                <Link to="/NovoElfo">Novo Elfo</Link>
                            </li>
                            <li>
                                { localStorage.getItem("Authorization") != null ? <Link onClick={() => localStorage.removeItem("Authorization")}>Logout</Link> : <Link to="/Login">Login</Link> }
                            </li>
                        </ul>
                    </b>
                </nav>
            </header>
        </React.Fragment>
    )
}

export default ComponentHeader

