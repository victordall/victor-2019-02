import React, { Component } from 'react';

export default class BotaoVoltar extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="container margin-top-50 padding-bot">
                    <div className="row">
                        <div className="col col-50 pull-left">
                            <h4 className="voltar" onClick={() => this.props.voltar()}><i className="fa fa-arrow-left"></i> Voltar</h4>
                        </div>
                    </div>
                </div>  
            </React.Fragment>
        )
    }
}



