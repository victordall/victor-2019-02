const Mensagens = {
    SUCESSO: {
        ELFO_SALVO: 'Elfo criado com sucesso!'
    },
    ERRO: {
        LOGIN_ERRO: 'Usuário ou senha incorretos.',
        CAMPO_VAZIO: 'Necessário preencher todos campos!',
        ERRO_POST_ELFO: 'Um erro ocorreu!'
    }
}

export default Mensagens