import java.util.*;
public class Elfo extends Personagem implements OrdemDeAtaqueElfico
{ 
    private static int numeroElfos = 0;

    public Elfo(String nome){
        super(nome);        
        super.ganharItem(new Item(2, "Flecha"));
        super.ganharItem(new Item(1, "Arco"));
        this.vida = 100.0;     
        Elfo.numeroElfos++;
    }

    public Item getFlecha(){ //método temporario
        return this.getInventario().obterItemPorDescricao("Flecha");      
    }

    public int getQntFlecha(){
        return this.getFlecha().getQuantidade();   
    }   

    protected void incrimentarFlecha(int quantidade){
        getFlecha().acrescentarQuantidade(quantidade);  
    } 

    protected boolean podeAtirarFlecha(){
        return this.getQntFlecha() > 0;   
    }

    protected void gastarFlecha(){
        int qntAtual = this.getFlecha().getQuantidade();             
        this.getFlecha().setQuantidade(qntAtual - 1);
    } 

    public void setFlecha(int qnt){
        int qntAtual = this.getFlecha().getQuantidade();             
        this.getFlecha().setQuantidade(qntAtual + qnt);
    }

    public void atirarFlecha(Dwarf dwarf){
        if(this.podeAtirarFlecha() && dwarf.podeReceberDano()){
            this.gastarFlecha();
            dwarf.receberDano();
            this.incrementarExp(1);    
        }     
    }     

    public static int getNumeroDeElfosCriados(){
        return numeroElfos;
    }

    protected boolean isNoturno(){
        return this.getClass() == ElfoNoturno.class;   
    }

    protected boolean isVerde(){
        return this.getClass() == ElfoVerde.class;   
    }

    protected void finalize() throws Throwable{
        Elfo.numeroElfos--;
    }    
}
