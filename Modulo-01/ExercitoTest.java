
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoTest
{
    @Test
    public void exercitoNaoceitaElfoLuz(){
        Exercito e = new Exercito();
        ElfoDaLuz elfLuz = new ElfoDaLuz("Raito");
        e.alistar(elfLuz);
        assertEquals(0, e.getExercito().size());
    }

    @Test
    public void exercicitoAceitaVerdeNoturno(){
        Exercito e = new Exercito();
        ElfoVerde ev = new ElfoVerde("Gurin");
        ElfoVerde morto = new ElfoVerde("Verde");
        ElfoNoturno en = new ElfoNoturno("Sylvanas");
        e.alistar(ev);
        e.alistar(morto);
        e.alistar(en); 
        assertEquals(3, e.getExercito().size());
    }

    @Test
    public void buscarElfosPorStatus(){
        Exercito e = new Exercito();
        ElfoVerde ev = new ElfoVerde("Gurin");
        ElfoVerde morto = new ElfoVerde("Verde");
        ElfoNoturno en = new ElfoNoturno("Sylvanas");
        morto.matarPersonagem();
        e.alistar(ev);
        e.alistar(morto);
        e.alistar(en);    
        assertEquals(2, e.getElfosPorStatus(Status.RECEM_CRIADO).size());
    }

    
}
