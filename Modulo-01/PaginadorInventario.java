import java.util.*;
public class PaginadorInventario
{
    private Inventario inventario;
    private int marcador = 0;
    public PaginadorInventario(Inventario mochila){
        inventario = mochila;
    }

    public void pular(int quantidade){        
        marcador = quantidade > 0 ? quantidade : 0;   
    }

    public ArrayList<Item> limitar(int quantidade){
        ArrayList<Item> itens = this.inventario.getLista();
        ArrayList<Item> itensLimitados = new ArrayList<Item>();        
        if(this.marcador > itens.size() - 1){
            return  itensLimitados;
        }
        for(int i = this.marcador; i < itens.size(); i++){
            if(quantidade > 0){
                itensLimitados.add(itens.get(i));
                quantidade--;
            }            
        }
        return itensLimitados;
    }
}
