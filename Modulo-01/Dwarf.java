public class Dwarf extends Personagem
{    
    private boolean escudoEquipado = false;

    public Dwarf(String nome){
        super(nome);        
        this.ganharItem(new Item(1, "Escudo"));
        this.setVida(110.0);
    }    

    public void mataDwarf(){
        if(this.vida == 0){
            this.status = Status.MORTO;
        }
    }

    protected double calcularDano(){
        return this.escudoEquipado ? 5.0 : 10.0;   
    }
    
    public void receberDano(){
        if(this.podeReceberDano()){
            if(this.vida > this.calcularDano()){
                this.vida = this.vida - this.calcularDano();
                this.status = Status.SOFREU_DANO;
            }else{                
                this.matarPersonagem();
            }            
        }  
    }
    
    private boolean temEscudo(){
        if(this.getInventario().obterItemPorDescricao("Escudo") != null){
            return true;
        }
        return false;
    }

    public void equipaEscudo(){
        if(this.temEscudo()){
            this.escudoEquipado = true;   
        }
    }

    public void desequipaEscudo(){
        this.escudoEquipado = false;
    }
    
    public String imprimirResultado(){
          return "";
    }
}
