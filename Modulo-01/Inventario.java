import java.util.*;
public class Inventario
{
    private ArrayList<Item> itens;

    public Inventario(){
        itens = new ArrayList<Item>();
    }

    public void adicionar(Item item){
        itens.add(item);
    }

    public Item obter(int posicao){
        return itens.get(posicao);
    }

    public Item obterItemPorDescricao(String descricao){
        for(Item iten : itens){ 
            if(iten.getDescricao().equals(descricao)){
                return iten;
            }
        }
        return null;
    }

    public void removerItem(int posicao){
        itens.remove(posicao);         
    }
    
    public void removerItemPorItem(Item item){
        itens.remove(item);         
    }

    public ArrayList<Item> getLista(){
        return itens;   
    }

    public String getDescricaoItens(){
        String descricaoItens = "";
        if(itens.size() > 0){
            for(Item item : itens){
                descricaoItens += item.getDescricao() + ",";
            } 
            return descricaoItens.substring(0, (descricaoItens.length()-1));
        }
        return descricaoItens = "Nenhum item.";         
    }

    public Item itemComMaiorQuantidade(){
        int indice = 0, maiorQuantidade = 0;
        for(Item item : itens){
            if(item.getQuantidade() > maiorQuantidade){
                indice = itens.indexOf(item);
                maiorQuantidade = item.getQuantidade();
            }
        }
        return this.itens.size() > 0 ? this.itens.get(indice) : null;
    }

    public ArrayList<Item> getOrdemInversaItens(){
        ArrayList<Item> arrayReversa = new ArrayList();
        for(int i = itens.size(); i > 0; i--){
            arrayReversa.add(itens.get(i - 1));
        }
        return arrayReversa;
    }

    public void ordenarItens(){     
        Item[] novaOrdem = new Item[this.getLista().size()];
        int count = 0;
        for(Item iten : this.getLista()){
            novaOrdem[count] = iten;
            count++;
        }        
        Item aux;        
        for(int i = 0; i < novaOrdem.length; i++){
            for(int j = 1; j < novaOrdem.length -i; j++){
                if(novaOrdem[j].getQuantidade() < novaOrdem[j - 1].getQuantidade()){
                    aux = novaOrdem[j - 1];
                    novaOrdem[j - 1] = novaOrdem[j];
                    novaOrdem[j] = aux;
                }
            }
        }
        this.itens.clear();
        for(int i = 0; i < novaOrdem.length; i++){
            itens.add(novaOrdem[i]);
        }
    }

    public void ordenarItensDecrescente(){     
        Item[] novaOrdem = new Item[this.getLista().size()];
        int count = 0;
        for(Item iten : this.getLista()){
            novaOrdem[count] = iten;
            count++;
        }        
        Item aux;        
        for(int i = 0; i < novaOrdem.length; i++){
            for(int j = 1; j < novaOrdem.length -i; j++){
                if(novaOrdem[j].getQuantidade() > novaOrdem[j - 1].getQuantidade()){
                    aux = novaOrdem[j - 1];
                    novaOrdem[j - 1] = novaOrdem[j];
                    novaOrdem[j] = aux;
                }
            }
        }
        this.itens.clear();
        for(int i = 0; i < novaOrdem.length; i++){
            itens.add(novaOrdem[i]);
        }
    }

    public void ordenarItens(TipoOrdenacao tipo){     
        switch(tipo){
            case ASC:
            this.ordenarItens();
            break;
            case DESC:
            this.ordenarItensDecrescente();
            break;
            default:
            break;
        }
    }
}
