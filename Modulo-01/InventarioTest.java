import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventarioTest
{
    @Test
    public void adicionarDoisItens(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        assertEquals(espada, inventario.getLista().get(0));
        assertEquals(escudo, inventario.getLista().get(1));
        assertEquals(2, inventario.getLista().size());
    }

    @Test
    public void removerItemLista(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        assertEquals(2, inventario.getLista().size());
        inventario.removerItem(1);
        assertNull(inventario.obterItemPorDescricao("Escudo"));
    }
    
    @Test
    public void obterItemPelaDescricao(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        assertEquals(espada, inventario.obterItemPorDescricao("Espada"));
    }

    @Test
    public void retornarDescricaoDeTodosItensNoInventario(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        Item zweihander = new Item(1, "Zweihander");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(zweihander);
        assertEquals("Espada,Escudo,Zweihander", inventario.getDescricaoItens());      
    }

    @Test
    public void removerItemAntesDeAdicionarProximo(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionar(espada);
        inventario.removerItem(0);
        inventario.adicionar(escudo);
        assertEquals(escudo, inventario.obter(0));
    }

    @Test
    public void retornarDescricaoTodosItensNoInventarioVazio(){
        Inventario inventario = new Inventario();
        assertEquals("Nenhum item.", inventario.getDescricaoItens());    
    }

    @Test
    public void retornarItemComMaiorQuantidadeComVarios(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(19, "Escudo");
        Item zweihander = new Item(20, "Zweihander");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(zweihander);
        assertEquals(zweihander, inventario.itemComMaiorQuantidade());   
    }

    @Test
    public void getItemComMaiorQuantidadeVazio(){
        Inventario inventario = new Inventario();        
        assertNull(inventario.itemComMaiorQuantidade());   
    }

    @Test
    public void getItemComMaiorQuantidadeComItensMesmaQuantidade(){
        Inventario inventario = new Inventario();
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item zweihander = new Item(2, "Zweihander");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(zweihander);
        assertEquals(espada, inventario.itemComMaiorQuantidade());   
    }
}
