
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * A classe de teste ElfoTest.
 *
 * @author  (seu nome)
 * @version (um número de versão ou data)
 */
public class ElfoTest
{
    @After
    public void tearDown(){
        System.gc();   
    }

    @Test   
    public void atirarFlechaDiminuirFlechaAumentarXP(){
        Elfo novoElfo = new Elfo("Legolas");  
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoDwarf);
        assertEquals(1, novoElfo.getExp());
        assertEquals(1, novoElfo.getQntFlecha());
        assertEquals(100.0, novoDwarf.getVida(), .00001);
    }

    @Test 
    public void atirar3FlechasQuandoTemApenas2(){
        Elfo novoElfoDeTeste = new Elfo("Elf"); 
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfoDeTeste.atirarFlecha(novoDwarf);
        novoElfoDeTeste.atirarFlecha(novoDwarf);
        novoElfoDeTeste.atirarFlecha(novoDwarf);
        assertEquals(2, novoElfoDeTeste.getExp());
        assertEquals(0, novoElfoDeTeste.getQntFlecha());
        assertEquals(90.0, novoDwarf.getVida(), .00001);
    }

    @Test
    public void elfoNascemComStatusRecemCriado(){
        Elfo novoElfo = new Elfo("Legolas");  
        assertEquals(Status.RECEM_CRIADO, novoElfo.getStatus());
    }

    @Test
    public void contarNumeroDeElfos(){        
        for(int i = 0; i < 100; i++){        
            Elfo novoElfo = new Elfo("Elfo n#"+i);
        }

        assertEquals(100, Elfo.getNumeroDeElfosCriados());
    }
}
