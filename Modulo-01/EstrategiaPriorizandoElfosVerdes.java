import java.util.*;
public class EstrategiaPriorizandoElfosVerdes implements EstrategiaDeAtaque
{
    private ArrayList<Elfo> collections(ArrayList<Elfo> elfos){
        Collections.sort(elfos, new Comparator<Elfo>() {
                public int compare(Elfo elfoAtual, Elfo elfoProximo){
                    boolean mesmoTipo = elfoAtual.getClass() == elfoProximo.getClass();
                    if(mesmoTipo){
                        return 0;
                    }
                    return elfoAtual instanceof ElfoVerde && elfoProximo instanceof ElfoNoturno ? -1 : 1;
                }
            });
        return elfos;
    }

    public ArrayList<Elfo> getOrdemAtaque(ArrayList<Elfo> atacantes){
        return collections(atacantes);
    }
}
