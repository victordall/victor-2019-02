import java.util.*;
public class PaginadorInventarioTest
{
    public static void main(String args[]){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(3, "Espada"));
        inventario.adicionar(new Item(4, "Escudo de metal"));
        inventario.adicionar(new Item(5, "Poção de HP"));
        inventario.adicionar(new Item(1, "Bracelete"));
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> retorno;
        retorno = paginador.limitar(2); // retorna os itens “Espada” e “Escudo de metal”
        System.out.println("***Primeiro retorno***");
        for(Item item : retorno){
            System.out.println(item.getDescricao());
        }
        paginador.pular(2);
        retorno = paginador.limitar(2); // retorna os itens “Poção de HP” e “Bracelete”
        System.out.println("***Segundo retorno***");
        for(Item item : retorno){
            System.out.println(item.getDescricao());
        }
        
        //Testa se ordena itens por quantidade
        inventario.ordenarItens();
        System.out.println("***Terceiro retorno***");
        for(Item item : inventario.getLista()){
            System.out.println("Descrição: " + item.getDescricao() + " Quantidade: " + item.getQuantidade());
        }
        
        //Testa se ordena itens por quantidade crescente
        inventario.ordenarItens(TipoOrdenacao.ASC);
        System.out.println("***Quarto retorno***");
        for(Item item : inventario.getLista()){
            System.out.println("Descrição: " + item.getDescricao() + " Quantidade: " + item.getQuantidade());
        }
        
        //Testa se ordena itens por quantidade decrescente
        inventario.ordenarItens(TipoOrdenacao.DESC);
        System.out.println("***Quinto retorno***");
        for(Item item : inventario.getLista()){
            System.out.println("Descrição: " + item.getDescricao() + " Quantidade: " + item.getQuantidade());
        }
    }
}
