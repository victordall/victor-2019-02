import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class ExercicitoQueAtacaTest
{
    private final double DELTA = 1e-9;
    @Test
    public void atacaDwarf(){
        Dwarf dwarf = new Dwarf("Gimli");
        EstrategiaPriorizandoElfosVerdes epev = new EstrategiaPriorizandoElfosVerdes();
        ExercicitoQueAtaca exercito = new ExercicitoQueAtaca(epev);
        Elfo n1 = new ElfoNoturno("Sylvanas");
        Elfo n2 = new ElfoNoturno("Naito" );
        Elfo v1 = new ElfoVerde("Gurin"); 
        Elfo n3 = new ElfoNoturno("Yoru");
        Elfo v2 = new ElfoVerde("Midori");     
        Elfo v3 = new ElfoVerde("Aoi");   
        Elfo n4 = new ElfoNoturno("Yami");
        Elfo v4 = new ElfoVerde("Morto");
        Elfo v5 = new ElfoVerde("Sofreu Dano");
        v4.matarPersonagem();
        v5.receberDano(98);
        exercito.alistar(n1);
        exercito.alistar(n2);
        exercito.alistar(v1);
        exercito.alistar(n3);
        exercito.alistar(v2);
        exercito.alistar(v3);
        exercito.alistar(n4);
        exercito.alistar(v3);
        exercito.alistar(v4);
        exercito.atacarDwarf(dwarf);
        assertEquals(30.0, dwarf.getVida(), DELTA);
    }
    
    @Test
    public void trocarEstrategia(){
        EstrategiaPriorizandoElfosVerdes epev = new EstrategiaPriorizandoElfosVerdes();
        ExercicitoQueAtaca exercito = new ExercicitoQueAtaca(epev);
        EstrategiaAtaqueIntercalado eai = new EstrategiaAtaqueIntercalado();
        exercito.trocaEstrategia(eai);
        assertEquals(eai, exercito.getEstrategia());
    }
}
