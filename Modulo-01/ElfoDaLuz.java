public class ElfoDaLuz extends Elfo
{
    private int contadorAtaques = 1;
    private int QNTD_VIDA_GANHA = 10;
    private int QNTD_DANO = 21;
    public ElfoDaLuz(String nome)
    {
        super(nome);
        this.ganharItem(new Item(1, "Espada de Galvorn"));
    }    

    @Override
    public void perderItem(Item item){
        if(!item.getDescricao().equals("Espada de Galvorn")){
            super.perderItem(item);
        }
    }    

    private boolean isContadorPar(){
        return this.contadorAtaques%2 == 0;    
    }

    private void resetContadorAtaq(){
        this.contadorAtaques = 0;
    }

    private void contarAtaque(){
        if(isContadorPar()){
            contadorAtaques++;
        }
        this.resetContadorAtaq();
    }

    private void efetuarAtaqueComEspada(Dwarf dwarf){
        this.contarAtaque();
        dwarf.receberDano();
        this.incrementarExp(1);
    }

    public void atacarComEspada(Dwarf dwarf){
        if(dwarf.podeReceberDano() && this.status != Status.MORTO){
            if(this.isContadorPar()){
                this.efetuarAtaqueComEspada(dwarf);
                this.recuperarVida(QNTD_VIDA_GANHA);
            }else{
                this.efetuarAtaqueComEspada(dwarf);
                this.receberDano(QNTD_DANO);                
            }            
        }
    }
}
