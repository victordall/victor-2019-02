import java.util.*;
public abstract class Personagem
{
    private String nome;
    protected double vida;
    protected Inventario mochila;
    protected int exp;
    protected Status status;

    {
        mochila = new Inventario(); 
        this.status = Status.RECEM_CRIADO;
        exp = 0;
    }

    protected Personagem(String nome){
        this.nome = nome;
    }

    protected String getNome(){
        return nome;
    }

    protected void setNome(String nome){
        this.nome = nome;   
    }

    protected void ganharItem(Item item){
        this.mochila.adicionar(item);
    }

    protected void perderItem(Item item){
        this.mochila.removerItemPorItem(item); 
    }

    protected Inventario getInventario(){
        return this.mochila;   
    }

    protected double getVida(){
        return this.vida;   
    }

    protected void setVida(double valor){
        this.vida = valor;
    }

    protected boolean podeReceberDano(){
        if(this.status != Status.MORTO){
            return this.vida > 0;
        } 
        return false;
    }

    protected int getExp(){
        return exp;
    }

    protected Status getStatus(){
        return this.status;   
    }

    protected void incrementarExp(int exp){
        this.exp = this.exp + exp;
    }

    protected void receberDano(double dano){
        if(this.podeReceberDano()){
            if(this.vida > dano){
                this.vida = this.vida - dano;
                this.status = Status.SOFREU_DANO;
            }else{                
                this.matarPersonagem();
            }            
        }        
    }

    protected void recuperarVida(double vida){
        this.vida += vida;
    }

    protected void matarPersonagem(){
        this.status = Status.MORTO;
        this.vida = 0;
    }  
}
