package OrdenacaoDeExercito_VERSAO_ANTIGA;

import java.util.*;
public class OrdenarPorFlechaDescrescente implements Comparator<Elfo> 
{
    @Override
    public int compare(Elfo e1, Elfo e2) {                
        return (int) (e2.getQntFlecha() - e1.getQntFlecha());
    }
}
