package OrdenacaoDeExercito_VERSAO_ANTIGA;

import java.util.*;
public class OrdenarPorElfosVerdesPrimeiro implements Comparator<Elfo> 
{
    @Override
    public int compare(Elfo e1, Elfo e2) {                
        if(e1.getClass() == e2.getClass()){
            return 0;
        }
        if(e1.getClass() == ElfoVerde.class){
            return -1;   
        }
        return 1;
    }
}
