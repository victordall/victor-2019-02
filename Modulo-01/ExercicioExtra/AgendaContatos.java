package ExercicioExtra;

import java.util.*;

public class AgendaContatos
{
    private HashMap<String, Contato> contatos = new HashMap<>();
    public void adicionarContato(Contato c){
        contatos.put(c.getNome(), c);
    }

    public String getTelefonePorNome(String nome){
        return contatos.get(nome).getTelefone();        
    }

    public Contato getContatoPorTelefone(String tel){
        for(Contato contato : contatos.values()) {
            if(contato.getTelefone().equals(tel)){
                return contato;
            }
        }
        return null;
    }

    public String csv(){
        String csv = "NOME,TELEFONE"  + System.lineSeparator();
        ArrayList<String> nomesAlbetico = new ArrayList(contatos.keySet());
        Collections.sort(nomesAlbetico);
        for(String nome : nomesAlbetico) {
            csv += nome + "," + contatos.get(nome).getTelefone()+ System.lineSeparator();
        }
        return csv;        
    }
}
