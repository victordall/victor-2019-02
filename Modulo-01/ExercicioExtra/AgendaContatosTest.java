package ExercicioExtra;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class AgendaContatosTest
{
    @Test
    public void getTelefonePorNome(){
        AgendaContatos ac = new AgendaContatos();
        Contato c = new Contato("AAAA", "00");
        ac.adicionarContato(c);
        assertEquals("00", ac.getTelefonePorNome("AAAA"));
    }
    
    @Test
    public void getContatoPorTelefone(){
        AgendaContatos ac = new AgendaContatos();
        Contato c = new Contato("AAAA", "00");
        ac.adicionarContato(c);
        assertEquals(c, ac.getContatoPorTelefone("00"));
    }
    
    @Test
    public void getCSV(){
        AgendaContatos ac = new AgendaContatos();
        Contato c0 = new Contato("AAAA", "012120");
        Contato c1 = new Contato("CCCC", "0232420");
        Contato c2 = new Contato("HFHD", "045450");
        Contato c3 = new Contato("REGTY", "454500");
        Contato c4 = new Contato("QWFR", "04440");
        Contato c5 = new Contato("CFREQQ", "25200");        
        ac.adicionarContato(c0);
        ac.adicionarContato(c1);
        ac.adicionarContato(c2);
        ac.adicionarContato(c3);
        ac.adicionarContato(c4);
        ac.adicionarContato(c5);     
        System.out.println(ac.csv());
    }
}
