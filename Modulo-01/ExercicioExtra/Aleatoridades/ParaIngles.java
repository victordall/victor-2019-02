package Aleatoridades;
public class ParaIngles implements Tradutor
{
    public String traduzir(String textoEmPortugues){
        switch(textoEmPortugues){
            case "Sim":
            return "Yes";                 
            case "Obrigado":
            case "Obrigada":
            return "Thank You";
            default:
            return null;
        }
    }
}
