import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class DwarfTest
{
    private final double DELTA = 1e-9;
    @Test   
    public void dwarfTomaDano(){          
        Dwarf novoDwarf = new Dwarf("Gimli");
        
         novoDwarf.receberDano();           
               
        assertEquals(100, novoDwarf.getVida(), DELTA);
        assertEquals(Status.SOFREU_DANO, novoDwarf.getStatus());
    }
    
    
    @Test   
    public void dwarfMorreSeChegarAZero(){          
        Dwarf novoDwarf = new Dwarf("Gimli");
        int hits = 11;
        while(hits > 0){
           novoDwarf.receberDano(); 
           hits--;
        }        
        assertEquals(0, novoDwarf.getVida(), DELTA);
        assertEquals(Status.MORTO, novoDwarf.getStatus());
    }
    
    @Test
    public void dwarfNaoTomaDanoPosMortem(){
        Dwarf novoDwarf = new Dwarf("Gimli");
        int hits = 11;
        while(hits > 0){
           novoDwarf.receberDano(); 
           hits--;
        }        
        novoDwarf.receberDano(); 
        assertEquals(0, novoDwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfNasceComEscudoNoInventario(){
        Dwarf dwarf = new Dwarf("Gimli");
        assertNotNull(dwarf.getInventario().obterItemPorDescricao("Escudo"));
    }
    
    @Test
    public void dwarfTomaDanoPelaMetadeSeEscudoEquipado(){
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoDwarf.receberDano();
        assertEquals(100.0, novoDwarf.getVida(), DELTA);
        novoDwarf.equipaEscudo();
        novoDwarf.receberDano();
        assertEquals(95.0, novoDwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfNaoEquipaEscudoTomaDanoIntegral(){
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.equipaEscudo();
        dwarf.receberDano();
        assertEquals(105.0, dwarf.getVida(), DELTA);    
        dwarf.desequipaEscudo();
        dwarf.receberDano();
        assertEquals(95.0, dwarf.getVida(), DELTA);
    }
}
