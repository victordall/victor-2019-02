import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest
{
    @Test     
    public void atirarFlechaDiminuirFlechaEVidaAumentarXP(){
        ElfoNoturno elfoN = new ElfoNoturno("Sylvanas");  
        Dwarf novoDwarf = new Dwarf("Gimli");
        elfoN.atirarFlecha(novoDwarf);
        assertEquals(3, elfoN.getExp());
        assertEquals(1, elfoN.getQntFlecha());
        assertEquals(100.0, novoDwarf.getVida(), .00001);
        assertEquals(85.0, elfoN.getVida(), .00001);
    }

    @Test 
    public void atirar3FlechasQuandoTemApenas2(){
        ElfoNoturno elfoN = new ElfoNoturno("Sylvanas");  
        Dwarf novoDwarf = new Dwarf("Gimli");        
        elfoN.atirarFlecha(novoDwarf);
        elfoN.atirarFlecha(novoDwarf);
        elfoN.atirarFlecha(novoDwarf);
        assertEquals(6, elfoN.getExp());
        assertEquals(0, elfoN.getQntFlecha());
        assertEquals(90.0, novoDwarf.getVida(), .00001);
        assertEquals(70.0, elfoN.getVida(), .00001);
    }
    
    @Test //não acho que deveria ser possível disparar se não tem vida o suficiente
    public void nDisparaComVidaMenorQue15(){
        ElfoNoturno elfoN = new ElfoNoturno("Sylvanas");  
        Dwarf novoDwarf = new Dwarf("Gimli");   
        elfoN.receberDano(90);
        elfoN.atirarFlecha(novoDwarf);
        assertEquals(0, elfoN.getExp());
        assertEquals(2, elfoN.getQntFlecha());
        assertEquals(110.0, novoDwarf.getVida(), .00001);
    }
}
