import java.util.*;

public class EstrategiaPorNumFlecha implements EstrategiaDeAtaque
{
    private ArrayList<Elfo> collections(ArrayList<Elfo> elfos){        
        Collections.sort(elfos, new Comparator<Elfo>() {
                public int compare(Elfo elfoAtual, Elfo elfoProximo){
                    if(elfoAtual.getQntFlecha() == elfoProximo.getQntFlecha()){
                        return 0;
                    }
                    return elfoAtual.getQntFlecha() > elfoProximo.getQntFlecha() ? -1 : 1;
                }
            });
        return elfos;
    }   

    private ArrayList<Elfo> getListaComTrintaPorCentoDeNoturnos(ArrayList<Elfo> listaElfica){
        int verde = 0;
        int noturno = 0;
        ArrayList<Elfo> novaList = new ArrayList<Elfo>(listaElfica);
        for(Elfo ef : listaElfica){
            if(ef.getQntFlecha() < 1){
                novaList.remove(ef);   
            }else if(ef.isNoturno()){
                noturno++;
            }else{
                verde++;
            }
        }
        listaElfica = new ArrayList<Elfo>(novaList);
        double porcento = (verde + noturno)*0.3;
        int elfosNoturnosPossiveis = (int)porcento;
        int count = noturno - elfosNoturnosPossiveis;
        Collections.reverse(listaElfica);
        for(Elfo f : listaElfica){
            if(count > 0){
                if(f.isNoturno()){
                    novaList.remove(f);
                    count--;
                }
            }else{break;}
        }
        return novaList;
    }

    public ArrayList<Elfo> getOrdemAtaque(ArrayList<Elfo> atacantes){
        return getListaComTrintaPorCentoDeNoturnos(collections(atacantes));
    }
}
