import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;
public class EstrategiaAtaqueIntercaladoTest
{
    @Test
    public void exercitoIntercalado(){
        EstrategiaAtaqueIntercalado estrategia = new EstrategiaAtaqueIntercalado();
        //EstrategiaPriorizandoElfosVerdes estrategia = new EstrategiaPriorizandoElfosVerdes();
        Elfo n1 = new ElfoNoturno("Sylvanas #1");
        Elfo n2 = new ElfoNoturno("Sylvanas #2" );
        Elfo v1 = new ElfoVerde("Gurin #3"); 
        Elfo n3 = new ElfoNoturno("Sylvanas #4");
        Elfo v2 = new ElfoVerde("Gurin #5");     
        Elfo v3 = new ElfoVerde("Gurin #6");   
        Elfo n4 = new ElfoNoturno("Sylvanas #7");
        ArrayList<Elfo> elfosEnviados = new ArrayList<>(
                Arrays.asList(n1, n2,  v1, n3, v2,v3,n4)
            );

        ArrayList<Elfo> elfosEsperado = new ArrayList<>(
                Arrays.asList(n2, v1, n3, v2, n4, v3)
            );
        ArrayList<Elfo> elfosResultado = estrategia.getOrdemAtaque(elfosEnviados);

        assertEquals(elfosEsperado, elfosResultado);
    }
}
