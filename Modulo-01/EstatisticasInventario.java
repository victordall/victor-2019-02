import java.util.*;
public class EstatisticasInventario
{
    private Inventario inventario;
    public EstatisticasInventario(Inventario mochila){
        inventario = mochila;
    }

    public double calcularMedia(){
        double media = 0;
        for(Item item : inventario.getLista()){
            media += item.getQuantidade();
        }
        return media/inventario.getLista().size();
    }

    public double calcularMediana(){
        ArrayList<Integer> ordenadoPorQnt = new ArrayList<Integer>();        
        for(Item item : inventario.getLista()){
            ordenadoPorQnt.add(item.getQuantidade());            
        }
        Collections.sort(ordenadoPorQnt);
        double mediana = 0;
        if(ordenadoPorQnt.size() > 0){
            if(ordenadoPorQnt.size() % 2 == 0){
                mediana = (ordenadoPorQnt.get(ordenadoPorQnt.size()/2) + ordenadoPorQnt.get(ordenadoPorQnt.size()/2 - 1))/2;
            }else{
                mediana = ordenadoPorQnt.get(ordenadoPorQnt.size()/2);
            }
        }
        return mediana;
    }

    public int qtdItensAcimaDaMedia(){
        double media = this.calcularMedia();
        int nItensAcimaMedia = 0;
        for(Item item : inventario.getLista()){
            if(item.getQuantidade() > media){
                nItensAcimaMedia++;   
            }
        }
        return nItensAcimaMedia;
    }
}
