import java.util.*;
public class Exercito 
{
    private ArrayList<Elfo> elfos = new ArrayList<Elfo>();
    private HashMap<Status, ArrayList<Elfo>> porStatus  = new HashMap<>();
    private final ArrayList<Class> TIPOS_PERMITIDOS
    = new ArrayList<>(
            Arrays.asList(ElfoVerde.class, ElfoNoturno.class));   

    private boolean isNoturno(Elfo elfo){
        return elfo.getClass() == ElfoNoturno.class;   
    }

    private boolean isVerde(Elfo elfo){
        return elfo.getClass() == ElfoVerde.class;   
    }

    private boolean podeEntrarExercito(Elfo elf){
        return TIPOS_PERMITIDOS.contains(elf.getClass());
    }

    public ArrayList<Elfo> getExercito(){
        return elfos;
    }

    public void alistar(Elfo elf){
        if(podeEntrarExercito(elf)){
            elfos.add(elf);
            ArrayList<Elfo> elfoDoStatus = porStatus.get(elf.getStatus());
            if(elfoDoStatus == null){
                elfoDoStatus = new ArrayList<>();
                porStatus.put(elf.getStatus(), elfoDoStatus);
            }
            elfoDoStatus.add(elf);
        }
    }

    public ArrayList<Elfo> getElfosPorStatus(Status s){
        return porStatus.get(s);
    }    

    protected ArrayList<Elfo> getListaSemMortos(){
        ArrayList<Elfo> vivos = getElfosPorStatus(Status.RECEM_CRIADO);
        ArrayList<Elfo> avariados = getElfosPorStatus(Status.SOFREU_DANO);
        if(avariados != null){
            vivos.addAll(avariados);    
        }        
        return vivos;
    }

    private boolean isVivo(Elfo e){
        return e.getStatus() != Status.MORTO;
    }    

    //================VERSÃO ANTIGA =====================================================================================================
    private HashMap<String, Elfo> exercitoDeElfos = new HashMap<>();
    public HashMap<String, Elfo> getElfosPorStatusDEPRECATED(Status  s){
        HashMap<String, Elfo> elfosEncontratods = new HashMap<>();
        for(Elfo elf : exercitoDeElfos.values()) {
            if(elf.getStatus() == s){
                elfosEncontratods.put(elf.getNome(), elf);   
            }
        }
        return elfosEncontratods;
    }    

    public void alistarElfoVerdeNoturno(Elfo elf){
        if(podeEntrarExercito(elf)){
            exercitoDeElfos.put(elf.getNome(), elf);
        }
    }

    public HashMap<String, Elfo> getExercitoDEPRECATED(){
        return this.exercitoDeElfos;
    }
}
