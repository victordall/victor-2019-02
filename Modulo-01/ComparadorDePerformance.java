import java.util.*;
public class ComparadorDePerformance
{
    public void comparar(){
        ArrayList<Elfo> arrayElfos = new ArrayList<>();
        HashMap<String, Elfo> hashMapElfos = new HashMap<>();
        int qtdElfos = 1000000;
        for(int i = 0; i < qtdElfos; i++){
            String nome = "Erufu " + i;
            Elfo elfo = new Elfo(nome);
            arrayElfos.add(elfo);
            hashMapElfos.put(elfo.getNome(), elfo);
        }

        String nomeBusca = "Erufu 999599";

        long mSeqInicio = System.currentTimeMillis();
        Elfo elfoSeq = buscaSequencial(arrayElfos, nomeBusca);        
        long seqEndo = System.currentTimeMillis();

        long mHashInicio = System.currentTimeMillis();     
        Elfo elfoHash = buscaHash(hashMapElfos, nomeBusca);
        long hashEndo = System.currentTimeMillis();

        String tempoSeq = String.format("%.10f", (mSeqInicio - seqEndo)/ 1000);
        String tempoSeqHash = String.format("%.10f", (mHashInicio - hashEndo)/ 1000);
        
        System.out.println("Tempo Array: " + tempoSeq);
        System.out.println("Tempo Hash: " + tempoSeqHash);
    }

    private Elfo buscaSequencial(ArrayList<Elfo> lista, String nomePesquisar){
        for(Elfo elfo : lista){
            if(elfo.getNome().equals(nomePesquisar)){
                return elfo;
            }
        }
        return null;
    }

    private Elfo buscaHash(HashMap<String, Elfo> hash, String nomePesquisar){
        return hash.get(nomePesquisar);
    }
}
