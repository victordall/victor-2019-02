import java.util.*;
public class ExercicitoQueAtaca extends Exercito
{
    private EstrategiaDeAtaque estrategia;

    public ExercicitoQueAtaca(EstrategiaDeAtaque estrategia){
        this.estrategia = estrategia;
    }

    public void trocaEstrategia(EstrategiaDeAtaque estrategia){
        this.estrategia = estrategia;
    }    

    public EstrategiaDeAtaque getEstrategia(){
        return this.estrategia;
    }

    public void atacarDwarf(Dwarf d){
        ArrayList<Elfo> exercito = estrategia.getOrdemAtaque(this.getListaSemMortos());
        for(Elfo elf : exercito){
            elf.atirarFlecha(d);
        }
    }
}
