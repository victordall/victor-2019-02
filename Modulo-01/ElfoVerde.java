import java.util.*;

public class ElfoVerde extends Elfo
{    
    private final ArrayList<String> itensPossiveis = new ArrayList<String>
        (
            Arrays.asList("Espada de aço valiriano","Arco de Vidro","Flecha de Vidro" )
        );

    public ElfoVerde(String nome){
        super(nome);
    }

    @Override
    public void atirarFlecha(Dwarf dwarf){
        if(this.podeAtirarFlecha() && dwarf.podeReceberDano()){
            this.gastarFlecha();
            dwarf.receberDano();
            this.incrementarExp(2);               
        }     
    }     

    public boolean elfoVerdePodeTer(Item item){
       return itensPossiveis.contains(item.getDescricao());
    }

    @Override
    public void ganharItem(Item item){
        if(this.elfoVerdePodeTer(item)){
            super.ganharItem(item); 
        }
    }

    @Override
    public void perderItem(Item item){
        if(this.elfoVerdePodeTer(item)){
            super.perderItem(item);
        }
    }
}

