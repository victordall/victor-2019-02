public class Item
{
    private int quantidade;
    private String descricao;
    
    public Item(int quantidade, String descricao){
        this.descricao = descricao;
        this.quantidade = quantidade;
    }
    
    public void setQuantidade(int quantidade){
        this.quantidade = quantidade;   
    }
        
    public void setDescricao(String descricao){
        this.descricao = descricao;
    }
    
    public int getQuantidade(){
        return quantidade;
    }
    
    public String getDescricao(){
        return descricao;
    }        
    
    public void acrescentarQuantidade(int qnt){
        this.quantidade = this.quantidade + qnt;
    }
    
    public void decrementaQuantidade(){
        this.quantidade--;
    }
}
