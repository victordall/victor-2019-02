public class DwarfBarbaLonga extends Dwarf
{
    public DwarfBarbaLonga(String nome){
        super(nome);
    }

    private boolean passivaNaoTomarDano(){
        int resultado = DadoD6.sortear();
        return resultado >= 5;
    }

    @Override
    public void receberDano(){
        if(this.podeReceberDano()){
            if(!passivaNaoTomarDano()){
                if(this.vida > this.calcularDano()){
                    this.vida = this.vida - this.calcularDano();
                    this.status = Status.SOFREU_DANO;
                }else{                
                    this.matarPersonagem();
                }  
            }                      
        }  
    }
}
