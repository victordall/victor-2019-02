import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest
{
    private final double DELTA = 1e-9;

    @Test     
    public void nasceComEspada(){
        ElfoDaLuz elfoLuz = new ElfoDaLuz("Raito");
        assertNotNull(elfoLuz.getInventario().obterItemPorDescricao("Espada de Galvorn"));
    }

    @Test 
    public void naoPodePerderEspada(){      
        ElfoDaLuz elfoLuz = new ElfoDaLuz("Raito");
        elfoLuz.perderItem(elfoLuz.getInventario().obterItemPorDescricao("Flecha"));
        elfoLuz.perderItem(elfoLuz.getInventario().obterItemPorDescricao("Espada de Galvorn"));
        assertNotNull(elfoLuz.getInventario().obterItemPorDescricao("Espada de Galvorn"));
        assertNull(elfoLuz.getInventario().obterItemPorDescricao("Flecha"));
    }

    @Test
    public void perdeVidaAtaqueImparGanhaNoPar(){
        Dwarf dwarf = new Dwarf("Gimli");   
        ElfoDaLuz elfoLuz = new ElfoDaLuz("Raito");
        elfoLuz.atacarComEspada(dwarf);
        assertEquals(1, elfoLuz.getExp());
        assertEquals(79.0 , elfoLuz.getVida(), DELTA);
        elfoLuz.atacarComEspada(dwarf);
        assertEquals(2, elfoLuz.getExp());
        assertEquals(89.0 , elfoLuz.getVida(), DELTA);
    }
    
    @Test
    public void elfoDaLuzMorreAtaqueImparVidaInsuficiente(){
        Dwarf dwarf = new Dwarf("Gimli");   
        ElfoDaLuz elfoLuz = new ElfoDaLuz("Raito");
        elfoLuz.receberDano(90);
        elfoLuz.atacarComEspada(dwarf);
        assertEquals(1, elfoLuz.getExp());
        assertEquals(0.0 , elfoLuz.getVida(), DELTA);
        assertEquals(Status.MORTO, elfoLuz.getStatus());
    }
    
    @Test
    public void elfoDaLuzNaoAtacaQuandoMorto(){
        Dwarf dwarf = new Dwarf("Gimli");   
        ElfoDaLuz elfoLuz = new ElfoDaLuz("Raito");
        elfoLuz.receberDano(100);
        elfoLuz.atacarComEspada(dwarf);
        assertEquals(0, elfoLuz.getExp());
        assertEquals(0.0 , elfoLuz.getVida(), DELTA);
        assertEquals(Status.MORTO, elfoLuz.getStatus());
    }
}
