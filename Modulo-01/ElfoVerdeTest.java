import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest
{
    @Test     
    public void atirarFlechaDiminuirFlechaAumentarXP(){
        ElfoVerde elfoVerde = new ElfoVerde("Gurin");  
        Dwarf novoDwarf = new Dwarf("Gimli");
        elfoVerde.atirarFlecha(novoDwarf);
        assertEquals(2, elfoVerde.getExp());
        assertEquals(1, elfoVerde.getQntFlecha());
        assertEquals(100.0, novoDwarf.getVida(), .00001);
    }
    
    @Test 
    public void atirar3FlechasQuandoTemApenas2(){
        ElfoVerde elfoVerde = new ElfoVerde("Gurin");  
        Dwarf novoDwarf = new Dwarf("Gimli");
        elfoVerde.atirarFlecha(novoDwarf);
        elfoVerde.atirarFlecha(novoDwarf);
        elfoVerde.atirarFlecha(novoDwarf);
        assertEquals(4, elfoVerde.getExp());
        assertEquals(0, elfoVerde.getQntFlecha());
        assertEquals(90.0, novoDwarf.getVida(), .00001);
    }
    
    @Test
    public void SoRecebeItemEspeciais(){
        Item espadaAco = new Item(1, "Espada de aço valiriano");
        Item flechaVidro = new Item(1,"Flecha de Vidro");
        Item arcoVidro = new Item(1,"Arco de Vidro");
        Item zweihander = new Item(1, "Zweihander");
        ElfoVerde elfoVerde = new ElfoVerde("Gurin"); 
        elfoVerde.ganharItem(espadaAco);
        elfoVerde.ganharItem(flechaVidro);
        elfoVerde.ganharItem(arcoVidro);
        elfoVerde.ganharItem(zweihander);
        assertEquals(5, elfoVerde.getInventario().getLista().size());
        assertNull(elfoVerde.getInventario().obterItemPorDescricao("Zweihander"));
    }
    
    @Test
    public void ElfoVerdeTemArcoEFlechaNormais(){
        ElfoVerde elfoVerde = new ElfoVerde("Gurin"); 
        Inventario inventario = elfoVerde.getInventario();
        assertEquals(inventario.obter(0).getDescricao(), "Flecha");
        assertEquals(inventario.obter(1).getDescricao(), "Arco");        
    }
    
    @Test
    public void ElfoVerdeNaoConsegueDescartarArcoFlechaNormal(){
        ElfoVerde elfoVerde = new ElfoVerde("Gurin"); 
        Item arco = elfoVerde.getInventario().obter(1);
        elfoVerde.perderItem(arco);
        Inventario inventario = elfoVerde.getInventario();     
        assertEquals(inventario.obter(1).getDescricao(), "Arco");
        Item flechaVidro = new Item(1,"Flecha de Vidro");
        elfoVerde.ganharItem(flechaVidro);
        assertEquals(inventario.obterItemPorDescricao("Flecha de Vidro"), flechaVidro);
        elfoVerde.perderItem(flechaVidro);
        assertNull(inventario.obterItemPorDescricao("Flecha de Vidro"));
    }
}
