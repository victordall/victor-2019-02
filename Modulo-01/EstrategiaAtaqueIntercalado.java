import java.util.*;
public class EstrategiaAtaqueIntercalado implements EstrategiaDeAtaque
{
    private ArrayList<Elfo> collections(ArrayList<Elfo> elfos){
        ArrayList<Elfo> listaElfosLimpa = cortaElfosEmDemasia(elfos);
        ArrayList<Elfo> formacao = new ArrayList<Elfo>();
        boolean verde = false;
        boolean noturno = false;
        while(formacao.size() < listaElfosLimpa.size()){
            for(Elfo elf : listaElfosLimpa){ 
                if(elf.isNoturno()){
                    if(!noturno && !formacao.contains(elf)){
                        verde = false;
                        noturno = true;                        
                        formacao.add(elf);                   
                    }
                }else{
                    if(!verde && !formacao.contains(elf)){
                        verde = true;
                        noturno = false;
                        formacao.add(elf);                    
                    }
                }
            }
        }
        return formacao;
    }

    public ArrayList<Elfo> getOrdemAtaque(ArrayList<Elfo> atacantes){
        return collections(atacantes);
    }

    private ArrayList<Elfo> cortaElfosEmDemasia(ArrayList<Elfo> elfos){
        int noturno = 0;
        int verde = 0;
        ArrayList<Elfo> listaElfica = new ArrayList<Elfo>(elfos);
        for(Elfo ef : elfos){
            if(ef.isNoturno()){
                noturno++;
            }else{
                verde++;}
        }

        int count = 0;        
        if(verde > noturno){
            count = verde - noturno;
            for(Elfo ef : elfos){
                if(count > 0){
                    if(ef.isVerde()){
                        count--;
                        listaElfica.remove(ef);
                    }
                }else{break;}
            }
        }else{
            count = noturno - verde;
            for(Elfo ef : elfos){
                if(count > 0){
                    if(ef.isNoturno()){
                        count--;
                        listaElfica.remove(ef);
                    }
                }else{break;}
            }
        }
        return listaElfica;
    }
}
