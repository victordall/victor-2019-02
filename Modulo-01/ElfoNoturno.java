public class ElfoNoturno extends Elfo
{
    private final int danoPorDisparo = 15;
    
    public ElfoNoturno(String nome){
        super(nome);
    }
    
    @Override
    public void atirarFlecha(Dwarf dwarf){
        if(this.podeAtirarFlecha() && dwarf.podeReceberDano() && this.temVidaParaAtirarFlecha()){
            this.gastarFlecha();
            dwarf.receberDano();
            this.receberDano(danoPorDisparo);
            this.incrementarExp(3);               
        }     
    }  
    
    public boolean temVidaParaAtirarFlecha(){
     return this.getVida() > danoPorDisparo;
    }
}