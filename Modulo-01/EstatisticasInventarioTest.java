import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class EstatisticasInventarioTest
{
    @Test
    public void calcularMedia(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(25, "Espada"));
        inventario.adicionar(new Item(50, "Escudo"));
        inventario.adicionar(new Item(20, "Lança"));
        inventario.adicionar(new Item(5, "Rapier"));
        EstatisticasInventario eI = new EstatisticasInventario(inventario);
        assertEquals(25.0, eI.calcularMedia(), .00001);      
    }

    @Test
    public void calcularMedianaQuantidadeItensImpar(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(20, "Espada"));       
        inventario.adicionar(new Item(25, "Lança"));
        inventario.adicionar(new Item(5, "Rapier"));
        EstatisticasInventario eI = new EstatisticasInventario(inventario);
        assertEquals(20.0, eI.calcularMediana(), .00001);       
    }

    @Test
    public void calcularMedianaQuantidadeItensPar(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(22, "Espada"));
        inventario.adicionar(new Item(50, "Escudo"));
        inventario.adicionar(new Item(20, "Lança"));
        inventario.adicionar(new Item(5, "Rapier"));
        EstatisticasInventario eI = new EstatisticasInventario(inventario);
        assertEquals(21.0, eI.calcularMediana(), .00001);       
    }

    @Test
    public void qtdItensAcimaDaMedia(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(25, "Espada"));
        inventario.adicionar(new Item(50, "Escudo"));
        inventario.adicionar(new Item(20, "Lança"));
        inventario.adicionar(new Item(5, "Rapier"));
        EstatisticasInventario eI = new EstatisticasInventario(inventario);
        assertEquals(1, eI.qtdItensAcimaDaMedia()); 
    }
}
