function criarSanduiche (pao, recheio, queijo){
    console.log(`Seu Sanduiche tem o pão ${pao} com recheio de ${recheio} e queijo ${queijo}`)
}

const ingredientes = ['3 queijos', 'Frango', 'cheddar']

criarSanduiche(...ingredientes);

function receberValoresIndefinidos(...valores){
    valores.map(val => console.log(val));
}

receberValoresIndefinidos([1, 3, 4, 5]);