//Exercicio 2
let inputNome = document.getElementById('name');
let inputEmail = document.getElementById('email');
let avisoEmail = document.getElementById('avisoEmail');
let avisoNome = document.getElementById('avisoNome');
let btn = document.getElementById('btnEnviar'); 

let avisarNome = function(){
    if(inputNome.value.length < 10){
        btn.disabled = true;
        avisoNome.className = "show";        
    }else{
        avisoNome.className = "hide";
        btn.disabled = false;
    }
}

let avisarEmail = function(){
    if(!inputEmail.value.includes("@")){
        avisoEmail.className = "show";
        btn.disabled = true;
    }else{
        avisoEmail.className = "hide";
        btn.disabled = false;
    }
}

inputNome.addEventListener('blur', function () {
    avisarNome();    
})

inputEmail.addEventListener('blur', function () {
    avisarEmail();    
})

let validarForm = function () {
    let formulario = document.getElementById('formContato').elements;
	let vazia = true;
    for (let i = 0; i < formulario.length; i++) {
		if(formulario[i] != formulario[formulario.length - 1]){
			if (formulario[i].value == "") {
				alert("Necessário preencher todos campos!");
				vazia = false;			
				break;
			}
        }
    }
	if(vazia){	
		limpaForm();
		console.log("Certamente foi enviada a mensagem.");
	}
}

let limpaForm = function () {
    let formulario = document.getElementById('formContato').elements;
    for (let i = 0; i < formulario.length; i++) {
        formulario[i].value = "";
    }
}